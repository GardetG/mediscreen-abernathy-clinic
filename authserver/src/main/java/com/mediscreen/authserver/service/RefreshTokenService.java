package com.mediscreen.authserver.service;

import com.mediscreen.authserver.domain.RefreshToken;
import com.mediscreen.authserver.domain.User;
import com.mediscreen.authserver.exception.RefreshTokenException;

/**
 * Service interface to managed user's {@link RefreshToken}.
 */
public interface RefreshTokenService {

  /**
   * Create a new refresh token for the provided {@link User}.
   * If a refresh token already exists for the user then it will be deleted.
   *
   * @param user {@link User} for whom create the {@link RefreshToken}
   * @return a new {@link RefreshToken}
   */
  RefreshToken createRefreshToken(User user);

  /**
   * Use the provided token to retrieve the associated RefreshToken.
   * To ensure single use of the token, the current RefreshToken is discarded and a new one
   * is created and returned with same information and expiry but a new token.
   * If the retrieve RefreshToken couldn't be found or is invalid then an
   * {@link RefreshTokenException} is thrown.
   *
   * @param token Token of the {@link RefreshToken}
   * @return a {@link RefreshToken}
   * @throws RefreshTokenException when refresh token invalid
   */
  RefreshToken useRefreshToken(String token);

}
