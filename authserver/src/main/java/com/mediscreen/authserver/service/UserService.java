package com.mediscreen.authserver.service;

import com.mediscreen.authserver.domain.User;
import com.mediscreen.authserver.dto.UserDto;
import com.mediscreen.authserver.exception.EmailAlreadyUsedException;
import com.mediscreen.authserver.exception.UserNotFoundException;
import com.mediscreen.common.domain.Role;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service interface to manage {@link User}.
 */
public interface UserService {

  /**
   * Get all users and return a page of users according to the paging restriction provided in the
   * {@link Pageable} object.
   *
   * @param pageable to request a paged result, can be {@link Pageable#unpaged()}, must not be null
   * @return a Page of {@link UserDto}
   */
  Page<UserDto> getAllUsersPage(Pageable pageable);

  /**
   * Get a user by the provided id.
   * If no user with such id could be found then an {@link UserNotFoundException} is thrown.
   *
   * @param userId id of the {@link User}
   * @return the requested {@link UserDto}
   * @throws UserNotFoundException when user not found
   */
  UserDto getUserById(int userId) throws UserNotFoundException;

  /**
   * Register a new user with the information provided by the Dto.
   * If the provided email is already used then an {@link EmailAlreadyUsedException} is thrown.
   *
   * @param userDto {@link UserDto} with user to register information
   * @return the registered {@link UserDto}
   * @throws EmailAlreadyUsedException when email already used
   */
  UserDto register(UserDto userDto) throws EmailAlreadyUsedException;

  /**
   * Reset the password of the user according to the provided user id and new password.
   * If no user with such id could be found then an {@link UserNotFoundException} is thrown.
   *
   * @param userId   id of the {@link User}
   * @param password new password
   * @throws UserNotFoundException when user is not found.
   */
  void changePassword(int userId, String password) throws UserNotFoundException;

  /**
   * Assign a role to the user by the provided id.
   * If no user with such id could be found then an {@link UserNotFoundException} is thrown.
   *
   * @param userId id of the {@link User}
   * @param role   new {@link Role} to assign
   * @throws UserNotFoundException when user not found
   */
  void assignRole(int userId, Role role) throws UserNotFoundException;

}