package com.mediscreen.authserver.service.impl;

import com.mediscreen.authserver.config.AppConfig;
import com.mediscreen.authserver.domain.RefreshToken;
import com.mediscreen.authserver.domain.User;
import com.mediscreen.authserver.exception.RefreshTokenException;
import com.mediscreen.authserver.repository.RefreshTokenRepository;
import com.mediscreen.authserver.service.RefreshTokenService;
import java.time.Instant;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service class implementation to managed user's refresh token.
 */
@Service
public class RefreshTokenServiceImpl implements RefreshTokenService {

  @Autowired
  private RefreshTokenRepository refreshTokenRepository;
  @Autowired
  private AppConfig config;

  /**
   * {@inheritDoc}
   */
  @Override
  @Transactional
  public RefreshToken createRefreshToken(User user) {
    refreshTokenRepository.findByUser(user)
        .ifPresent(refreshToken -> {
          refreshTokenRepository.delete(refreshToken);
          refreshTokenRepository.flush();
        });
    RefreshToken refreshToken = new RefreshToken(
        user,
        UUID.randomUUID().toString(),
        Instant.now().plusMillis(config.getRefreshTokenValidity())
    );
    return refreshTokenRepository.save(refreshToken);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  @Transactional
  public RefreshToken useRefreshToken(String token) {
    RefreshToken currentToken = consumeToken(token);
    verifyExpiration(currentToken);
    RefreshToken refreshToken = new RefreshToken(
        currentToken.getUser(),
        UUID.randomUUID().toString(),
        currentToken.getExpiryDate()
    );
    return refreshTokenRepository.save(refreshToken);
  }

  /**
   * Retrieve a refresh token by the provided token and delete it before returning it to ensure
   * that the token are single use.
   *
   * @param token Token of the refresh token
   * @return Current refresh token
   */
  private RefreshToken consumeToken(String token) {
    RefreshToken currentToken = refreshTokenRepository.findByToken(token)
        .orElseThrow(() -> new RefreshTokenException("Refresh token can't be verified."));
    refreshTokenRepository.delete(currentToken);
    refreshTokenRepository.flush();
    return currentToken;
  }

  /**
   * Check the expiration of the refresh token.
   * If the token is expired then an exception is thrown.
   *
   * @param token Refresh token to validate
   * @throws RefreshTokenException when refresh token expired
   */
  private void verifyExpiration(RefreshToken token) {
    if (token.getExpiryDate().isBefore(Instant.now())) {
      throw new RefreshTokenException("Refresh token is expired.");
    }
  }

}
