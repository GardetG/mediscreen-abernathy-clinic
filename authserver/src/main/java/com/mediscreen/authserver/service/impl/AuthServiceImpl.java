package com.mediscreen.authserver.service.impl;

import com.mediscreen.authserver.domain.RefreshToken;
import com.mediscreen.authserver.domain.User;
import com.mediscreen.authserver.dto.LoginRequestDto;
import com.mediscreen.authserver.dto.LoginResponseDto;
import com.mediscreen.authserver.service.AuthService;
import com.mediscreen.authserver.service.RefreshTokenService;
import com.mediscreen.authserver.utils.UserMapper;
import com.mediscreen.common.domain.JwtPrincipal;
import com.mediscreen.common.utils.JwtUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

/**
 * Service class implementation to manage authentication of users.
 */
@Service
public class AuthServiceImpl implements AuthService {

  @Autowired
  AuthenticationManager authenticationManager;
  @Autowired
  RefreshTokenService refreshTokenService;
  @Autowired
  JwtUtils jwtUtils;

  /**
   * {@inheritDoc}
   */
  @Override
  public LoginResponseDto login(LoginRequestDto request) throws AuthenticationException {
    Authentication authentication = authenticationManager.authenticate(
        new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword())
    );
    SecurityContextHolder.getContext().setAuthentication(authentication);
    User user = (User) authentication.getPrincipal();
    JwtPrincipal jwtPrincipal = new JwtPrincipal(user.getUserId(), user.getRole());
    String jwtToken = jwtUtils.generateToken(jwtPrincipal);
    String refreshToken = refreshTokenService.createRefreshToken(user).getToken();
    return UserMapper.toDto(user, jwtToken, refreshToken);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public LoginResponseDto refresh(String token) {
    RefreshToken refreshToken = refreshTokenService.useRefreshToken(token);
    User user = refreshToken.getUser();
    String newJwtToken = createJwt(user);
    String newRefreshToken = refreshToken.getToken();
    return UserMapper.toDto(user, newJwtToken, newRefreshToken);
  }

  /**
   * Return the corresponding jwt that will be user for authentication from the user.
   *
   * @param user User
   * @return Corresponding JwtPrincipal
   */
  private String createJwt(User user) {
    JwtPrincipal principal = new JwtPrincipal(
        user.getUserId(),
        user.getRole()
    );
    return jwtUtils.generateToken(principal);
  }

}
