package com.mediscreen.authserver.service;

import com.mediscreen.authserver.domain.RefreshToken;
import com.mediscreen.authserver.domain.User;
import com.mediscreen.authserver.dto.LoginRequestDto;
import com.mediscreen.authserver.dto.LoginResponseDto;
import com.mediscreen.authserver.exception.RefreshTokenException;
import org.springframework.security.core.AuthenticationException;

/**
 * Service interface to manage authentication of {@link User}.
 */
public interface AuthService {

  /**
   * Use the provided login request to authenticate the user and then return the authenticated user
   * information with the generated Jwt token and refresh token.
   *
   * @param request login {@link LoginRequestDto request} with username and password
   * @return a {@link LoginResponseDto response} with user information and tokens
   * @throws AuthenticationException when authentication fails
   */
  LoginResponseDto login(LoginRequestDto request);

  /**
   * Use the provided token of {@link RefreshToken} to retrieve the user and return user information
   * with refreshed Jwt token and renew token of RefreshToken.
   * If the RefreshToken couldn't be found or has expired then an exception is thrown.
   *
   * @param token token of the {@link RefreshToken}
   * @return a {@link LoginResponseDto response} with user information and tokens
   * @throws RefreshTokenException when refresh token processing fails
   */
  LoginResponseDto refresh(String token);

}
