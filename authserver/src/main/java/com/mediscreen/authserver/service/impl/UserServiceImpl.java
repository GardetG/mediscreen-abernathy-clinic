package com.mediscreen.authserver.service.impl;

import com.mediscreen.authserver.domain.User;
import com.mediscreen.authserver.dto.UserDto;
import com.mediscreen.authserver.exception.EmailAlreadyUsedException;
import com.mediscreen.authserver.exception.UserNotFoundException;
import com.mediscreen.authserver.repository.UserRepository;
import com.mediscreen.authserver.service.UserService;
import com.mediscreen.authserver.utils.UserMapper;
import com.mediscreen.common.domain.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service class implementation to manage users.
 */
@Service
public class UserServiceImpl implements UserService, UserDetailsService {

  @Autowired
  private UserRepository userRepository;
  @Autowired
  private PasswordEncoder passwordEncoder;

  /**
   * {@inheritDoc}
   */
  @Override
  public Page<UserDto> getAllUsersPage(Pageable pageable) {
    return userRepository.findAll(pageable)
        .map(UserMapper::toDto);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public UserDto getUserById(int userId) throws UserNotFoundException {
    User user = getUserByIdOrThrowsException(userId);
    return UserMapper.toDto(user);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  @Transactional
  public UserDto register(UserDto userDto) throws EmailAlreadyUsedException {
    if (userRepository.existsByEmail(userDto.getEmail())) {
      throw new EmailAlreadyUsedException("This email is already registered.", userDto.getEmail());
    }
    User userToRegister = UserMapper.toEntity(userDto);
    userToRegister.setPassword(passwordEncoder.encode(userToRegister.getPassword()));
    User registeredUser = userRepository.save(userToRegister);
    return UserMapper.toDto(registeredUser);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  @Transactional
  public void changePassword(int userId, String password) throws UserNotFoundException {
    User user = getUserByIdOrThrowsException(userId);
    user.setPassword(passwordEncoder.encode(password));
  }

  /**
   * {@inheritDoc}
   */
  @Override
  @Transactional
  public void assignRole(int userId, Role role) throws UserNotFoundException {
    User user = getUserByIdOrThrowsException(userId);
    user.setRole(role);
  }

  /**
   * Return the requested user by the provided id. If no user with such id is found then an
   * exception is thrown.
   *
   * @param userId id of the user
   * @return User entity
   * @throws UserNotFoundException when user not found
   */
  private User getUserByIdOrThrowsException(int userId) throws UserNotFoundException {
    return userRepository.findById(userId)
        .orElseThrow(() -> new UserNotFoundException("User not found.", userId));
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    return userRepository.findByEmail(username)
        .orElseThrow(() -> new UsernameNotFoundException("User not found."));
  }

}