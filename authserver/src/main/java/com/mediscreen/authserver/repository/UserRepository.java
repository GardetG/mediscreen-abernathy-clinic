package com.mediscreen.authserver.repository;

import com.mediscreen.authserver.domain.User;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;

/**
 * Repository interface for {@link User} entities.
 */
@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

  /**
   * Find all users and return a page of users according to the paging restriction provided in the
   * {@link Pageable} object.
   *
   * @param pageable to request a paged result, can be {@link Pageable#unpaged()}, must not be null
   * @return a page of {@link User Users}
   */
  @NonNull
  Page<User> findAll(@NonNull Pageable pageable);

  /**
   * Find a user by the provided id and return an optional of it.
   * If no user with such id could be found then an empty optional is returned.
   *
   * @param userId id of the {@link User}
   * @return an Optional of {@link User}
   */
  Optional<User> findById(int userId);

  /**
   * Find a user by the provided email and return an optional of it.
   * If no user with such email could be found then an empty optional is returned.
   *
   * @param email email of the {@link User}
   * @return an Optional of the {@link User}
   */
  Optional<User> findByEmail(String email);

  /**
   * Check if a user with the provided email already exists.
   *
   * @param email email of the {@link User}
   * @return true if a {@link User} with such email already exists
   */
  boolean existsByEmail(String email);

  /**
   * Save a new user or update an existing one.
   * If unique constraints are violated then a DataIntegrityViolationException is thrown.
   *
   * @param user {@link User} to add or update
   * @param <T>  {@link User} or derived entity
   * @return the saved {@link User}
   */
  @NonNull
  <T extends User> T save(@NonNull T user);

}
