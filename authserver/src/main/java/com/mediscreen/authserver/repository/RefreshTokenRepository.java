package com.mediscreen.authserver.repository;

import com.mediscreen.authserver.domain.RefreshToken;
import com.mediscreen.authserver.domain.User;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;

/**
 * Repository interface for {@link RefreshToken} entities.
 */
@Repository
public interface RefreshTokenRepository extends JpaRepository<RefreshToken, Integer> {

  /**
   * Find a refresh token by the provided {@link User} and return an Optional of it.
   * If no refresh token with such user could be found then an empty Optional is returned.
   *
   * @param user {@link User} of the {@link RefreshToken}
   * @return an Optional of {@link RefreshToken}
   */
  Optional<RefreshToken> findByUser(User user);

  /**
   * Find a refresh token by the provided token and return an Optional of it.
   * If no refresh token with such token could be found then an empty Optional is returned.
   *
   * @param token token of the {@link RefreshToken}
   * @return an Optional of {@link RefreshToken}
   */
  Optional<RefreshToken> findByToken(String token);

  /**
   * Save a new refresh token.
   * If unique constraints are violated then a DataIntegrityViolationException is thrown.
   *
   * @param token {@link RefreshToken} to add
   * @param <T>   {@link RefreshToken} or derived entity
   * @return the saved {@link RefreshToken}
   */
  @NonNull
  <T extends RefreshToken> T save(@NonNull T token);

  /**
   * Delete a refresh token.
   *
   * @param token {@link RefreshToken} to delete
   */
  void delete(@NonNull RefreshToken token);

}
