package com.mediscreen.authserver.dto;

import com.mediscreen.common.domain.Role;

/**
 * Dto class holding login response with the authenticated user info and the generated tokens.
 */
public class LoginResponseDto {

  private final int userId;
  private final String firstname;
  private final String lastname;
  private final Role role;
  private final String token;
  private final String refreshToken;

  /**
   * Constructor method to create a new LoginResponseDto Dto.
   *
   * @param userId       id of the patient
   * @param firstname    firstname of the user
   * @param lastname     lastname of the user
   * @param role         Role of the patient
   * @param token        Authentication jwt token
   * @param refreshToken Refresh token
   */
  public LoginResponseDto(int userId, String firstname, String lastname, Role role,
                          String token,
                          String refreshToken) {
    this.userId = userId;
    this.firstname = firstname;
    this.lastname = lastname;
    this.role = role;
    this.token = token;
    this.refreshToken = refreshToken;
  }

  public int getUserId() {
    return userId;
  }

  public String getFirstname() {
    return firstname;
  }

  public String getLastname() {
    return lastname;
  }

  public Role getRole() {
    return role;
  }

  public String getToken() {
    return token;
  }

  public String getRefreshToken() {
    return refreshToken;
  }

}
