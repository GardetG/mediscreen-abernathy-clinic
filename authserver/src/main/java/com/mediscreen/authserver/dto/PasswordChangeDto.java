package com.mediscreen.authserver.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.validation.constraints.Pattern;

/**
 * Dto class for a password change request.
 */
public class PasswordChangeDto {

  @Pattern(regexp = "^(?=.*\\d)(?=.*[A-Z])(?=.*\\W)(?!.*\\s).{8,125}$",
      message = "must contains one capital letter, one digit and one special characters and have "
          + "at least 8 characters")
  private final String password;

  /**
   * Constructor method to create a new ChangePasswordDto.
   *
   * @param password new password
   */
  @JsonCreator
  public PasswordChangeDto(@JsonProperty("password") String password) {
    this.password = password;
  }

  public String getPassword() {
    return password;
  }

}
