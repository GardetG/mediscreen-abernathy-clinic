package com.mediscreen.authserver.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Dto class for a refresh token request.
 */
public class RefreshRequestDto {

  private final String refreshToken;

  /**
   * Constructor method to create a nes RefreshRequestDto.
   *
   * @param refreshToken Refresh token
   */
  @JsonCreator
  public RefreshRequestDto(@JsonProperty("refreshToken") String refreshToken) {
    this.refreshToken = refreshToken;
  }

  public String getRefreshToken() {
    return refreshToken;
  }

}
