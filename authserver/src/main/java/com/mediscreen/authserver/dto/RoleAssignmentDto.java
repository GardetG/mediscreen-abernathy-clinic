package com.mediscreen.authserver.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.mediscreen.common.domain.Role;
import javax.validation.constraints.NotNull;

/**
 * Dto class for a role assignment request.
 */
public class RoleAssignmentDto {

  @NotNull(message = "must be a valid role")
  private final Role role;

  /**
   * Constructor method to create a new RoleAssignmentDto.
   *
   * @param role role to assign
   */
  @JsonCreator
  public RoleAssignmentDto(@JsonProperty("role") Role role) {
    this.role = role;
  }

  public Role getRole() {
    return role;
  }

}
