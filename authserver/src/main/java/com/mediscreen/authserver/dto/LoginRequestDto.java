package com.mediscreen.authserver.dto;

/**
 * Dto class for a login request.
 */
public class LoginRequestDto {

  private final String username;
  private final String password;

  /**
   * Constructor method to create a new LoginRequestDto.
   *
   * @param username username of the user to authenticate
   * @param password lastname of the user to authenticate
   */
  public LoginRequestDto(String username, String password) {
    this.username = username;
    this.password = password;
  }

  public String getUsername() {
    return username;
  }

  public String getPassword() {
    return password;
  }

}
