package com.mediscreen.authserver.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mediscreen.common.domain.Role;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * Dto Class for user with personal information and credentials.
 */
public class UserDto {

  private final int userId;
  @NotBlank
  @Size(max = 20)
  private final String firstname;
  @NotBlank
  @Size(max = 20)
  private final String lastname;
  @NotBlank
  @Size(max = 50)
  @Email
  private final String email;
  @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
  @Pattern(regexp = "^(?=.*\\d)(?=.*[A-Z])(?=.*\\W)(?!.*\\s).{8,125}$",
      message = "must contains one capital letter, one digit and one special characters and have "
          + "at least 8 characters")
  private final String password;
  @JsonProperty(access = JsonProperty.Access.READ_ONLY)
  private final Role role;

  /**
   * Constructor method to create a new User Dto.
   *
   * @param userId    id of the user
   * @param firstname firstname of the user
   * @param lastname  lastname of the user
   * @param email     email of the user
   * @param password  password of the user
   * @param role      role of the user
   */
  public UserDto(int userId, String firstname, String lastname, String email,
                 String password, Role role) {
    this.userId = userId;
    this.firstname = firstname;
    this.lastname = lastname;
    this.email = email;
    this.password = password;
    this.role = role;
  }

  public int getUserId() {
    return userId;
  }

  public String getFirstname() {
    return firstname;
  }

  public String getLastname() {
    return lastname;
  }

  public String getEmail() {
    return email;
  }

  public String getPassword() {
    return password;
  }

  public Role getRole() {
    return role;
  }

}
