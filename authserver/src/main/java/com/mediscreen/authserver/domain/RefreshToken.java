package com.mediscreen.authserver.domain;

import java.time.Instant;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Entity class of a RefreshToken with the associated token and user, and it's expiry date.
 */
@Entity
@Table(name = "token")
public class RefreshToken {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "token_id")
  private int id;
  @OneToOne
  @JoinColumn(name = "user_id", referencedColumnName = "user_id",
      unique = true, nullable = false)
  private User user;
  @Column(name = "token", unique = true, nullable = false)
  private String token;
  @Column(name = "expiry_date", nullable = false)
  private Instant expiryDate;

  /**
   * Constructor method to create a new RefreshToken entity.
   *
   * @param user       {@link User} associated to this RefreshToken
   * @param token      Token used to retrieve this RefreshToken
   * @param expiryDate Expiry date of this RefreshToken
   */
  public RefreshToken(User user, String token, Instant expiryDate) {
    this.user = user;
    this.token = token;
    this.expiryDate = expiryDate;
  }

  /**
   * Constructor method to create a new RefreshToken entity.
   *
   * @param id         RefreshToken id
   * @param user       {@link User} associated to this RefreshToken
   * @param token      Token used to retrieve this RefreshToken
   * @param expiryDate Expiry date of this RefreshToken
   */
  public RefreshToken(int id, User user, String token, Instant expiryDate) {
    this.id = id;
    this.user = user;
    this.token = token;
    this.expiryDate = expiryDate;
  }

  private RefreshToken() {
    // private constructor needed by hibernate
  }

  public int getId() {
    return id;
  }

  public User getUser() {
    return user;
  }

  public String getToken() {
    return token;
  }

  public Instant getExpiryDate() {
    return expiryDate;
  }

}

