package com.mediscreen.authserver.controller;

import com.mediscreen.authserver.domain.User;
import com.mediscreen.authserver.exception.EmailAlreadyUsedException;
import com.mediscreen.authserver.exception.UserNotFoundException;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * Class handling exceptions thrown by Service in Controller and generate the corresponding HTTP
 * response.
 */
@ControllerAdvice
@Validated
public class ControllerExceptionHandler {

  private static final Logger LOGGER = LoggerFactory.getLogger(ControllerExceptionHandler.class);

  /**
   * Handle {@link UserNotFoundException} thrown when requested {@link User} is not found.
   *
   * @param ex instance of the exception
   * @return HTTP 404 response
   */
  @ExceptionHandler(UserNotFoundException.class)
  public ResponseEntity<String> handleUserNotFoundException(UserNotFoundException ex) {
    LOGGER.warn("{} Id: {}", ex.getMessage(), ex.getId());
    String error = ex.getMessage();
    LOGGER.info("Response : 404 {}", error);
    return ResponseEntity.status(HttpStatus.NOT_FOUND).body(error);
  }

  /**
   * Handle {@link EmailAlreadyUsedException} thrown when email is already registered.
   *
   * @param ex instance of the exception
   * @return HTTP 409 response
   */
  @ExceptionHandler(EmailAlreadyUsedException.class)
  public ResponseEntity<String> handleEmailAlreadyUsedException(EmailAlreadyUsedException ex) {
    LOGGER.warn("{} Email: {}", ex.getMessage(), ex.getEmail());
    String error = ex.getMessage();
    LOGGER.info("Response : 409 {}", error);
    return ResponseEntity.status(HttpStatus.CONFLICT).body(error);
  }

  /**
   * Handle AuthenticationException thrown when authentication fails.
   *
   * @param ex instance of the exception
   * @return HTTP 401 response
   */
  @ExceptionHandler(AuthenticationException.class)
  public ResponseEntity<String> handleAuthenticationExceptionException(AuthenticationException ex) {
    String error = ex.getMessage();
    LOGGER.info("Response : 401 {}", error);
    return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(error);
  }

  /**
   * Handle MethodArgumentNotValidException thrown when validation fails.
   *
   * @param ex instance of the exception
   * @return HTTP 422 response with information on invalid fields
   */
  @ExceptionHandler(MethodArgumentNotValidException.class)
  public ResponseEntity<Map<String, String>> handleValidationExceptions(
      MethodArgumentNotValidException ex) {
    LOGGER.warn("Invalid Dto: {}", ex.getMessage());
    Map<String, String> errors = new HashMap<>();
    ex.getBindingResult().getAllErrors().forEach(error -> {
      String fieldName = ((FieldError) error).getField();
      String errorMessage = error.getDefaultMessage();
      errors.put(fieldName, errorMessage);
    });
    LOGGER.info("Response : 422 invalid DTO");
    return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(errors);
  }

}
