package com.mediscreen.authserver.controller;

import com.mediscreen.authserver.dto.LoginRequestDto;
import com.mediscreen.authserver.dto.LoginResponseDto;
import com.mediscreen.authserver.dto.RefreshRequestDto;
import com.mediscreen.authserver.service.AuthService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller class exposing API end-points for authentication.
 */
@RestController
@RequestMapping("/auth")
@Validated
public class AuthController {

  private static final Logger LOGGER = LoggerFactory.getLogger(AuthController.class);

  @Autowired
  private AuthService authService;

  /**
   * POST a login request to authenticate the user and then return the authenticated user
   * information with the generated Jwt token and refresh token.
   *
   * @param request {@link LoginRequestDto request} request with username and password.
   * @return HTTP 200 with {@link LoginResponseDto response} with user information and tokens
   */
  @Operation(description = "Login a user", tags = {"Authentication"})
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "OK"),
      @ApiResponse(responseCode = "401", description = "UNAUTHORIZED - Authentication fails",
          content = @Content),
  })
  @PostMapping("/login")
  public ResponseEntity<LoginResponseDto> login(@RequestBody LoginRequestDto request) {
    LOGGER.info("Request: Authenticate user");
    LoginResponseDto response = authService.login(request);
    LOGGER.info("Response: User {} successfully authenticated", response.getUserId());
    return ResponseEntity.ok(response);
  }

  /**
   * POST a refresh token to received user information with refreshed Jwt token and renew token
   * of RefreshToken.
   *
   * @param token {@link RefreshRequestDto Request Dto} holding the refresh token
   * @return HTTP 200 with {@link LoginResponseDto response} with user information and tokens
   */
  @Operation(description = "Refresh a user Jwt", tags = {"Authentication"})
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "OK"),
      @ApiResponse(responseCode = "401", description = "UNAUTHORIZED - Refresh token fails",
          content = @Content),
  })
  @PostMapping("/refresh")
  public ResponseEntity<LoginResponseDto> refresh(@RequestBody RefreshRequestDto token) {
    LOGGER.info("Request: Refresh user token");
    LoginResponseDto response = authService.refresh(token.getRefreshToken());
    LOGGER.info("Response: User {} token successfully refreshed", response.getUserId());
    return ResponseEntity.ok(response);
  }

}
