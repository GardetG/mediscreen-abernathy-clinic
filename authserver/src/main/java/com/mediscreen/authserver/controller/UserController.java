package com.mediscreen.authserver.controller;

import com.mediscreen.authserver.domain.User;
import com.mediscreen.authserver.dto.PasswordChangeDto;
import com.mediscreen.authserver.dto.RoleAssignmentDto;
import com.mediscreen.authserver.dto.UserDto;
import com.mediscreen.authserver.exception.EmailAlreadyUsedException;
import com.mediscreen.authserver.exception.UserNotFoundException;
import com.mediscreen.authserver.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller class exposing API end-points for user management.
 */
@RestController
@RequestMapping("/auth/account")
@Validated
public class UserController {

  private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

  @Autowired
  private UserService userService;

  /**
   * GET a page from all users according to the paging restriction provided in the {@link Pageable}
   * object.
   *
   * @param pageable to request a paged result
   * @return HTTP 200 with a Page of {@link UserDto}
   */
  @Operation(description = "View all users by pages", tags = {"User"})
  @PreAuthorize("hasRole('ADMIN')")
  @GetMapping("")
  public ResponseEntity<Page<UserDto>> getAllUsersPage(Pageable pageable) {
    LOGGER.info("Request: Get page {} of users list", pageable.getPageNumber());
    Page<UserDto> usersPage = userService.getAllUsersPage(pageable);

    LOGGER.info("Response: Page {} of users list sent", pageable.getPageNumber());
    return ResponseEntity.ok(usersPage);
  }

  /**
   * GET a user by the provided id.
   *
   * @param userId id of the user
   * @return HTTP 200 with the requested {@link UserDto}
   * @throws UserNotFoundException when user not found
   */
  @Operation(description = "View user profile", tags = {"User"})
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "OK"),
      @ApiResponse(responseCode = "403", description = "FORBIDDEN - User is not allowed to view "
          + "other users profile", content = @Content),
      @ApiResponse(responseCode = "404", description = "NOT FOUND - User not found",
          content = @Content)
  })
  @PreAuthorize("hasRole('ADMIN') or #userId == authentication.principal.userId")
  @GetMapping("/{userId}")
  public ResponseEntity<UserDto> getUserById(@PathVariable("userId") int userId)
      throws UserNotFoundException {
    LOGGER.info("Request: Profile of user {}", userId);
    UserDto user = userService.getUserById(userId);
    LOGGER.info("Response: User {} profile sent", userId);
    return ResponseEntity.ok(user);
  }

  /**
   * POST a registration request for a new user.
   *
   * @param userDto {@link UserDto} with user to register information
   * @return HTTP 201 with the registered {@link UserDto}
   * @throws EmailAlreadyUsedException when email is already used
   */
  @Operation(description = "Register a user", tags = {"User"})
  @ApiResponses(value = {
      @ApiResponse(responseCode = "201", description = "CREATED"),
      @ApiResponse(responseCode = "409", description = "CONFLICT - Email already used",
          content = @Content),
      @ApiResponse(responseCode = "422", description = "UNPROCESSABLE ENTITY - Some fields of the "
          + "provided user are invalid",
          content = @Content)
  })
  @PostMapping("/register")
  public ResponseEntity<UserDto> register(@Valid @RequestBody UserDto userDto)
      throws EmailAlreadyUsedException {
    LOGGER.info("Request: Register a new user");
    UserDto registeredUser = userService.register(userDto);

    LOGGER.info("Response: New user registered with id {}", registeredUser.getUserId());
    return ResponseEntity.status(HttpStatus.CREATED).body(registeredUser);
  }

  /**
   * Reset the password of the user designated by the id and replace it with the provided new
   * password. This end point is authorized only for Admin or User with their own id.
   *
   * @param userId            id of the {@link User}
   * @param passwordChangeDto {@link PasswordChangeDto} holding the new password
   * @return HTTP 204
   * @throws UserNotFoundException when user not found
   */
  @Operation(description = "Reset the password of a user", tags = {"User"})
  @ApiResponses(value = {
      @ApiResponse(responseCode = "204", description = "NO CONTENT - Password reset"),
      @ApiResponse(responseCode = "403", description = "FORBIDDEN - User is not allowed to change "
          + "other users password", content = @Content),
      @ApiResponse(responseCode = "404", description = "NOT FOUND - User not found",
          content = @Content),
      @ApiResponse(responseCode = "422", description = "UNPROCESSABLE ENTITY - new password "
          + "invalid", content = @Content),
  })
  @PreAuthorize("hasRole('ADMIN') or #userId == authentication.principal.userId")
  @PatchMapping("/{userId}/resetPassword")
  public ResponseEntity<Void> changePassword(@PathVariable("userId") int userId,
                                             @Valid @RequestBody PasswordChangeDto passwordChangeDto)
      throws UserNotFoundException {
    LOGGER.info("Request: Change password of User {}", userId);
    userService.changePassword(userId, passwordChangeDto.getPassword());
    LOGGER.info("Response: User {} password changed", userId);
    return ResponseEntity.noContent().build();
  }

  /**
   * Assign a role to the user by the provided id and role.
   * This end point is authorized only for Admin.
   *
   * @param userId            id of the {@link User}
   * @param roleAssignmentDto {@link RoleAssignmentDto} holding the role to assign
   * @return HTTP 204
   * @throws UserNotFoundException when user not found
   */
  @Operation(description = "Assign a role to a user", tags = {"User"})
  @ApiResponses(value = {
      @ApiResponse(responseCode = "204", description = "NO CONTENT - Role assigned"),
      @ApiResponse(responseCode = "403", description = "FORBIDDEN - User is not allowed to assign "
          + "role", content = @Content),
      @ApiResponse(responseCode = "404", description = "NOT FOUND - User not found",
          content = @Content),
      @ApiResponse(responseCode = "422", description = "UNPROCESSABLE ENTITY - Role invalid",
          content = @Content),
  })
  @PreAuthorize("hasRole('ADMIN')")
  @PatchMapping("/{userId}/assignRole")
  public ResponseEntity<Void> assignRole(@PathVariable("userId") int userId,
                                         @Valid @RequestBody RoleAssignmentDto roleAssignmentDto)
      throws UserNotFoundException {
    LOGGER.info("Request: Assign role {} to user {}", roleAssignmentDto.getRole(), userId);
    userService.assignRole(userId, roleAssignmentDto.getRole());
    LOGGER.info("Response: Role {} assigned to user {}", roleAssignmentDto.getRole(), userId);
    return ResponseEntity.noContent().build();
  }

}
