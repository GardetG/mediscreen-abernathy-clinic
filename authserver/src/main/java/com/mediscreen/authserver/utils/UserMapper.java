package com.mediscreen.authserver.utils;

import com.mediscreen.authserver.domain.User;
import com.mediscreen.authserver.dto.LoginResponseDto;
import com.mediscreen.authserver.dto.UserDto;

/**
 * Mapper Class to map {@link User} entity into Dto.
 */
public class UserMapper {

  private UserMapper() {
    throw new IllegalStateException("Utility class");
  }

  /**
   * Map a User entity into UserDto.
   *
   * @param user {@link User} to map
   * @return corresponding {@link UserDto} mapped
   */
  public static UserDto toDto(User user) {
    return new UserDto(
        user.getUserId(),
        user.getFirstname(),
        user.getLastname(),
        user.getEmail(),
        null,
        user.getRole()
    );
  }

  /**
   * Map a User entity into LoginResponseDto.
   *
   * @param user         {@link User} to map
   * @param jwtToken     Jwt Token
   * @param refreshToken Refresh token
   * @return corresponding {@link LoginResponseDto} mapped
   */
  public static LoginResponseDto toDto(User user, String jwtToken, String refreshToken) {
    return new LoginResponseDto(
        user.getUserId(),
        user.getFirstname(),
        user.getLastname(),
        user.getRole(),
        jwtToken,
        refreshToken);
  }

  /**
   * Map a UserDto entity into User.
   *
   * @param userDto {@link UserDto} to map
   * @return corresponding {@link User} mapped
   */
  public static User toEntity(UserDto userDto) {
    return new User(
        userDto.getFirstname(),
        userDto.getLastname(),
        userDto.getEmail(),
        userDto.getPassword()
    );
  }

}
