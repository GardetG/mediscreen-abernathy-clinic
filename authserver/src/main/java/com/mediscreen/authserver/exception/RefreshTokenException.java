package com.mediscreen.authserver.exception;

import org.springframework.security.core.AuthenticationException;

/**
 * Authentication Exception thrown when the refresh token cannot be processed due to being invalid
 * or expired.
 */
public class RefreshTokenException extends AuthenticationException {

  public RefreshTokenException(String msg) {
    super(msg);
  }

}
