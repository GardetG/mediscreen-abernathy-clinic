package com.mediscreen.authserver.exception;

/**
 * Exception thrown to prevent duplicate email when trying to save the requested user.
 */
public class EmailAlreadyUsedException extends Exception {

  private final String email;

  public EmailAlreadyUsedException(String s, String email) {
    super(s);
    this.email = email;
  }

  public String getEmail() {
    return email;
  }

}
