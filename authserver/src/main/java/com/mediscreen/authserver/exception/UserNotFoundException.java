package com.mediscreen.authserver.exception;

/**
 * Exception thrown when the requested user is not found.
 */
public class UserNotFoundException extends Exception {

  private final int id;

  public UserNotFoundException(String s, int id) {
    super(s);
    this.id = id;
  }

  public int getId() {
    return id;
  }

}