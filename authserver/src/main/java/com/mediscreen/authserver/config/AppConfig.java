package com.mediscreen.authserver.config;

import com.mediscreen.common.config.JwtConfig;
import com.mediscreen.common.utils.JwtUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Configuration class to create and configure beans.
 */
@Configuration
@ConfigurationProperties
public class AppConfig {

  private long refreshTokenValidity;

  public long getRefreshTokenValidity() {
    return refreshTokenValidity;
  }

  public void setRefreshTokenValidity(long refreshTokenValidity) {
    this.refreshTokenValidity = refreshTokenValidity;
  }

  @Bean
  @ConfigurationProperties(prefix = "jwt")
  public JwtConfig getJwtConfig() {
    return new JwtConfig();
  }

  @Bean
  public JwtUtils getJwtUtils(JwtConfig config) {
    return new JwtUtils(config);
  }

  @Bean
  public PasswordEncoder getPasswordEncoder() {
    return new BCryptPasswordEncoder();
  }

}
