package com.mediscreen.authserver.config;

import com.mediscreen.common.domain.JwtPrincipal;
import com.mediscreen.common.exception.JwtValidationException;
import com.mediscreen.common.utils.JwtUtils;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.lang.NonNull;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

/**
 * Filter class to check if an Authorization header with Jwt is present, and in that case
 * authenticate the owner of the token.
 */
public class JwtTokenFilter extends OncePerRequestFilter {

  private static final Logger LOGGER = LoggerFactory.getLogger(JwtTokenFilter.class);
  private final JwtUtils jwtUtils;

  public JwtTokenFilter(JwtUtils jwtUtils) {
    this.jwtUtils = jwtUtils;
  }

  @Override
  protected void doFilterInternal(@NonNull HttpServletRequest request,
                                  @NonNull HttpServletResponse response,
                                  @NonNull FilterChain filterChain)
      throws ServletException, IOException {

    Optional<String> extractedToken = extractToken(request);
    if (extractedToken.isPresent()) {
      String token = extractedToken.get();
      try {
        JwtPrincipal principal = jwtUtils.getPrincipal(token);
        Authentication authentication = new UsernamePasswordAuthenticationToken(principal, null,
            List.of(new SimpleGrantedAuthority("ROLE_" + principal.getRole())));
        SecurityContextHolder.getContext().setAuthentication(authentication);
      } catch (JwtValidationException ex) {
        LOGGER.warn("Provided JWT invalid: {}", ex.getMessage());
        SecurityContextHolder.clearContext();
      }
    }
    filterChain.doFilter(request, response);
  }

  /**
   * Extract the token from the request header and return an Optional. If no token are present in
   * the header or the token is malformed, an empty optional is returned.
   *
   * @param request containing the token
   * @return Optional of the token
   */
  private Optional<String> extractToken(HttpServletRequest request) {
    String authHeader = request.getHeader("Authorization");
    if (authHeader == null || !authHeader.startsWith("Bearer ")) {
      return Optional.empty();
    }
    return Optional.of(authHeader.substring(7));
  }

}
