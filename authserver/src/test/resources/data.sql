INSERT INTO `user` (`user_id`, `firstname`, `lastname`, `email`, `password`, `role`)
  values (1, 'Admin', 'Principal', 'admin@abernathy.com', '$2a$10$LyalOVgvR9fcTvjCT/SBleVB6ra85MRkJZ4DlHPPimxf/7y.dA4uS', 'ADMIN');

INSERT INTO `user` (`user_id`, `firstname`, `lastname`, `email`, `password`, `role`)
  values (2, 'Doctor', 'Test', 'doctor@abernathy.com', '$2a$10$LyalOVgvR9fcTvjCT/SBleVB6ra85MRkJZ4DlHPPimxf/7y.dA4uS', 'DOCTOR');

INSERT INTO `user` (`user_id`, `firstname`, `lastname`, `email`, `password`, `role`)
  values (3, 'Guest', 'Test', 'guest@abernathy.com', '$2a$10$LyalOVgvR9fcTvjCT/SBleVB6ra85MRkJZ4DlHPPimxf/7y.dA4uS', 'GUEST');


INSERT INTO `token` (`token_id`, `expiry_date`, `token`, `user_id`)
  values (1, '2100-01-01 00:00:00', 'TestTokenAdmin', 1);

INSERT INTO `token` (`token_id`, `expiry_date`, `token`, `user_id`)
  values (2, '2100-01-01 00:00:00', 'TestTokenGuest', 3);