package com.mediscreen.authserver.utils;

import com.mediscreen.authserver.domain.RefreshToken;
import com.mediscreen.authserver.domain.User;
import com.mediscreen.common.domain.Role;
import org.springframework.test.util.ReflectionTestUtils;

/**
 * Utility class used for testing
 */
public class TestUtils {

  /**
   * Create an instance of User for testing purposes and manually set the fields usually handled
   * by the framework and database.
   *
   * @param userId    id of the user
   * @param firstname firstname of the user
   * @param lastname  lastname of the user
   * @param email     email of the user
   * @param password  password of the user
   * @param role      role assign to the user
   * @return user
   */
  public static User createUser(int userId, String firstname, String lastname, String email,
                                String password, Role role) {
    User user = new User(firstname, lastname, email, password);
    user.setRole(role);
    ReflectionTestUtils.setField(user, "userId", userId);
    return user;
  }

  /**
   * Mock the creation of a record in repository by manually setting id.
   *
   * @param user user on which apply creation
   * @return user
   */
  public static User mockCreateUser(User user) {
    ReflectionTestUtils.setField(user, "userId", 1);
    return user;
  }

  /**
   * Mock the creation of a refresh token in repository by manually setting id.
   *
   * @param token Refresh token on which apply creation
   * @return refresh token
   */
  public static RefreshToken mockCreateToken(RefreshToken token) {
    ReflectionTestUtils.setField(token, "id", 1);
    return token;
  }

}
