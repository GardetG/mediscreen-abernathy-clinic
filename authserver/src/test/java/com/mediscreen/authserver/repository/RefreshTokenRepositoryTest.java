package com.mediscreen.authserver.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.mediscreen.authserver.domain.RefreshToken;
import com.mediscreen.authserver.domain.User;
import com.mediscreen.authserver.utils.TestUtils;
import com.mediscreen.common.domain.Role;
import java.time.Instant;
import java.util.Optional;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.DataIntegrityViolationException;

@DataJpaTest
class RefreshTokenRepositoryTest {

  @Autowired
  private RefreshTokenRepository refreshTokenRepository;

  @DisplayName("Find refresh token by user should return an optional of the refresh token")
  @Test
  void findByUserTest() {
    // GIVEN
    User user = TestUtils.createUser(1, "Admin", "Principal",
        "admin@abernathy.com", "password", Role.ADMIN);

    // WHEN
    Optional<RefreshToken> actualToken = refreshTokenRepository.findByUser(user);

    //Then
    assertThat(actualToken).isPresent();
  }

  @DisplayName("Find refresh token by user when no token found should return an empty optional")
  @Test
  void findByUserWhenNotFoundTest() {
    // GIVEN
    User user = TestUtils.createUser(2, "Doctor", "Test",
        "doctor@abernathy.com", "password", Role.DOCTOR);

    // WHEN
    Optional<RefreshToken> actualToken = refreshTokenRepository.findByUser(user);

    //Then
    assertThat(actualToken).isEmpty();
  }

  @DisplayName("Find refresh token by token should return an optional of the refresh token")
  @Test
  void findByTokenTest() {
    // GIVEN
    String token = "TestTokenAdmin";

    // WHEN
    Optional<RefreshToken> actualToken = refreshTokenRepository.findByToken(token);

    //Then
    assertThat(actualToken).isPresent();
  }

  @DisplayName("Find refresh token by token when no token found should return an empty optional")
  @Test
  void findByTokenWhenNotFoundTest() {
    // GIVEN
    String token = "NotExistingToken";

    // WHEN
    Optional<RefreshToken> actualToken = refreshTokenRepository.findByToken(token);

    //Then
    assertThat(actualToken).isEmpty();
  }

  @DisplayName("Save a refresh token should save it and return the saved token with a defined id")
  @Test
  void saveTest() {
    // GIVEN
    User user = TestUtils.createUser(2, "Doctor", "Test",
        "doctor@abernathy.com", "password", Role.DOCTOR);
    RefreshToken token = new RefreshToken(user, "Token", Instant.now().minusSeconds(60));

    // WHEN
    RefreshToken actualToken = refreshTokenRepository.save(token);

    // THEN
    assertThat(actualToken.getId()).isNotZero();
  }

  @DisplayName("Save a refresh token violating unique constraint should throw an exception")
  @Test
  void saveWhenAlreadyExistsTest() {
    // GIVEN
    User user = TestUtils.createUser(1, "Admin", "Principal",
        "admin@abernathy.com", "password", Role.ADMIN);
    RefreshToken token = new RefreshToken(user, "token", Instant.now().minusSeconds(60));

    // WHEN
    assertThatThrownBy(() -> refreshTokenRepository.save(token))

        // THEN
        .isInstanceOf(DataIntegrityViolationException.class);
  }

  @DisplayName("Save a refresh token with a not persisted user should throw an exception")
  @Test
  void saveWhenUserNotPersistedTest() {
    // GIVEN
    User user = TestUtils.createUser(4, "User", "NonPersisted",
        "user@mail.com", "password", Role.GUEST);
    RefreshToken token = new RefreshToken(user, "token", Instant.now().minusSeconds(60));

    // WHEN
    assertThatThrownBy(() -> refreshTokenRepository.save(token))

        // THEN
        .isInstanceOf(DataIntegrityViolationException.class);
  }

  @DisplayName("Save a refresh token with a not persisted user should throw an exception")
  @Test
  void deleteTest() {
    // GIVEN
    RefreshToken existingToken = refreshTokenRepository.findByToken("TestTokenGuest")
        .orElseThrow(() -> new IllegalStateException("Can't find test data"));

    // WHEN
    refreshTokenRepository.delete(existingToken);

    // THEN
    assertThat(refreshTokenRepository.findByToken("TestTokenDoctor")).isEmpty();
  }

}