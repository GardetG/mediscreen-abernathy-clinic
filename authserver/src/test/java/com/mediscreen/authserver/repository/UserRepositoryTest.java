package com.mediscreen.authserver.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.mediscreen.authserver.domain.User;
import java.util.Optional;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

@DataJpaTest
class UserRepositoryTest {

  @Autowired
  private UserRepository userRepository;

  @DisplayName("Find all users should return a page containing all patients")
  @Test
  void findAllTest() {
    // Given
    Pageable pageable = PageRequest.of(0, 5);

    // When
    Page<User> actualPatients = userRepository.findAll(pageable);

    //Then
    assertThat(actualPatients.getContent()).isNotEmpty();
  }

  @DisplayName("Find user by id should return an optional of the patient")
  @Test
  void findByIdTest() {
    // Given
    int id = 1;

    // When
    Optional<User> actualPatient = userRepository.findById(id);

    //Then
    assertThat(actualPatient).isPresent();
  }

  @DisplayName("Find user by id when no user found should return an empty optional")
  @Test
  void findByIdWhenNotFoundTest() {
    // Given
    int id = 9;

    // When
    Optional<User> actualUser = userRepository.findById(9);

    //Then
    assertThat(actualUser).isEmpty();
  }

  @DisplayName("Find user by email should return an optional of the patient")
  @Test
  void findByEmailTest() {
    // Given
    String email = "admin@abernathy.com";

    // When
    Optional<User> actualPatient = userRepository.findByEmail(email);

    //Then
    assertThat(actualPatient).isPresent();
  }

  @DisplayName("Find user by email when no user found should return an empty optional")
  @Test
  void findByEmailWhenNotFoundTest() {
    // Given
    String email = "NonExisting@mail.com";

    // When
    Optional<User> actualUser = userRepository.findByEmail(email);

    //Then
    assertThat(actualUser).isEmpty();
  }

  @DisplayName("Check patient existence by email when existing should return true")
  @Test
  void existsByEmailTest() {
    // Given
    String email = "admin@abernathy.com";

    // When
    boolean result = userRepository.existsByEmail(email);

    //Then
    assertThat(result).isTrue();
  }

  @DisplayName("Check user existence by email when not existing should return false")
  @Test
  void notExistsByEmailTest() {
    // Given
    String email = "NonExisting@mail.com";

    // When
    boolean result = userRepository.existsByEmail(email);

    //Then
    assertThat(result).isFalse();
  }

  @DisplayName("Save a user should save it and return the saved user with a defined id")
  @Test
  void saveTest() {
    // Given
    User user = new User("User", "Test",  "test@mail.com", "password");

    // When
    User actualUser = userRepository.save(user);

    // Then
    assertThat(actualUser)
        .usingRecursiveComparison().ignoringFields("userId").isEqualTo(user);
    assertThat(actualUser.getUserId()).isNotZero();
  }

  @DisplayName("Save an user violating unique constraint should throw an exception")
  @Test
  void saveWhenAlreadyExistsTest() {
    // Given
    User user = new User("User", "Test",  "admin@abernathy.com", "password");

    // When
    assertThatThrownBy(() -> userRepository.save(user))

        // Then
        .isInstanceOf(DataIntegrityViolationException.class);
  }

}