package com.mediscreen.authserver;

import static org.assertj.core.api.Assertions.assertThat;

import com.mediscreen.authserver.controller.AuthController;
import com.mediscreen.authserver.controller.UserController;
import com.mediscreen.authserver.repository.RefreshTokenRepository;
import com.mediscreen.authserver.repository.UserRepository;
import com.mediscreen.authserver.service.AuthService;
import com.mediscreen.authserver.service.RefreshTokenService;
import com.mediscreen.authserver.service.UserService;
import com.mediscreen.common.utils.JwtUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootTest
class AuthServerApplicationTest {

  @Autowired
  private UserRepository userRepository;
  @Autowired
  private RefreshTokenRepository refreshTokenRepository;
  @Autowired
  private UserService userService;
  @Autowired
  private AuthService authService;
  @Autowired
  private RefreshTokenService refreshTokenService;
  @Autowired
  private AuthController authController;
  @Autowired
  private UserController userController;
  @Autowired
  private JwtUtils jwtUtils;
  @Autowired
  private PasswordEncoder passwordEncoder;

  @Test
  void contextLoads() {
    assertThat(userRepository).isNotNull();
    assertThat(refreshTokenRepository).isNotNull();
    assertThat(userService).isNotNull();
    assertThat(authService).isNotNull();
    assertThat(refreshTokenService).isNotNull();
    assertThat(authController).isNotNull();
    assertThat(userController).isNotNull();
    assertThat(jwtUtils).isNotNull();
  }

}