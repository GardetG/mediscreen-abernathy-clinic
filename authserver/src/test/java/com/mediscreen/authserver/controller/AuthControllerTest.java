package com.mediscreen.authserver.controller;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.mediscreen.authserver.dto.LoginRequestDto;
import com.mediscreen.authserver.dto.LoginResponseDto;
import com.mediscreen.authserver.exception.RefreshTokenException;
import com.mediscreen.authserver.service.AuthService;
import com.mediscreen.common.domain.Role;
import com.mediscreen.common.utils.JwtUtils;
import org.json.JSONObject;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(controllers = AuthController.class)
class AuthControllerTest {

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private AuthService authService;
  @MockBean
  private UserDetailsService userDetailsService;
  @MockBean
  private PasswordEncoder passwordEncoder;
  @MockBean
  private JwtUtils jwtUtils;

  @DisplayName("POST successful login request should return 200 with user info and tokens")
  @Test
  void LoginTest() throws Exception {
    // GIVEN
    JSONObject jsonObject = new JSONObject()
        .put("username", "user@mail.com")
        .put("password", "P@ssW0rd");
    when(authService.login(any(LoginRequestDto.class)))
        .thenReturn(new LoginResponseDto(1, "User", "Test", Role.GUEST, "JWT Token", "Refresh Token"));

    // WHEN
    mockMvc.perform(post("/auth/login")
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonObject.toString()))

        // THEN
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.userId", is(1)))
        .andExpect(jsonPath("$.firstname", is("User")))
        .andExpect(jsonPath("$.lastname", is("Test")))
        .andExpect(jsonPath("$.role", is(Role.GUEST.toString())))
        .andExpect(jsonPath("$.token", is("JWT Token")))
        .andExpect(jsonPath("$.refreshToken", is("Refresh Token")));
    verify(authService, times(1)).login(any(LoginRequestDto.class));
  }

  @DisplayName("POST failed login request should return 401")
  @Test
  void LoginFailedTest() throws Exception {
    // GIVEN
    JSONObject jsonObject = new JSONObject()
        .put("username", "user@mail.com")
        .put("password", "P@ssW0rd");
    when(authService.login(any(LoginRequestDto.class)))
        .thenThrow(new BadCredentialsException("Bad credentials"));

    // WHEN
    mockMvc.perform(post("/auth/login")
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonObject.toString()))

        // THEN
        .andExpect(status().isUnauthorized())
        .andExpect(jsonPath("$", is("Bad credentials")));
    verify(authService, times(1)).login(any(LoginRequestDto.class));
  }

  @DisplayName("POST successful refresh request should return 200 with user info and tokens")
  @Test
  void refreshTest() throws Exception {
    // GIVEN
    JSONObject jsonObject = new JSONObject().put("refreshToken", "Token");
    when(authService.refresh(anyString()))
        .thenReturn(new LoginResponseDto(1, "User", "Test", Role.GUEST, "JWT Token", "Refresh Token"));

    // WHEN
    mockMvc.perform(post("/auth/refresh")
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonObject.toString()))

        // THEN
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.userId", is(1)))
        .andExpect(jsonPath("$.firstname", is("User")))
        .andExpect(jsonPath("$.lastname", is("Test")))
        .andExpect(jsonPath("$.role", is(Role.GUEST.toString())))
        .andExpect(jsonPath("$.token", is("JWT Token")))
        .andExpect(jsonPath("$.refreshToken", is("Refresh Token")));
    verify(authService, times(1)).refresh("Token");
  }

  @DisplayName("POST failed refresh request should return 401")
  @Test
  void refreshFailedTest() throws Exception {
    // GIVEN
    JSONObject jsonObject = new JSONObject().put("refreshToken", "Invalid");
    when(authService.refresh(anyString()))
        .thenThrow(new RefreshTokenException("Refresh token can't be verified."));

    // WHEN
    mockMvc.perform(post("/auth/refresh")
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonObject.toString()))

        // THEN
        .andExpect(status().isUnauthorized())
        .andExpect(jsonPath("$", is("Refresh token can't be verified.")));
    verify(authService, times(1)).refresh("Invalid");
  }

}
