package com.mediscreen.authserver.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.mediscreen.authserver.domain.User;
import com.mediscreen.authserver.dto.UserDto;
import com.mediscreen.authserver.exception.EmailAlreadyUsedException;
import com.mediscreen.authserver.exception.UserNotFoundException;
import com.mediscreen.authserver.service.UserService;
import com.mediscreen.authserver.utils.TestUtils;
import com.mediscreen.common.domain.Role;
import com.mediscreen.common.utils.JwtUtils;
import java.util.List;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(controllers = UserController.class)
class UserControllerTest {

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private UserService userService;
  @MockBean
  private UserDetailsService userDetailsService;
  @MockBean
  private PasswordEncoder passwordEncoder;
  @MockBean
  private JwtUtils jwtUtils;

  @Captor
  private ArgumentCaptor<UserDto> UserCaptor;

  private static User DOCTOR;
  private static User ADMIN;

  @BeforeAll
  static void setUp() {
    DOCTOR = TestUtils.createUser(1, "Doctor", "Test", "doctor@mail.com", "password", Role.DOCTOR);
    ADMIN = TestUtils.createUser(2, "Admin", "Test", "admin@mail.com", "password", Role.ADMIN);
  }

  @DisplayName("GET page of all users as Admin should return 200 with page of users")
  @Test
  void getAllUsersPageAsAdminTest() throws Exception {
    // GIVEN
    Pageable pageable = PageRequest.of(0, 5);
    Page<UserDto> usersPage = new PageImpl<>(List.of(
        new UserDto(1, "User1", "Test", "user1@test.com", null, Role.ADMIN),
        new UserDto(2, "User2", "Test", "user2@test.com", null, Role.DOCTOR)
    ), pageable, 2);
    when(userService.getAllUsersPage(any(Pageable.class))).thenReturn(usersPage);

    // WHEN
    mockMvc.perform(get("/auth/account?page=0&size=5")
            .with(user(ADMIN)))

        // THEN
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.content", hasSize(2)))
        .andExpect(jsonPath("$.content[0].userId", is(1)))
        .andExpect(jsonPath("$.content[0].firstname", is("User1")))
        .andExpect(jsonPath("$.content[1].userId", is(2)))
        .andExpect(jsonPath("$.content[1].firstname", is("User2")));
    verify(userService, times(1)).getAllUsersPage(pageable);
  }

  @DisplayName("GET page of all users as non Admin should return 403")
  @Test
  void getAllUsersPageAsNonAdminTest() throws Exception {
    // WHEN
    mockMvc.perform(get("/auth/account?page=0&size=5")
            .with(user(DOCTOR)))

        // THEN
        .andExpect(status().isForbidden());
  }

  @DisplayName("")
  @Test
  void getUserByIdAsAdminTest() throws Exception {
    // GIVEN
    when(userService.getUserById(anyInt()))
        .thenReturn(new UserDto(1, "User", "Test", "test@mail.com", null, Role.DOCTOR));

    // WHEN
    mockMvc.perform(get("/auth/account/1")
            .with(user(ADMIN)))

        // THEN
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.userId", is(1)))
        .andExpect(jsonPath("$.firstname", is("User")))
        .andExpect(jsonPath("$.lastname", is("Test")))
        .andExpect(jsonPath("$.email", is("test@mail.com")))
        .andExpect(jsonPath("$.role", is("DOCTOR")))
        .andExpect(jsonPath("$.password").doesNotExist());
    verify(userService, times(1)).getUserById(1);
  }

  @DisplayName("")
  @Test
  void getUserByIdAsUserTest() throws Exception {
    // GIVEN
    when(userService.getUserById(anyInt()))
        .thenReturn(new UserDto(1, "User", "Test", "test@test.com", null, Role.DOCTOR));

    // WHEN
    mockMvc.perform(get("/auth/account/1")
            .with(user(DOCTOR)))

        // THEN
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.userId", is(1)));
    verify(userService, times(1)).getUserById(1);
  }

  @DisplayName("")
  @Test
  void getUserByIdAsOtherUserTest() throws Exception {
    // WHEN
    mockMvc.perform(get("/auth/account/2")
            .with(user(DOCTOR)))

        // THEN
        .andExpect(status().isForbidden());
  }

  @DisplayName("")
  @Test
  void getUserByIdWhenNotFoundTest() throws Exception {
    // GIVEN
    doThrow(new UserNotFoundException("User not found", 9))
        .when(userService).getUserById(anyInt());

    // WHEN
    mockMvc.perform(get("/auth/account/9")
            .with(user(ADMIN)))

        // THEN
        .andExpect(status().isNotFound())
        .andExpect(jsonPath("$", is("User not found")));
    verify(userService, times(1)).getUserById(9);
  }

  @DisplayName("POST a registration should return 201")
  @Test
  void registerTest() throws Exception {
    // GIVEN
    JSONObject jsonObject = new JSONObject().put("firstname", "User")
        .put("lastname", "Test")
        .put("email", "test@mail.com")
        .put("password", "p@ssW0rd");
    UserDto userDto = new UserDto(0, "User", "Test", "test@mail.com", "p@ssW0rd", null);
    when(userService.register(any(UserDto.class)))
        .thenReturn(new UserDto(1, "User", "Test", "test@mail.com", "EncryptedPassword", Role.ADMIN));

    // WHEN
    mockMvc.perform(post("/auth/account/register")
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonObject.toString()))

        // THEN
        .andExpect(status().isCreated())
        .andExpect(jsonPath("$.userId", is(1)))
        .andExpect(jsonPath("$.firstname", is("User")))
        .andExpect(jsonPath("$.lastname", is("Test")))
        .andExpect(jsonPath("$.email", is("test@mail.com")))
        .andExpect(jsonPath("$.role", is("ADMIN")))
        .andExpect(jsonPath("$.password").doesNotExist());
    verify(userService, times(1)).register(UserCaptor.capture());
    assertThat(UserCaptor.getValue()).usingRecursiveComparison().isEqualTo(userDto);
  }

  @DisplayName("POST an invalid registration dto should return 422")
  @Test
  void registerWhenInvalidTest() throws Exception {
    // GIVEN
    JSONObject jsonObject = new JSONObject()
        .put("firstname", "")
        .put("lastname", "LastNameTooLongToBeValid")
        .put("email", "test")
        .put("password", "password");

    // WHEN
    mockMvc.perform(post("/auth/account/register")
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonObject.toString()))

        // THEN
        .andExpect(status().isUnprocessableEntity())
        .andExpect(jsonPath("$.firstname", is("must not be blank")))
        .andExpect(jsonPath("$.lastname", is("size must be between 0 and 20")))
        .andExpect(jsonPath("$.email", is("must be a well-formed email address")))
        .andExpect(jsonPath("$.password", is("must contains one capital letter, one digit and one special characters and have at least 8 characters")));
    verify(userService, times(0)).register(any(UserDto.class));
  }

  @DisplayName("POST a registration when email already used should return 409")
  @Test
  void registerWhenAlreadyExistsTest() throws Exception {
    // GIVEN
    JSONObject jsonObject = new JSONObject()
        .put("firstname", "User")
        .put("lastname", "Test")
        .put("email", "AlreadyExists@mail.com")
        .put("password", "p@ssW0rd");
    when(userService.register(any(UserDto.class)))
        .thenThrow(new EmailAlreadyUsedException("This email is already registered", "AlreadyExists@mail.com"));

    // WHEN
    mockMvc.perform(post("/auth/account/register")
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonObject.toString()))

        // THEN
        .andExpect(status().isConflict())
        .andExpect(jsonPath("$", is("This email is already registered")));
    verify(userService, times(1)).register(any(UserDto.class));
  }

  @DisplayName("PATCH reset user password as Admin should return 204")
  @Test
  void resetPasswordTest() throws Exception {
    // GIVEN
    JSONObject jsonObject = new JSONObject().put("password", "p@ssW0rd");

    // WHEN
    mockMvc.perform(patch("/auth/account/1/resetPassword")
            .with(user(ADMIN))
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonObject.toString()))

        // THEN
        .andExpect(status().isNoContent());
    verify(userService, times(1)).changePassword(1, "p@ssW0rd");
  }

  @DisplayName("PATCH reset user password as User should return 204")
  @Test
  void resetPasswordAsUserTest() throws Exception {
    // GIVEN
    JSONObject jsonObject = new JSONObject().put("password", "p@ssW0rd");

    // WHEN
    mockMvc.perform(patch("/auth/account/1/resetPassword")
            .with(user(DOCTOR))
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonObject.toString()))

        // THEN
        .andExpect(status().isNoContent());
  }

  @DisplayName("PATCH reset user password as an other User should return 403")
  @Test
  void resetPasswordOfOtherUserTest() throws Exception {
    // GIVEN
    JSONObject jsonObject = new JSONObject().put("password", "p@ssW0rd");

    // WHEN
    mockMvc.perform(patch("/auth/account/2/resetPassword")
            .with(user(DOCTOR))
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonObject.toString()))

        // THEN
        .andExpect(status().isForbidden());
  }

  @DisplayName("PATCH reset user password with invalid password should return 422")
  @Test
  void resetPasswordWhenInvalidTest() throws Exception {
    // GIVEN
    JSONObject jsonObject = new JSONObject().put("password", "password");

    // WHEN
    mockMvc.perform(patch("/auth/account/1/resetPassword")
            .with(user(ADMIN))
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonObject.toString()))

        // THEN
        .andExpect(status().isUnprocessableEntity())
        .andExpect(jsonPath("$.password",
            is("must contains one capital letter, one digit and one special characters and have at least 8 characters")));
    verify(userService, times(0)).register(any(UserDto.class));
  }

  @DisplayName("PATCH reset user password when user not found should return 404")
  @Test
  void resetPasswordWhenNotFoundTest() throws Exception {
    // GIVEN
    JSONObject jsonObject = new JSONObject().put("password", "p@ssW0rd");
    doThrow(new UserNotFoundException("User not found", 9))
        .when(userService).changePassword(anyInt(), anyString());

    // WHEN
    mockMvc.perform(patch("/auth/account/9/resetPassword")
            .with(user(ADMIN))
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonObject.toString()))

        // THEN
        .andExpect(status().isNotFound())
        .andExpect(jsonPath("$", is("User not found")));
    verify(userService, times(1)).changePassword(9, "p@ssW0rd");
  }

  @DisplayName("PATCH assign role to user as Admin should return 204")
  @Test
  void assignRoleTest() throws Exception {
    // GIVEN
    JSONObject jsonObject = new JSONObject().put("role", "DOCTOR");

    // WHEN
    mockMvc.perform(patch("/auth/account/1/assignRole")
            .with(user(ADMIN))
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonObject.toString()))

        // THEN
        .andExpect(status().isNoContent());
    verify(userService, times(1)).assignRole(1, Role.DOCTOR);
  }

  @DisplayName("PATCH assign role to user as non Admin should return 403")
  @Test
  void assignRoleWhenNotAdminTest() throws Exception {
    // GIVEN
    JSONObject jsonObject = new JSONObject().put("role", "DOCTOR");

    // WHEN
    mockMvc.perform(patch("/auth/account/1/assignRole")
            .with(user(DOCTOR))
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonObject.toString()))

        // THEN
        .andExpect(status().isForbidden());
  }

  @DisplayName("PATCH assign role to user with invalid role should return 422")
  @Test
  void assignRoleWhenInvalidTest() throws Exception {
    // GIVEN
    JSONObject jsonObject = new JSONObject().put("role", "");

    // WHEN
    mockMvc.perform(patch("/auth/account/1/assignRole")
            .with(user(ADMIN))
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonObject.toString()))

        // THEN
        .andExpect(status().isUnprocessableEntity())
        .andExpect(jsonPath("$.role", is("must be a valid role")));
    verify(userService, times(0)).assignRole(anyInt(), any(Role.class));
  }

  @DisplayName("PATCH assign role to user when user not found should return 404")
  @Test
  void assignRoleWhenNotFoundTest() throws Exception {
    // GIVEN
    JSONObject jsonObject = new JSONObject().put("role", "DOCTOR");
    doThrow(new UserNotFoundException("User not found", 9))
        .when(userService).assignRole(anyInt(), any(Role.class));

    // WHEN
    mockMvc.perform(patch("/auth/account/9/assignRole")
            .with(user(ADMIN))
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonObject.toString()))

        // THEN
        .andExpect(status().isNotFound())
        .andExpect(jsonPath("$", is("User not found")));
    verify(userService, times(1)).assignRole(9, Role.DOCTOR);
  }

}