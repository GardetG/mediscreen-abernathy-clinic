package com.mediscreen.authserver.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.mediscreen.authserver.utils.TestUtils;
import com.mediscreen.common.domain.Role;
import com.mediscreen.authserver.domain.User;
import com.mediscreen.authserver.dto.UserDto;
import com.mediscreen.authserver.exception.EmailAlreadyUsedException;
import com.mediscreen.authserver.exception.UserNotFoundException;
import com.mediscreen.authserver.repository.UserRepository;
import com.mediscreen.authserver.service.impl.UserServiceImpl;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootTest
class UserServiceTest {

  @Autowired
  private UserServiceImpl userService;

  @MockBean
  private UserRepository userRepository;
  @MockBean
  private PasswordEncoder passwordEncoder;

  @Captor
  private ArgumentCaptor<User> userCaptor;

  @DisplayName("Get page of all users return page of users")
  @Test
  void getAllUsersPageTest() {
    // Given
    Pageable pageable = PageRequest.of(0, 5);
    Page<User> usersPage = new PageImpl<>(List.of(
        TestUtils.createUser(1,"User1", "Test", "user1@test.com", "password", Role.ADMIN),
        TestUtils.createUser(2,"User2", "Test", "user2@test.com", "password", Role.DOCTOR)
    ), pageable, 2);
    when(userRepository.findAll(any(Pageable.class))).thenReturn(usersPage);

    // When
    Page<UserDto> actualUsersPage = userService.getAllUsersPage(pageable);

    // Then
    assertThat(actualUsersPage).usingRecursiveComparison()
        .isEqualTo(new PageImpl<>(List.of(
            new UserDto(1,"User1", "Test", "user1@test.com", null, Role.ADMIN),
            new UserDto(2,"User2", "Test", "user2@test.com", null, Role.DOCTOR)
        ), pageable, 2));
    verify(userRepository, times(1)).findAll(pageable);
  }

  @DisplayName("Get page of all users no user found should return an empty page")
  @Test
  void getAllUsersPageWhenEmptyTest() {
    // Given
    Pageable pageable = PageRequest.of(0, 5);
    when(userRepository.findAll(any(Pageable.class))).thenReturn(Page.empty());

    // When
    Page<UserDto> actualPatientsResume = userService.getAllUsersPage(pageable);

    // Then
    assertThat(actualPatientsResume).isEmpty();
    verify(userRepository, times(1)).findAll(pageable);
  }

  @DisplayName("Get user by id should return dto of the user")
  @Test
  void getUserByIdTest() throws UserNotFoundException {
    // Given
    User user = new User("User", "Test","test@mail.com", "actualPassword");
    when(userRepository.findById(anyInt())).thenReturn(Optional.of(user));

    // When
    UserDto actualDto = userService.getUserById(1);

    // Then
    assertThat(actualDto).usingRecursiveComparison()
        .isEqualTo(new UserDto(0, "User", "Test","test@mail.com", null, Role.GUEST ));
    verify(userRepository, times(1)).findById(1);
  }

  @DisplayName("Get user by id when not found should throw an exception")
  @Test
  void getUserByIdWhenNotFoundTest() {
    // Given
    when(userRepository.findById(anyInt())).thenReturn(Optional.empty());

    // When
    assertThatThrownBy(() -> userService.getUserById(9))

        // Then
        .isInstanceOf(UserNotFoundException.class)
        .hasMessageContaining("User not found");
    verify(userRepository, times(1)).findById(9);
  }

  @DisplayName("Load a user by username should return user details")
  @Test
  void loadUserByUsernameTest() {
    // Given
    String email = "test@mail.com";
    User user = new User("User", "Test", email, "password");
    when(userRepository.findByEmail(anyString())).thenReturn(Optional.of(user));

    // When
    UserDetails actualUser = userService.loadUserByUsername(email);

    // Then
    assertThat(actualUser).usingRecursiveComparison().isEqualTo(user);
    verify(userRepository, times(1)).findByEmail(email);
  }

  @DisplayName("Load a user by username when not found should throw an exception")
  @Test
  void loadUserByUsernameWhenNotFoundTest() {
    // Given
    String email = "NonExisting@mail.com";
    when(userRepository.findByEmail(anyString())).thenReturn(Optional.empty());

    // When
    assertThatThrownBy(() -> userService.loadUserByUsername(email))

        // Then
        .isInstanceOf(UsernameNotFoundException.class);
    verify(userRepository, times(1)).findByEmail(email);
  }

  @DisplayName("Register a user should save new user entity")
  @Test
  void registerTest() throws EmailAlreadyUsedException {
    // Given
    UserDto userDto = new UserDto(0,"User", "Test","test@mail.com", "password", null);
    when(userRepository.existsByEmail(anyString())).thenReturn(false);
    when(passwordEncoder.encode(anyString())).thenReturn("EncodedPassword");
    doAnswer(invocation -> TestUtils.mockCreateUser(invocation.getArgument(0)))
        .when(userRepository).save(any(User.class));

    // When
    UserDto registeredUser = userService.register(userDto);

    // Then
    assertThat(registeredUser).usingRecursiveComparison().
        isEqualTo(new UserDto(1, "User", "Test", "test@mail.com", null, Role.GUEST));
    verify(userRepository, times(1)).existsByEmail("test@mail.com");
    verify(passwordEncoder, times(1)).encode("password");
    verify(userRepository, times(1)).save(userCaptor.capture());
    assertThat(userCaptor.getValue()).usingRecursiveComparison()
        .isEqualTo(TestUtils.createUser(1, "User", "Test", "test@mail.com", "EncodedPassword", Role.GUEST));
  }

  @DisplayName("Register a user with already used email should throw an exception")
  @Test
  void registerUserWHenEmailAlreadyExistsTest() {
    // Given
    UserDto userDto = new UserDto(0,"User", "Test","AlreadyUsed@mail.com", "password", null);
    when(userRepository.existsByEmail(anyString())).thenReturn(true);

    // When
    assertThatThrownBy(() -> userService.register(userDto))

        // Then
        .isInstanceOf(EmailAlreadyUsedException.class)
        .hasMessageContaining("This email is already registered");
    verify(userRepository, times(1)).existsByEmail("AlreadyUsed@mail.com");
    verify(userRepository, times(0)).save(any(User.class));
  }

  @DisplayName("Reset the user password should change the password")
  @Test
  void resetPasswordTest() throws UserNotFoundException {
    // Given
    User user = new User("User", "Test","test@mail.com", "actualPassword");
    when(userRepository.findById(anyInt())).thenReturn(Optional.of(user));
    when(passwordEncoder.encode(anyString())).thenReturn("EncodedPassword");

    // When
    userService.changePassword(1, "NewPassword");

    // Then
    assertThat(user.getPassword()).isEqualTo("EncodedPassword");
    verify(userRepository, times(1)).findById(1);
  }

  @DisplayName("Reset the password of a not found user should throw an exception")
  @Test
  void resetPasswordWhenNotFoundTest() {
    // Given
    when(userRepository.findById(anyInt())).thenReturn(Optional.empty());

    // When
    assertThatThrownBy(() -> userService.changePassword(9,"password"))

        // Then
        .isInstanceOf(UserNotFoundException.class)
        .hasMessageContaining("User not found");
    verify(userRepository, times(1)).findById(9);
  }

  @DisplayName("Assign a role to the user should change the user's role")
  @Test
  void assignRoleTest() throws UserNotFoundException {
    // Given
    User user = new User("User", "Test","test@mail.com", "actualPassword");
    when(userRepository.findById(anyInt())).thenReturn(Optional.of(user));

    // When
    userService.assignRole(1, Role.DOCTOR);

    // Then
    assertThat(user.getRole()).isEqualTo(Role.DOCTOR);
    verify(userRepository, times(1)).findById(1);
  }

  @DisplayName("Assign a role to a not found user should throw an exception")
  @Test
  void assignRoleWhenNotFoundTest() {
    // Given
    when(userRepository.findById(anyInt())).thenReturn(Optional.empty());

    // When
    assertThatThrownBy(() -> userService.assignRole(9, Role.DOCTOR))

        // Then
        .isInstanceOf(UserNotFoundException.class)
        .hasMessageContaining("User not found");
    verify(userRepository, times(1)).findById(9);
  }

}