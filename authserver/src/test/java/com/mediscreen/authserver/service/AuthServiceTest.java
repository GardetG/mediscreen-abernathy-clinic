package com.mediscreen.authserver.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.mediscreen.authserver.domain.RefreshToken;
import com.mediscreen.authserver.domain.User;
import com.mediscreen.authserver.dto.LoginRequestDto;
import com.mediscreen.authserver.dto.LoginResponseDto;
import com.mediscreen.authserver.exception.RefreshTokenException;
import com.mediscreen.authserver.utils.TestUtils;
import com.mediscreen.common.domain.JwtPrincipal;
import com.mediscreen.common.domain.Role;
import com.mediscreen.common.utils.JwtUtils;
import java.time.Instant;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

@SpringBootTest
class AuthServiceTest {

  @Autowired
  private AuthService authService;

  @MockBean
  private AuthenticationManager authenticationManager;
  @MockBean
  private JwtUtils jwtUtils;
  @MockBean
  private RefreshTokenService refreshTokenService;

  @Captor
  private ArgumentCaptor<JwtPrincipal> jwtPrincipalCaptor;

  @DisplayName("Login successfully a user by username and password should return user info and tokens")
  @Test
  void loginTest() {
    // Given
    User user = TestUtils.createUser(1, "User", "Test", "doctor@mail.com", "password", Role.DOCTOR);
    when(authenticationManager.authenticate(any(Authentication.class)))
        .thenReturn(new TestingAuthenticationToken(user, "password"));
    when(jwtUtils.generateToken(any(JwtPrincipal.class))).thenReturn("Jwt Token");
    when(refreshTokenService.createRefreshToken(any(User.class)))
        .thenReturn(new RefreshToken(user, "Refresh Token", Instant.now().plusSeconds(60)));
    LoginRequestDto request = new LoginRequestDto("doctor@mail.com", "password");

    // When
    LoginResponseDto actualDto = authService.login(request);

    // Then
    assertThat(actualDto).usingRecursiveComparison()
        .isEqualTo(new LoginResponseDto(1, "User", "Test", Role.DOCTOR, "Jwt Token", "Refresh Token"));
    verify(authenticationManager, times(1)).authenticate(any(Authentication.class));
    verify(jwtUtils, times(1)).generateToken(jwtPrincipalCaptor.capture());
    verify(refreshTokenService, times(1)).createRefreshToken(user);
    assertThat(jwtPrincipalCaptor.getValue()).usingRecursiveComparison()
        .isEqualTo(new JwtPrincipal(1, Role.DOCTOR));
  }

  @DisplayName("Login a user with bad credentials should throw an exception")
  @Test
  void loginWrongCredentialsTest() {
    // Given
    when(authenticationManager.authenticate(any(Authentication.class))).thenThrow(
        new BadCredentialsException("Bad credentials"));
    LoginRequestDto request = new LoginRequestDto("doctor@mail.com", "WrongPassword");

    // When
    assertThatThrownBy(() -> authService.login(request))

        // Then
        .isInstanceOf(AuthenticationException.class)
        .hasMessage("Bad credentials");
    verify(authenticationManager, times(1)).authenticate(any(Authentication.class));
    verify(jwtUtils, times(0)).generateToken(any());
    verify(refreshTokenService, times(0)).createRefreshToken(any());
  }

  @DisplayName("Refresh with token should return verify token and return renew tokens")
  @Test
  void refreshTest() {
    // GIVEN
    String token = "OldRefreshToken";
    Instant expiry = Instant.now().plusSeconds(60);
    User user = TestUtils.createUser(1, "User", "Test", "doctor@mail.com", "password", Role.DOCTOR);
    RefreshToken refreshToken = new RefreshToken(user, "Refresh Token", expiry);
    when(refreshTokenService.useRefreshToken(anyString())).thenReturn(refreshToken);
    when(jwtUtils.generateToken(any(JwtPrincipal.class))).thenReturn("Jwt Token");

    // When
    LoginResponseDto actualDto = authService.refresh(token);

    // Then
    assertThat(actualDto).usingRecursiveComparison()
        .isEqualTo(new LoginResponseDto(1, "User", "Test", Role.DOCTOR, "Jwt Token", "Refresh Token"));
    verify(refreshTokenService, times(1)).useRefreshToken(token);
    verify(jwtUtils, times(1)).generateToken(jwtPrincipalCaptor.capture());
    assertThat(jwtPrincipalCaptor.getValue()).usingRecursiveComparison()
        .isEqualTo(new JwtPrincipal(1, Role.DOCTOR));
  }

  @DisplayName("Refresh with invalid token should throw an exception")
  @Test
  void refreshWhenInvalidTokenTest() {
    // GIVEN
    String token = "OldRefreshToken";
    when(refreshTokenService.useRefreshToken(anyString()))
        .thenThrow(new RefreshTokenException("Refresh token can't be verified."));

    // When
    assertThatThrownBy(() -> authService.refresh(token))
        .isInstanceOf(RefreshTokenException.class)
        .hasMessage("Refresh token can't be verified.");
    verify(refreshTokenService, times(1)).useRefreshToken(token);
  }

}