package com.mediscreen.authserver.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.mediscreen.authserver.domain.RefreshToken;
import com.mediscreen.authserver.domain.User;
import com.mediscreen.authserver.exception.RefreshTokenException;
import com.mediscreen.authserver.repository.RefreshTokenRepository;
import com.mediscreen.authserver.utils.TestUtils;
import com.mediscreen.common.domain.Role;
import java.time.Instant;
import java.util.Optional;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest
class RefreshTokenServiceTest {

  @Autowired
  private RefreshTokenService refreshTokenService;

  @MockBean
  private RefreshTokenRepository refreshTokenRepository;

  @DisplayName("Create token for a user should return a token with user and expiry date before a week")
  @Test
  void createRefreshTokenTest() {
    // Given
    User user = TestUtils.createUser(1, "User", "Test","doctor@mail.com", "password", Role.DOCTOR);
    when(refreshTokenRepository.findByUser(any(User.class))).thenReturn(Optional.empty());
    doAnswer(invocation -> TestUtils.mockCreateToken(invocation.getArgument(0)))
        .when(refreshTokenRepository).save(any(RefreshToken.class));

    // When
    RefreshToken refreshToken = refreshTokenService.createRefreshToken(user);

    // Then
    assertThat(refreshToken.getToken()).isNotNull();
    assertThat(refreshToken.getId()).isNotZero();
    assertThat(refreshToken.getUser()).isEqualTo(user);
    assertThat(refreshToken.getExpiryDate())
        .isBetween(Instant.now(), Instant.now().plusMillis(604800000));
    verify(refreshTokenRepository, times(1)).findByUser(user);
    verify(refreshTokenRepository, times(0)).delete(any());
    verify(refreshTokenRepository, times(1)).save(refreshToken);
  }

  @DisplayName("Create token for a user when a previous token already exists should delete it")
  @Test
  void createRefreshTokenWhenOneAlreadyExistsTest() {
    // Given
    User user = TestUtils.createUser(1, "User", "Test","doctor@mail.com", "password", Role.DOCTOR);
    RefreshToken previousToken = new RefreshToken(user, "PreviousToken", Instant.now().minusSeconds(60));
    when(refreshTokenRepository.findByUser(any(User.class))).thenReturn(Optional.of(previousToken));
    doAnswer(invocation -> TestUtils.mockCreateToken(invocation.getArgument(0)))
        .when(refreshTokenRepository).save(any(RefreshToken.class));

    // When
    RefreshToken refreshToken = refreshTokenService.createRefreshToken(user);

    // Then
    assertThat(refreshToken.getId()).isNotZero();
    verify(refreshTokenRepository, times(1)).findByUser(user);
    verify(refreshTokenRepository, times(1)).delete(previousToken);
    verify(refreshTokenRepository, times(1)).save(refreshToken);
  }

  @DisplayName("Use refresh token should discard the current one and renew it with same expiry")
  @Test
  void useRefreshTokenTest() {
    // Given
    String token = "Token";
    Instant expiry = Instant.now().plusSeconds(60);
    User user = TestUtils.createUser(1, "User", "Test","doctor@mail.com", "password", Role.DOCTOR);
    RefreshToken previousRefreshToken = new RefreshToken(user, token, expiry);
    when(refreshTokenRepository.findByToken(anyString())).thenReturn(Optional.of(previousRefreshToken));
    doAnswer(invocation -> TestUtils.mockCreateToken(invocation.getArgument(0)))
        .when(refreshTokenRepository).save(any(RefreshToken.class));

    // When
    RefreshToken refreshToken = refreshTokenService.useRefreshToken(token);

    // Then
    assertThat(refreshToken).isNotEqualTo(previousRefreshToken);
    assertThat(refreshToken.getUser()).isEqualTo(user);
    assertThat(refreshToken.getExpiryDate()).isEqualTo(expiry);
    assertThat(refreshToken.getToken()).isNotNull().isNotEqualTo("Token");
    verify(refreshTokenRepository, times(1)).findByToken("Token");
    verify(refreshTokenRepository, times(1)).delete(previousRefreshToken);
    verify(refreshTokenRepository, times(1)).save(refreshToken);
  }

  @DisplayName("Use refresh token when refresh token not found should throw an exception")
  @Test
  void useRefreshTokenWhenNotFoundTest() {
    // Given
    String token = "Token";
    when(refreshTokenRepository.findByToken(anyString())).thenReturn(Optional.empty());

    // When
    assertThatThrownBy(() -> refreshTokenService.useRefreshToken(token))

        // THEN
        .isInstanceOf(RefreshTokenException.class)
        .hasMessage("Refresh token can't be verified.");
    verify(refreshTokenRepository, times(1)).findByToken(token);
    verify(refreshTokenRepository, times(0)).delete(any());
    verify(refreshTokenRepository, times(0)).save(any());
  }

  @DisplayName("Use refresh token when refresh token not found should throw an exception")
  @Test
  void useRefreshTokenExpiredTest() {
    // Given
    String token = "Token";
    Instant expiry = Instant.now().minusMillis(1);
    User user = TestUtils.createUser(1, "User", "Test","doctor@mail.com", "password", Role.DOCTOR);
    RefreshToken previousRefreshToken = new RefreshToken(user, "Token", expiry);
    when(refreshTokenRepository.findByToken(anyString())).thenReturn(Optional.of(previousRefreshToken));

    // When
    assertThatThrownBy(() -> refreshTokenService.useRefreshToken(token))

        // THEN
        .isInstanceOf(RefreshTokenException.class)
        .hasMessage("Refresh token is expired.");
    verify(refreshTokenRepository, times(1)).findByToken(token);
    verify(refreshTokenRepository, times(1)).delete(any());
    verify(refreshTokenRepository, times(0)).save(any());
  }

}