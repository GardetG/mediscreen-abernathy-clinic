package com.mediscreen.authserver.integration;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.jayway.jsonpath.JsonPath;
import com.mediscreen.authserver.domain.User;
import com.mediscreen.authserver.repository.UserRepository;
import com.mediscreen.common.domain.JwtPrincipal;
import com.mediscreen.common.domain.Role;
import com.mediscreen.common.utils.JwtUtils;
import org.json.JSONObject;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
class AuthServerIntegrationTest {

  @Autowired
  private MockMvc mockMvc;

  @Autowired
  private UserRepository userRepository;
  @Autowired
  private JwtUtils jwtUtils;

  private String getJwt(int userId) {
    return userRepository.findById(userId)
        .map(u -> jwtUtils.generateToken(new JwtPrincipal(u.getUserId(), u.getRole())))
        .orElse("");
  }

  @DisplayName("As an anonymous user I want to register so I will be able to log in")
  @Test
  void registerTest() throws Exception {
    // GIVEN The registration request
    JSONObject jsonObject = new JSONObject()
        .put("firstname", "User")
        .put("lastname", "Test")
        .put("email", "new@mail.com")
        .put("password", "P@ssW0rd");

    // WHEN A user request the registration
    mockMvc.perform(post("/auth/account/register")
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonObject.toString()))

        // THEN The new user is added with GUEST role
        .andExpect(status().isCreated())
        .andExpect(jsonPath("$.userId", is(4)))
        .andExpect(jsonPath("$.firstname", is("User")))
        .andExpect(jsonPath("$.lastname", is("Test")))
        .andExpect(jsonPath("$.email", is("new@mail.com")))
        .andExpect(jsonPath("$.role", is("GUEST")))
        .andExpect(jsonPath("$.password").doesNotExist());
    assertThat(userRepository.findById(2)).isPresent();
  }

  @DisplayName("As an anonymous user I want to log-in so I can be authenticate")
  @Test
  void loginTest() throws Exception {
    // GIVEN The registration request
    JSONObject jsonObject = new JSONObject()
        .put("username", "admin@abernathy.com")
        .put("password", "adminA=0");

    // WHEN A user request the registration
    mockMvc.perform(post("/auth/login")
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonObject.toString()))

        // THEN The new user is added
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.userId", is(1)))
        .andExpect(jsonPath("$.role", is("ADMIN")))
        .andExpect(jsonPath("$.token").isNotEmpty())
        .andExpect(jsonPath("$.refreshToken").isNotEmpty());
  }

  @DisplayName("As a User I want to refresh my token so I wont have to log in again")
  @Test
  void refreshTokenTest() throws Exception {
    // GIVEN The refresh request
    JSONObject jsonObject = new JSONObject()
        .put("refreshToken", "TestTokenAdmin");

    // WHEN A user request refresh of the token
    MvcResult result = mockMvc.perform(post("/auth/refresh")
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonObject.toString()))

        // THEN The new user receive refreshed token
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.userId", is(1)))
        .andExpect(jsonPath("$.role", is("ADMIN")))
        .andExpect(jsonPath("$.token").isNotEmpty())
        .andExpect(jsonPath("$.refreshToken").isNotEmpty())
        .andReturn();

    // WHEN the user uses the refreshed token to request profile
    String token = JsonPath.read(result.getResponse().getContentAsString(), "$.token");
    mockMvc.perform(get("/auth/account/1")
        .header("Authorization", "Bearer " + token))

        // THEN he receives the profile
        .andExpect(jsonPath("$.userId", is(1)))
        .andExpect(jsonPath("$.firstname", is("Admin")))
        .andExpect(jsonPath("$.lastname", is("Principal")))
        .andExpect(jsonPath("$.email", is("admin@abernathy.com")))
        .andExpect(jsonPath("$.role", is("ADMIN")))
        .andExpect(jsonPath("$.password").doesNotExist());
  }

  @DisplayName("As an authenticated user I want to reset my password so I can login with it")
  @Test
  void resetPasswordTest() throws Exception {
    // GIVEN a User
    String token = getJwt(2);
    JSONObject jsonObject = new JSONObject()
        .put("password", "NewP@ssW0rd");

    // WHEN A user reset the password
    mockMvc.perform(patch("/auth/account/2/resetPassword")
            .header("Authorization", "Bearer " + token)
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonObject.toString()))

        // THEN The user can use the new password to login
        .andExpect(status().isNoContent());
    jsonObject = new JSONObject()
        .put("username", "doctor@abernathy.com")
        .put("password", "NewP@ssW0rd");
    mockMvc.perform(post("/auth/login")
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonObject.toString()))
        .andExpect(status().isOk());
  }

  @DisplayName("As an authenticated admin I want to assign a role to user")
  @Test
  void assignRoleTest() throws Exception {
    // GIVEN an admin
    String token = getJwt(1);

    // WHEN the admin view page of the list of all user
    mockMvc.perform(get("/auth/account/")
            .header("Authorization", "Bearer " + token))

        // THEN he can select a user
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.content[1].userId", is(2)))
        .andExpect(jsonPath("$.content[1].role", is("DOCTOR")));

    // WHEN the admin assign to the selected user a role
    JSONObject jsonObject = new JSONObject().put("role", "ADMIN");
    mockMvc.perform(patch("/auth/account/2/assignRole")
            .header("Authorization", "Bearer " + token)
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonObject.toString()))

        // THEN the selected user have the assign role
        .andExpect(status().isNoContent());
    User user = userRepository.findById(2)
        .orElseThrow(() -> new IllegalStateException("Test data not found"));
    assertThat(user.getRole()).isEqualTo(Role.ADMIN);
  }

}