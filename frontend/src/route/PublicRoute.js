import { Navigate } from "react-router-dom";
import { useAuth } from "../context/AuthContext";

export const PublicRoute = ({ children }) => {
    const { user } = useAuth();

    // If user authenticate redirect to home page
    if (user) {
        return <Navigate to="/home" />;
    }

    return <>{ children }</>;
};