import { Navigate } from "react-router-dom";
import { useAuth } from "../context/AuthContext";

export const PrivateRoute = ({ role, children }) => {
    const { user } = useAuth();

    // If user not authenticate redirect to login page
    if (!user) {
        return <Navigate to="/" />;
    }

    // If user authenticate but doesn't have required role redirect to home page
    if (role) {
        if (!role.includes(user.role))
            return <Navigate to="/home" />;
    }
    return <>{ children }</>;
};