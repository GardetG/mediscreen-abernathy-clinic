import React, { Component } from 'react'
import { MediscreenInfo } from '../component/sections/MediscreenInfo';
import { RoleInfo } from '../component/sections/RoleInfo';
import { UserProfile } from '../component/sections/UserProfile';

// Home page with user profile and mediscreen informations
class Home extends Component {

    render() {
        return (
            <>
                <RoleInfo />
                <UserProfile />
                <MediscreenInfo />
            </>
        )
    }
    
}

export default Home;
