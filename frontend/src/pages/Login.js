import { Form, Button } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { LoadingButton } from '../component/common/LoadingButton';
import { useAuth } from '../context/AuthContext';
import { useApi } from '../helper/useApi';
import { authService } from '../service/AuthService';

export const Login = () => {
    const { onLogin } = useAuth()
    const navigate = useNavigate();
    const { error, loading, request } = useApi(authService.login, onLogin);

    const login = (credential, resetForm) => {
        request(credential);
        resetForm()
    }

    const validationSchema = Yup.object().shape({
        username: Yup.string().required("Email is required").email("Email is invalid"),
        password: Yup.string().required("Password is required")
    })

    return (

        <Formik
            initialValues={{ username: "", password: "" }}
            onSubmit={(value, { resetForm }) => login(value, resetForm)}
            validationSchema={validationSchema}>
            {
                ({
                    values,
                    errors,
                    touched,
                    handleChange,
                    handleBlur,
                    handleSubmit,
                }) => (
                    <Form noValidate onSubmit={handleSubmit}>
                        <h2>Please Login:</h2>
                        {error && <div className="alert alert-danger text-center">{error}</div>}
                        <Form.Group className="mb-3" controlId="formEmail">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control
                                type="email"
                                name="username"
                                placeholder="Enter email"
                                value={values.username}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                isValid={touched.username && !errors.username}
                                isInvalid={touched.username && !!errors.username}
                            />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control
                                type="password"
                                name="password"
                                placeholder="Password"
                                value={values.password}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                isValid={touched.password && !errors.password}
                                isInvalid={touched.password && !!errors.password}
                            />
                        </Form.Group>

                        <Button variant="primary" onClick={() => { navigate('/register') }}>Register</Button>
                        <LoadingButton variant="primary" className="float-end" type="submit" isLoading={loading}>Login</LoadingButton>
                    </Form>
                )}
        </Formik>

    )
}
