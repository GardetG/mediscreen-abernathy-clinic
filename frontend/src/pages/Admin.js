import React, { Component } from 'react'
import { UsersPage } from '../component/sections/UsersPage';

// Admin page with user lists
class Admin extends Component {

  render() {
    return (
      <UsersPage />
    )
  }

}

export default Admin;