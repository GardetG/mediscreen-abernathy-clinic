import { Button, Form } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { LoadingButton } from '../component/common/LoadingButton';
import { useApi } from '../helper/useApi';
import { authService } from '../service/AuthService';

export const Register = () => {
    const navigate = useNavigate();
    const { error, loading, request } = useApi(authService.register, () => { navigate('/login') });

    const register = (user) => {
        request(user);
    }

    const validationSchema = Yup.object().shape({
        firstname: Yup.string().required("Firstname is required").max(20),
        lastname: Yup.string().required("Lastname is required").max(20),
        email: Yup.string().required("Email is required").max(50),
        password: Yup.string().required("Password is required")
            .matches(/^(?=.*\d)(?=.*[A-Z])(?=.*\W)(?!.*\s).{8,125}$/,
                "Password must contain at least 8 characters, one uppercase, one number and one special case character"),
        passwordConfirm: Yup.string().required("Password is required")
            .oneOf([Yup.ref("password"), null], "Password must match")
    })

    return (

        <Formik
            initialValues={{ firstname: "", lastname: "", username: "", email: "", password: "", passwordConfirm: "" }}
            onSubmit={(value) => register(value)}
            validationSchema={validationSchema}>
            {
                ({
                    values,
                    errors,
                    touched,
                    handleChange,
                    handleBlur,
                    handleSubmit,
                }) => (
                    <Form noValidate onSubmit={handleSubmit}>
                        <h2>Please Register:</h2>
                        {error && <div className="alert alert-danger text-center">{error}</div>}
                        <Form.Group className="mb-3" controlId="formFirstname">
                            <Form.Label>Firstname</Form.Label>
                            <Form.Control
                                type="text"
                                name="firstname"
                                placeholder="Firstname"
                                value={values.firstname}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                isValid={touched.firstname && !errors.firstname}
                                isInvalid={touched.firstname && !!errors.firstname}
                            />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formLastname">
                            <Form.Label>Lastname</Form.Label>
                            <Form.Control
                                type="text"
                                name="lastname"
                                placeholder="Lastname"
                                value={values.lastname}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                isValid={touched.lastname && !errors.lastname}
                                isInvalid={touched.lastname && !!errors.lastname}
                            />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formEmail">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control
                                type="email"
                                name="email"
                                placeholder="Enter email"
                                value={values.email}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                isValid={touched.email && !errors.email}
                                isInvalid={touched.email && !!errors.email}
                            />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control
                                type="password"
                                name="password"
                                placeholder="Password"
                                value={values.password}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                isValid={touched.password && !errors.password}
                                isInvalid={touched.password && !!errors.password}
                            />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formPasswordConfirm">
                            <Form.Label>Password confirmation</Form.Label>
                            <Form.Control
                                type="password"
                                name="passwordConfirm"
                                placeholder="Password confirmation"
                                value={values.passwordConfirm}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                isValid={touched.passwordConfirm && !errors.passwordConfirm}
                                isInvalid={touched.passwordConfirm && !!errors.passwordConfirm}
                            />
                        </Form.Group>

                        <LoadingButton variant="primary" className="float-end" type="submit" isLoading={loading}>Register</LoadingButton>
                        <Button variant="danger" onClick={() => { navigate('/login') }}>Cancel</Button>
                    </Form>
                )}
        </Formik>

    )
}
