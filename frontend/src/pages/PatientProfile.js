import React, { Component } from 'react'
import { Tab, Tabs } from 'react-bootstrap';
import { PatientHistory } from '../component/sections/PatientHistory';
import { PatientInfo } from '../component/sections/PatientInfo';
import { PatientReport } from '../component/sections/PatientReport';
import { Relative } from '../component/sections/Relative';

// Patients page with patients page
class PatientProfile extends Component {
  state = {
    lockTabs: true,
    activeTabs: 'info',
    patient: {}
  }

  setPatient = (user, patient) => {
    if (user && patient) {
      this.setState({
        lockTabs: !(user.role === 'ADMIN' || user.role === 'DOCTOR'),
        activeTabs: 'info',
        patient: patient
      })
    }
  }

  render() {
    return (
      <Tabs
        activeKey={this.state.activeTabs}
        onSelect={(k) => this.setState({ activeTabs: k })}
        id="uncontrolled-tab-example"
        className="my-3"
      >
        <Tab eventKey="info" title="Personnal Information">
          <PatientInfo setPatient={this.setPatient} />
        </Tab>
        <Tab eventKey="history" title="Medical History" disabled={this.state.lockTabs}>
          <PatientHistory  />
        </Tab>
        <Tab eventKey="report" title="Diabetes Report" disabled={this.state.lockTabs}>
          <PatientReport patient={this.state.patient} />
          <Relative patient={this.state.patient} />
        </Tab>
      </Tabs>

    )
  }
}

export default PatientProfile;