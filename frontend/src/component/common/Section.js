import { Col, Row } from "react-bootstrap"

// Section component to display the content in a bordered section with title
export const Section = ({ children, title, titleContent }) => {

    return (
        <Row className="my-4 mx-1 justify-content-center">
            <Col>
                <Row>
                    <Col>
                        <h5>{title}</h5>
                    </Col>
                    <Col className="p-0">
                        {titleContent}
                    </Col>
                </Row>
                <Row className="border rounded justify-content-center">
                    {children}
                </Row>
            </Col>
        </Row>
    )

}