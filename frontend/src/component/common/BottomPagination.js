import { useEffect, useState } from "react"
import { Pagination } from "react-bootstrap";

// Component to display pagination
export const BottomPagination = (props) => {
    const [pageArray, setPageArray] = useState([]);
    const { currentPage, totalPages, selectPage } = props;

    // Refresh the pagination when the total of page is updated
    useEffect(() => {
        var pages = []
        var i = 1;
        while (i <= totalPages) {
            pages.push(i);
            i++;
        }
        setPageArray(pages);
    }, [totalPages]);

    return (
        <Pagination className="justify-content-center" >
            {pageArray.map((page, index) => {
                const toReturn = []

                if (index === 0) {
                    toReturn.push(
                        <Pagination.First key="firstpage"
                            onClick={
                                currentPage === 1
                                    ? () => {}
                                    : () => selectPage(1)
                            } />
                    )

                    toReturn.push(
                        <Pagination.Prev key="prevpage"
                            onClick={
                                currentPage === 1
                                    ? () => {}
                                    : () => selectPage(currentPage - 1)
                            } />
                    )
                }

                toReturn.push(
                    <Pagination.Item key={index}
                        active={currentPage === page}
                        onClick={
                            currentPage === page
                                ? () => {}
                                : () => selectPage(page)
                        } >
                            {page}
                        </Pagination.Item>
                )

                if (index === pageArray.length - 1) {
                    toReturn.push(
                        <Pagination.Next key="nextpage"
                            onClick={
                                currentPage === page
                                    ? () => {}
                                    : () => selectPage(currentPage + 1)
                            } />
                    )

                    toReturn.push(
                        <Pagination.Last key="lastPage"
                            onClick={
                                currentPage === page
                                    ? () => {}
                                    : () => selectPage(page)
                            } />
                    )
                }

                return toReturn;
            })}
        </Pagination>
    );
}