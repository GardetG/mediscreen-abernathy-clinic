import { faUser } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { Col } from "react-bootstrap"
import { Section } from "./Section"

// Section component to display a profile content in a bordered section with title and user icon
export const Profile = ({ children, title, titleContent }) => {

    return (
        <Section title={title} titleContent={titleContent}>
            <Col className="text-center p-2">
                <FontAwesomeIcon icon={faUser} size='10x' className="bg-icon m-4" />
            </Col>
            <Col className="align-self-center border border-2 rounded p-2 m-3">
                {children}
            </Col>
        </Section>
    )

}