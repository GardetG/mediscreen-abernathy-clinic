import React from "react";
import './RiskIndicator.css'

function RiskIndicator({ percentage, label }) {
  return (
    <div className="progress-bar">
      <Filler percentage={percentage} label={label} />
    </div>
  );
}

const getColor = (percentage) => {
  let r = 255 * (percentage / 100)
  let g = 255-(255 * (percentage / 100))
  let b = 0
  return {r,g,b}
}

function Filler({ percentage , label}) {
  const {r,g,b} = getColor(percentage);
  return <div className="filler" style={{ width: `${percentage}%` , background: `rgb(${r},${g},${b})` }} >{label}</div>;
}

export default RiskIndicator;
