import { faCircleExclamation } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Alert, Spinner } from "react-bootstrap";

// Component to display a spinner while loading the content
export const LoadingContent = (props) => {
    const { isLoading, error, children } = props;

    if (isLoading) {
        return (
            <Spinner
                as="span"
                animation="border"
                size="xl"
                role="status"
                aria-hidden="true"
                className='m-5'
            />
        )
    }

    if (error) {
        return (
            <Alert variant="danger" className="m-0">
                <FontAwesomeIcon icon={faCircleExclamation} /> &nbsp;
                {error}
            </Alert>
        )
    }

    return (
        <>
            {children}
        </>
    )
}