import { Button, Spinner } from "react-bootstrap";

// Component to display a button with spinner effect of loading
export const LoadingButton = (props) => {
    let { variant, type, onClick, disabled, className, isLoading, children } = props;

    if (!onClick) {
        onClick = () => {};
    }

    return (
        <Button variant={variant} type={type} onClick={() => onClick()} className={className} disabled={isLoading || disabled}>
            {isLoading
                ? <Spinner
                    as="span"
                    animation="border"
                    size="sm"
                    role="status"
                    aria-hidden="true"
                />
                : children}
        </Button>
    )
}