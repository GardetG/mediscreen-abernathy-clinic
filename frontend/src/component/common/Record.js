import { Button, Col, Collapse, Row } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronDown, faChevronUp, faFile, faPen, faTrashCan } from "@fortawesome/free-solid-svg-icons";
import { LoadingButton } from "./LoadingButton";
import { RecordForm } from "../form/RecordForm";
import { useModal } from "../../helper/useModal";
import { useApi } from "../../helper/useApi";
import { historyService } from "../../service/HistoryService";
import { useCallback, useEffect } from "react";

// Component to display a record
export const Record = (props) => {
    const { activeKey, setActive, record, refresh, } = props;
    const { recordId, createdDate } = record
    const { state, showModal, hideModal } = useModal();
    const { data, loading, request, setData } = useApi(historyService.getMedicalRecord);
    const deleteMedicalRecordApi = useApi(historyService.deleteMedicalRecord, refresh);

    // request the record deletion
    const deleteRecord = () => {
        const confirm = window.confirm(`Are you sure to remove this medical record? This action is final.`)
        if (confirm) {
            deleteMedicalRecordApi.request(recordId)
        }
    }

    // request the record
    const load = useCallback(() => {
        request(recordId)
    }, [recordId, request]);

    // Check if this record is active
    const isActive = useCallback(() => {
        return (activeKey === recordId)
    }, [activeKey, recordId]);

    // Update the current active record
    const toogleActive = () => {
        if (isActive()) {
            return setActive(null);
        }
        setActive(recordId);
    }

    // Load the record if it's active
    useEffect(() => {
        if (isActive()) {
            load();
        }
    }, [load, isActive])

    return (
        <Row className="border border-2 rounded justify-content-center my-3 mx-1">
            <RecordForm show={state} onClose={hideModal} record={data} patientId={record.patientId} callback={setData} />
            <Row className="bg-theme text-white align-items-center p-0">
                <Col xs={8}>
                    <FontAwesomeIcon icon={faFile} />&nbsp;
                    {createdDate}
                </Col>
                <Col xs={4} className="p-0">
                    <LoadingButton className="float-end bg-transparent" onClick={toogleActive} isLoading={loading} >
                        {isActive()
                            ? <FontAwesomeIcon icon={faChevronUp} />
                            : <FontAwesomeIcon icon={faChevronDown} />
                        }
                    </LoadingButton>
                </Col>
            </Row>
            <Collapse in={data && isActive()}>
                <div>
                    <Row className="p-0 bg-light">
                        <div className="breaklines">
                            {data?.notes}
                        </div>
                    </Row>
                    <Row className="bg-theme text-white align-items-center p-0">
                        <Col xs="auto" className="ps-2 pe-0">
                            <small><em>Last update : {data?.updatedDate}</em></small>
                        </Col>
                        <Col className="p-0" >
                            <LoadingButton variant="danger" className="float-end" onClick={deleteRecord} isLoading={deleteMedicalRecordApi.loading} disabled={loading}><FontAwesomeIcon icon={faTrashCan} /></LoadingButton>
                            <Button variant="primary" className="float-end me-1" onClick={showModal} disabled={loading}><FontAwesomeIcon icon={faPen} /></Button>
                        </Col>
                    </Row>
                </div>
            </Collapse>
        </Row >

    )
}