import { Button, Form, Modal } from "react-bootstrap";
import { Formik } from "formik";
import * as Yup from 'yup';
import { LoadingButton } from "../common/LoadingButton";
import { useApi } from "../../helper/useApi";
import { patientService } from "../../service/PatientService";

// Component to display modal form to add or update patient
export const PatientForm = props => {
  const { show, close, patient, callback } = props;

  const getApiMethod = () => {
    if (patient) {
      return patientService.updatePatient
    }
    return patientService.addPatient
  }

  const onSuccess = (response) => {
    callback(response);
    close();
  }
  
  const savePatientApi = useApi(getApiMethod(), onSuccess);

  const getTitle = () => {
    if (patient) {
      return 'Edit a patient'
    }
    return 'Add a patient'
  }

  const save = (data) => {
    if (patient) {
      data.userId = patient.userId
    }
    savePatientApi.request(data)
  }

  const validationSchema = Yup.object().shape({
    firstname: Yup.string().required("Firstname is required").max(20),
    lastname: Yup.string().required("lastname is required").max(20),

    gender: Yup.string().required(),
    dateOfBirth: Yup.date().required("lastname is required").max(new Date()),

    address: Yup.string().max(50),
    phoneNumber: Yup.string().max(12),
  })

  return (
    <Modal show={show} onHide={close} centered>
      <Formik

        initialValues={patient || { firstname: "", lastname: "", gender: "", dateOfBirth: "", address: "", phoneNumber: "" }}
        onSubmit={(value) => save(value)}
        validationSchema={validationSchema}
      >{
          ({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
          }) => (
            <>

              <Form noValidate onSubmit={handleSubmit}>
                <Modal.Header closeButton>
                  <Modal.Title>{getTitle()}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                  {savePatientApi.error && <div className="alert alert-danger text-center">{savePatientApi.error}</div>}
                  <Form.Group className="mb-3" controlId="formFirstname">
                    <Form.Label>Firstname</Form.Label>
                    <Form.Control
                      type="text"
                      name="firstname"
                      placeholder="Firstname"
                      value={values.firstname}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      isValid={touched.firstname && !errors.firstname}
                      isInvalid={touched.firstname && !!errors.firstname}
                    />
                  </Form.Group>

                  <Form.Group className="mb-3" controlId="formLastname">
                    <Form.Label>Lastname</Form.Label>
                    <Form.Control
                      type="text"
                      name="lastname"
                      placeholder="Lastname"
                      value={values.lastname}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      isValid={touched.lastname && !errors.lastname}
                      isInvalid={touched.lastname && !!errors.lastname}
                    />
                  </Form.Group>

                  <Form.Group className="mb-3" controlId="formDateOfBirth">
                    <Form.Label>Date of birth</Form.Label>
                    <Form.Control
                      type="date"
                      name="dateOfBirth"
                      placeholder="Date of birth"
                      value={values.dateOfBirth}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      isValid={touched.dateOfBirth && !errors.dateOfBirth}
                      isInvalid={touched.dateOfBirth && !!errors.dateOfBirth}
                    />
                  </Form.Group>

                  <Form.Group className="mb-3" controlId="formAddress">
                    <Form.Label>Address (Optional)</Form.Label>
                    <Form.Control
                      type="text"
                      name="address"
                      placeholder="Address"
                      value={values.address}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      isValid={touched.address && !errors.address}
                      isInvalid={touched.address && !!errors.address}
                    />
                  </Form.Group>

                  <Form.Group className="mb-3" controlId="formPhone">
                    <Form.Label>Phone number (Optional)</Form.Label>
                    <Form.Control
                      type="text"
                      name="phoneNumber"
                      placeholder="Phone Number"
                      value={values.phoneNumber}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      isValid={touched.phoneNumber && !errors.phoneNumber}
                      isInvalid={touched.phoneNumber && !!errors.phoneNumber}
                    />
                  </Form.Group>

                  <Form.Group className="mb-3" controlId="formGender">
                    <Form.Label>Gender</Form.Label>
                    <Form.Select aria-label="Default select example"
                      required
                      name="gender"
                      placeholder="Choose the gender..."
                      value={values.gender}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      isValid={touched.gender && !errors.gender}
                      isInvalid={touched.gender && !!errors.gender}
                    >
                      <option value="">Choose the gender...</option>
                      <option value="F">Female</option>
                      <option value="M">Male</option>
                    </Form.Select>
                  </Form.Group>

                </Modal.Body>
                <Modal.Footer>
                  <Button variant="secondary" onClick={close}>
                    Close
                  </Button>
                  <LoadingButton variant="primary" type="submit" isLoading={savePatientApi.loading}>
                    Save Changes
                  </LoadingButton>
                </Modal.Footer>
              </Form>
            </>
          )}

      </Formik>

    </Modal>
  );
}