import { Button, Form, Modal } from "react-bootstrap";
import { Formik } from "formik";
import * as Yup from 'yup';
import { LoadingButton } from "../common/LoadingButton";
import { useApi } from "../../helper/useApi";
import { historyService } from "../../service/HistoryService";

// Component to display modal form to add or update record
export const RecordForm = props => {
    const { show, onClose, callback, record, patientId } = props;

    const getApiMethod = () => {
        if (record) {
            return historyService.updateMedicalRecord
        }
        return historyService.addMedicalRecord
    }

    const onSuccess = (response) => {
        callback(response);
        onClose();
    }
    const saveMedicalRecordApi = useApi(getApiMethod(), onSuccess);

    const save = (notes) => {
        let recordToSave = record || {}
        recordToSave.patientId = patientId;
        recordToSave.notes = notes
        saveMedicalRecordApi.request(recordToSave)
    }

    const getTitle = () => {
        if (record) {
            return 'Edit a medical Record'
        }
        return 'Add a medical Record'
    }

    const validationSchema = Yup.object().shape({
        notes: Yup.string().required("Notes are required")
    })

    return (
        <Modal show={show} onHide={onClose} size="lg" centered>
            <Formik
                initialValues={{ notes: record?.notes }}
                onSubmit={(value) => { save(value.notes) }}
                validationSchema={validationSchema}
            >{
                    ({
                        values,
                        errors,
                        touched,
                        handleChange,
                        handleBlur,
                        handleSubmit,
                    }) => (
                        <>

                            <Form noValidate onSubmit={handleSubmit}>
                                <Modal.Header closeButton>
                                    <Modal.Title>{getTitle()}</Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                    {saveMedicalRecordApi.error && <div className="alert alert-danger text-center">{saveMedicalRecordApi.error}</div>}

                                    <Form.Group className="mb-3" controlId="formNotes">
                                        <Form.Label>Medical record notes:</Form.Label>
                                        <Form.Control
                                            as="textarea"
                                            rows={7}
                                            name="notes"
                                            placeholder="Please, write your notes..."
                                            value={values.notes}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            isValid={touched.notes && !errors.notes}
                                            isInvalid={touched.notes && !!errors.notes}
                                        />
                                    </Form.Group>

                                </Modal.Body>
                                <Modal.Footer>
                                    <Button variant="secondary" onClick={onClose}>
                                        Close
                                    </Button>
                                    <LoadingButton variant="primary" type="submit" isLoading={saveMedicalRecordApi.loading}>
                                        Save Change
                                    </LoadingButton>

                                </Modal.Footer>
                            </Form>
                        </>
                    )}


            </Formik>

        </Modal>
    );
}