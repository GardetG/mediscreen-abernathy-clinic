import { Button, Form, Modal } from "react-bootstrap";
import { Formik } from "formik";
import * as Yup from 'yup';
import { LoadingButton } from "../common/LoadingButton";
import { useApi } from "../../helper/useApi";
import { userService } from "../../service/UserService";

// Component to display modal form to assign role
export const AssignRoleForm = props => {
  const { show, close, user, callback } = props;
  const onSuccess = () => {
    callback();
    close();
  }
  const { error, loading, request } = useApi(userService.assignRole, onSuccess);

  const assignRole = (role) => {
    request(user?.userId, role)
  }

  const getInitialValues = () => {
    return user ? { "role": user.role } : { "role": "GUEST" }
  }

  const validationSchema = Yup.object().shape({
    role: Yup.string().required()
  })

  return (
    <Modal show={show} onHide={close} centered>
      <Formik

        initialValues={getInitialValues()}
        onSubmit={(value) => assignRole(value)}
        validationSchema={validationSchema}
      >{
          ({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
          }) => (
            <>
              <Form noValidate onSubmit={handleSubmit}>
                <Modal.Header closeButton>
                  <Modal.Title>Assign Role:</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                  {error && <div className="alert alert-danger text-center">{error}</div>}

                  <Form.Group className="mb-3" controlId="formRole">
                    <Form.Label>Role</Form.Label>
                    <Form.Select aria-label="Default select example"
                      required
                      name="role"
                      placeholder="Choose a role..."
                      value={values.role}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      isValid={touched.role && !errors.role}
                      isInvalid={touched.role && !!errors.role}
                    >
                      <option value="">Choose a role...</option>
                      <option value="GUEST">GUEST</option>
                      <option value="ORGANIZER">ORGANIZER</option>
                      <option value="DOCTOR">DOCTOR</option>
                      <option value="ADMIN">ADMIN</option>
                    </Form.Select>
                  </Form.Group>

                </Modal.Body>
                <Modal.Footer>
                  <Button variant="secondary" onClick={close}>
                    Close
                  </Button>
                  <LoadingButton variant="primary" type="submit" isLoading={loading}>
                    Save Changes
                  </LoadingButton>
                </Modal.Footer>
              </Form>
            </>
          )}

      </Formik>

    </Modal>
  );
}