import { Button, Form, Modal } from "react-bootstrap";
import { Formik } from "formik";
import * as Yup from 'yup';
import { LoadingButton } from "../common/LoadingButton";
import { useApi } from "../../helper/useApi";
import { userService } from "../../service/UserService";

// Component to display modal form to reset password
export const ResetPasswordForm = props => {
    const { show, close, id } = props;
    const { error, loading, request } = useApi(userService.resetPassword, close);

    const resetPassword = (password) => {
        request(id, password);
    }

    const validationSchema = Yup.object().shape({
        password: Yup.string().required("Password is required")
            .matches(/^(?=.*\d)(?=.*[A-Z])(?=.*\W)(?!.*\s).{8,125}$/,
                "Password must contain at least 8 characters, one uppercase, one number and one special case character"),
        passwordConfirm: Yup.string().required("Password is required")
            .oneOf([Yup.ref("password"), null], "Password must match")
    })

    return (
        <Modal show={show} onHide={close} centered>
            <Formik

                initialValues={{ password: "", passwordConfirm: "" }}
                onSubmit={(value) => resetPassword(value)}
                validationSchema={validationSchema}
            >{
                    ({
                        values,
                        errors,
                        touched,
                        handleChange,
                        handleBlur,
                        handleSubmit,
                    }) => (
                        <>

                            <Form noValidate onSubmit={handleSubmit}>
                                <Modal.Header closeButton>
                                    <Modal.Title>Reset Password</Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                    {error && <div className="alert alert-danger text-center">{error}</div>}
                                    <Form.Group className="mb-3" controlId="formPassword">
                                        <Form.Label>Password</Form.Label>
                                        <Form.Control
                                            type="password"
                                            name="password"
                                            placeholder="Password"
                                            value={values.password}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            isValid={touched.password && !errors.password}
                                            isInvalid={touched.password && !!errors.password}
                                        />
                                    </Form.Group>

                                    <Form.Group className="mb-3" controlId="formPasswordConfirm">
                                        <Form.Label>Password confirmation</Form.Label>
                                        <Form.Control
                                            type="password"
                                            name="passwordConfirm"
                                            placeholder="Password confirmation"
                                            value={values.passwordConfirm}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            isValid={touched.passwordConfirm && !errors.passwordConfirm}
                                            isInvalid={touched.passwordConfirm && !!errors.passwordConfirm}
                                        />
                                    </Form.Group>

                                </Modal.Body>
                                <Modal.Footer>
                                    <Button variant="secondary" onClick={close}>
                                        Close
                                    </Button>
                                    <LoadingButton variant="primary" type="submit" isLoading={loading}>
                                        Save Change
                                    </LoadingButton>

                                </Modal.Footer>
                            </Form>
                        </>
                    )}


            </Formik>

        </Modal>
    );
}