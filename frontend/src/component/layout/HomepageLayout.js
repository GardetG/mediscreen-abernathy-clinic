import { Outlet } from 'react-router-dom';
import { Col, Container, Row } from "react-bootstrap";
import logo from '../../img/mediscreen_logo.png'
import './HomepageLayout.css';

export const HomepageLayout = () => {

    return (
        <Container>
            <Row className="my-5">
                <Col>
                    <div className="text-center">
                        <img src={logo} className="logo" alt="logo" />
                    </div>
                </Col>
                <Col className="pt-4">
                    <h1>Abernathy Clinic</h1>
                    <p>By Mediscreen</p>
                </Col>
            </Row>

            <Row className="py-5 bg-image bordered">
                <Col md={{ span: 4, offset: 2 }} sm={{ span: 8, offset: 2 }} className="px-3">
                    <Outlet />
                </Col>
            </Row>
        </Container>
    );
};
