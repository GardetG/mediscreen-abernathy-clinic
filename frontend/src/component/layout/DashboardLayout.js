import { Link, Outlet } from 'react-router-dom';
import { useAuth } from '../../context/AuthContext';
import { Col, Container, Nav, Navbar, Row } from "react-bootstrap";
import logo from '../../img/mediscreen_logo.png'
import './DashboardLayout.css'

// Dashboard layout with Navigation
export const DashboardLayout = () => {
    const { user, onLogout } = useAuth();
    const {firstname, lastname, role} = user || {}

    return (
        <>
            <Navbar bg="light" expand="lg">
                <Container>
                    <Navbar.Brand href="/home">
                        <Row>
                            <Col>
                                <img width='50px' src={logo} className="logo" alt="logo" />
                            </Col>
                            <Col className='align-self-center'>
                                <h2>Abernathy Clinic</h2>
                            </Col>
                        </Row>
                    </Navbar.Brand>
                </Container>
            </Navbar>
            <Navbar className="bg-theme" expand="md" variant="dark">
                <Container>
                    <Navbar.Brand href="/home">
                        <div className="text-center">
                            <h4>Welcome {firstname} {lastname}</h4>
                        </div>
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="ms-auto">
                            <Nav.Link as={Link} to="/home"><h4>Home</h4></Nav.Link>
                            {role && role !== "GUEST" &&
                                <Nav.Link as={Link} to="/patients"><h4>Patients</h4></Nav.Link>
                            }
                            {role && role === "ADMIN" &&
                                <Nav.Link as={Link} to="/admin"><h4>Admin</h4></Nav.Link>
                            }
                            <Nav.Link href="#" onClick={() => onLogout()}><h4>Logout</h4></Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>

            <Container>
                <Outlet />
            </Container>
        </>
    );
};
