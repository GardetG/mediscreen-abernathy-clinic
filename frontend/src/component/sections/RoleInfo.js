import { useAuth } from "../../context/AuthContext";
import { Section } from "../common/Section";

// Component to display current user's role specific informations
export const RoleInfo = () => {
    const { user } = useAuth();

    if (user.role === "GUEST") {
        return (
            <Section>
                <div className="alert alert-warning m-0">
                    You are currently authenticate as <b>{user.role}</b>. <br />
                    Please wait for an administrator to grant you a role to gain access to the full application functionalities.
                </div>
            </Section>
        )
    }

}