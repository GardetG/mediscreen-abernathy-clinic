import { useCallback, useEffect, useState } from "react";
import { Button, Table } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPen } from "@fortawesome/free-solid-svg-icons";
import { Section } from "../common/Section";
import { LoadingContent } from "../common/LoadingContent";
import { AssignRoleForm } from "../form/AssignRoleForm";
import { BottomPagination } from "../common/BottomPagination";
import { useApi } from "../../helper/useApi";
import { userService } from "../../service/UserService";

// Component to display pages of users
export const UsersPage = () => {
    const [page, setPage] = useState(1)
    const [modalState, setModalState] = useState(false)
    const [selectedUser, setSelectedUser] = useState(null)
    const { data, error, loading, request } = useApi(userService.getUsersPage);

    // request the page
    const load = useCallback(() => {
        request(page, 10)
    }, [page, request]);

    // load the page of user when page is updated
    useEffect(() => {
        load();
    }, [load]);

    const showModal = (user) => {
        setSelectedUser(user);
        setModalState(true);
    }

    const hideModal = () => {
        setSelectedUser(null);
        setModalState(false);
    }

    return (
        <Section title={"My Users"}>
            <AssignRoleForm show={modalState} close={hideModal} user={selectedUser} callback={load} />
            <LoadingContent isLoading={loading} error={error}>
                <Table responsive striped hover className="m-0 align-middle">
                    <thead>
                        <tr>
                            <th width={'3%'}>#</th>
                            <th width={'20%'}>Firstname</th>
                            <th>Lastname</th>
                            <th width={'20%'}>Role</th>
                            <th width={'5%'}></th>
                        </tr>
                    </thead>
                    <tbody>
                        {data?.content.map(user =>
                            <tr key={user.userId}>
                                <td>{user.userId}</td>
                                <td>{user.firstname}</td>
                                <td>{user.lastname}</td>
                                <td>{user.role}</td>
                                <td>
                                    <Button variant="primary" onClick={() => showModal(user)} ><FontAwesomeIcon icon={faPen} /></Button>
                                </td>
                            </tr>
                        )}
                    </tbody>
                </Table>
                <BottomPagination currentPage={page} totalPages={data?.totalPages} selectPage={setPage} />
            </LoadingContent>
        </Section>
    )

}