import { Col, Row } from "react-bootstrap"
import { faQuoteLeft, faQuoteRight } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { Section } from "../common/Section"
import { useParams } from "react-router-dom"
import { useApi } from "../../helper/useApi"
import { reportService } from "../../service/ReportService"
import { useCallback, useEffect } from "react"
import RiskIndicator from "../common/RiskIndicator/RiskIndicator"
import { LoadingContent } from "../common/LoadingContent"

// Component to display Mediscreen informations
export const PatientReport = () => {
    const { id } = useParams();
    const { data, error, loading, request, setData } = useApi(reportService.getReportById)

    // request the patient
    const load = useCallback(() => {
        request(id);
    }, [id, request]);

    // Load the patient when id is updated
    useEffect(() => {
        load()
    }, [load]);

    const level = () => {
        if (!data) {
            return 0;
        }
        let index = riskMapping(data.risk);
        let level = (index / 3) * 100;
        return level;
    }

    const riskMapping = (risk) => {
        if (!risk || risk==="None") {
            return 0;
        } else if (risk==="Borderline") {
            return 1;
        } else if (risk==="In danger") {
            return 2;
        } else if (risk==="Early onset") {
            return 3;
        }
    }


    return (
        <Section title={"Diabetes Risk Assessment Raport"}>
            <Col className="align-self-center border border-2 rounded p-3 m-3">
                <LoadingContent isLoading={loading} error={error}>
                <Row className="my-5">
                    <Col className='text-center'>
                        <FontAwesomeIcon icon={faQuoteLeft} size='2x' className="theme_icon" />
                        &nbsp;{data && data.report}&nbsp;
                        <FontAwesomeIcon icon={faQuoteRight} size='2x' className="theme_icon" />
                    </Col>
                    <Col>
                        <RiskIndicator percentage={level()} label={ data && data.risk} />
                    </Col>
                </Row>
                </LoadingContent>
            </Col>
        </Section>
    )

}