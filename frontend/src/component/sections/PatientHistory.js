import { useCallback, useEffect, useState } from "react"
import { Button, Col } from "react-bootstrap"
import { faPlus } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { useParams } from "react-router-dom"
import { Section } from "../common/Section"
import { Record } from "../common/Record"
import { LoadingContent } from "../common/LoadingContent"
import { BottomPagination } from "../common/BottomPagination"
import { RecordForm } from "../form/RecordForm"
import { useModal } from "../../helper/useModal"
import { useApi } from "../../helper/useApi"
import { historyService } from "../../service/HistoryService"

// Component to display patient medial history
export const PatientHistory = (props) => {
    const { id } = useParams();
    const [page, setPage] = useState(1)
    const [activeKey, setActiveKey] = useState(null)
    const { state, showModal, hideModal } = useModal()
    const { data, error, loading, request } = useApi(historyService.getMedicalHistory);

    // request the history page
    const load = useCallback(() => {
        request(id, page, 10);
    }, [page, id, request]);

    // load the page when patient id or page are updated
    useEffect(() => {
        load();
    }, [load]);

    return (
        <Section title={"Medical History"} titleContent={
            <Button variant="success" className="float-end" onClick={showModal}><FontAwesomeIcon icon={faPlus} /></Button>
        }>
            <RecordForm show={state} onClose={hideModal} patientId={id} callback={load} />
            <Col>
                <LoadingContent isLoading={loading} error={error}>
                    {data?.content.map(record =>
                        <Record
                            key={record.recordId}
                            activeKey={activeKey}
                            setActive={setActiveKey}
                            record={record}
                            refresh={load}
                        />
                    )}
                    <BottomPagination currentPage={page} totalPages={data?.totalPages} selectPage={setPage} />
                </LoadingContent>
            </Col>

        </Section>
    )

}