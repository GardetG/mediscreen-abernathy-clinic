import { Col } from "react-bootstrap"
import { faQuoteLeft, faQuoteRight } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { Section } from "../common/Section"

// Component to display Mediscreen informations
export const MediscreenInfo = () => {

    return (
        <Section title={"Mediscreen"}>
            <Col className="align-self-center border border-2 rounded p-3 m-3">
                <div className='headline'>We care.</div>
                <div className='text-center'>
                    <FontAwesomeIcon icon={faQuoteLeft} size='2x' className="theme_icon" />
                    &nbsp;Mediscreen specialize in detecting risk factor for disease. Our screenings using predictive analysis of patient populations at an affordable cost.&nbsp;
                    <FontAwesomeIcon icon={faQuoteRight} size='2x' className="theme_icon" />
                </div>
                <div className='headline text-end'>We have solution for you.</div>
            </Col>
        </Section>
    )

}