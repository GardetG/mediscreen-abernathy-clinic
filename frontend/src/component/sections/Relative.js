import { useCallback, useEffect, useState } from "react";
import { Button, Table } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFileLines, faPlus } from "@fortawesome/free-solid-svg-icons";
import { Section } from "../common/Section";
import { LoadingContent } from "../common/LoadingContent";
import { BottomPagination } from "../common/BottomPagination";
import { PatientForm } from "../form/PatientForm";
import { useNavigate } from "react-router-dom";
import { useModal } from "../../helper/useModal";
import { useApi } from "../../helper/useApi";
import { patientService } from "../../service/PatientService";
import { reportService } from "../../service/ReportService";

// Component to display patients relative report
export const Relative = (props) => {
    const {patient} = props
    const { data, error, loading, request } = useApi(reportService.getReportByLastname);

    // request the reports
    const load = useCallback(() => {
        if (patient) {
            request(patient.lastname);
        }
    }, [patient, request])

    // load the reports when the page is updated
    useEffect(() => {
        load()
    }, [load]);

    return (
        <Section title={"Patient's Relatives"}>
            <LoadingContent isLoading={loading} error={error}>
                <Table responsive striped hover className="m-0 align-middle">
                    <thead>
                        <tr>
                            <th width={'3%'}>#</th>
                            <th width={'20%'}>Report</th>
                            <th width={'5%'}>Risk</th>
                        </tr>
                    </thead>
                    <tbody>
                        {data?.map(patient =>
                            <tr key={patient.patientId}>
                                <td>{patient.patientId}</td>
                                <td>{patient.report}</td>
                                <td>{patient.risk}</td>
                            </tr>
                        )}

                    </tbody>
                </Table>
            </LoadingContent>
        </Section>
    )

}