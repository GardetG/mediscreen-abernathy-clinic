import { Button, Row, Table } from "react-bootstrap";
import { Profile } from "../common/Profile";
import { ResetPasswordForm } from "../form/ResetPasswordForm";
import { useAuth } from "../../context/AuthContext";
import { useModal } from "../../helper/useModal";
import { userService } from "../../service/UserService";
import { useApi } from "../../helper/useApi";
import { useCallback, useEffect } from "react";

// Component to display the current user profile
export const UserProfile = () => {
    const { user } = useAuth();
    const {data, error, loading, request} = useApi(userService.getUser)
    const { firstname, lastname, role, email, userId } = data || {}
    const {state, showModal, hideModal} = useModal()

    // request the patient
    const load = useCallback(() => {
        request(user.userId);
    }, [user.userId, request]);

    // Load the patient when id is updated
    useEffect(() => {
        load()
    }, [load]);

    return (
        <Profile title={"My Profile"}>
            <ResetPasswordForm show={state} close={hideModal} id={userId} />
            <Row>
                <Table responsive size="sm">
                    <tbody>
                        <tr>
                            <td>Firstname:</td>
                            <td>{firstname}</td>
                        </tr>
                        <tr>
                            <td>Lastname:</td>
                            <td>{lastname}</td>
                        </tr>
                        <tr>
                            <td>Role:</td>
                            <td>{role}</td>
                        </tr>
                        <tr>
                            <td>Email:</td>
                            <td>{email}</td>
                        </tr>
                    </tbody>
                </Table>
            </Row>
            <Row>
                <div>
                    <Button variant="primary" onClick={showModal} className="float-end">Change Password</Button>

                </div>
            </Row>
        </Profile>
    )

}