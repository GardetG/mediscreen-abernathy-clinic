import React, { useCallback, useEffect } from 'react'
import { Col, Row, Button, Table } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft, faPen, faTrashCan } from '@fortawesome/free-solid-svg-icons';
import { useNavigate, useParams } from 'react-router-dom';
import { Profile } from '../common/Profile';
import { LoadingContent } from '../common/LoadingContent';
import { LoadingButton } from '../common/LoadingButton';
import { PatientForm } from '../form/PatientForm';
import { useModal } from '../../helper/useModal';
import { useApi } from '../../helper/useApi';
import { patientService } from '../../service/PatientService';
import { useAuth } from '../../context/AuthContext';

// Component to display a patient personnal information
export const PatientInfo = (props) => {
    const { setPatient } = props
    const { user } = useAuth()
    const { id } = useParams();
    const { state, showModal, hideModal } = useModal()
    const navigate = useNavigate();
    const { data, error, loading, request, setData } = useApi(patientService.getPatient)
    const deletePatientApi = useApi(patientService.deletePatient, () => navigate('/patients'))
    const { firstname, lastname, dateOfBirth, gender, address, phoneNumber, lastUpdate } = data || {}

    // request the patient
    const load = useCallback(() => {
        request(id);
    }, [id, request]);

    // Load the patient when id is updated
    useEffect(() => {
        load()
    }, [load]);

    useEffect(() => {
        setPatient(user, data)
    }, [data, user]);

    // request the patient deletion
    const removePatient = () => {
        const confirm = window.confirm(`Are you sure to remove ${firstname} ${lastname}? This action is final.`)
        if (confirm) {
            deletePatientApi.request(id)
        }
    }

    return (
        <Profile title={loading || error ? 'Patient profile' : firstname + ' ' + lastname}>
            <PatientForm show={state} close={hideModal} patient={data} callback={setData} />
            <Row className="p-1 justify-content-center">
                <LoadingContent isLoading={loading} error={error}>
                    <Table responsive size="sm" className='m-0'>
                        <tbody>
                            <tr>
                                <td>Firstname:</td>
                                <td>{firstname}</td>
                            </tr>
                            <tr>
                                <td>Lastname:</td>
                                <td>{lastname}</td>
                            </tr>
                            <tr>
                                <td>Birthdate:</td>
                                <td>{dateOfBirth}</td>
                            </tr>
                            <tr>
                                <td>Gender:</td>
                                <td>{gender}</td>
                            </tr>
                            <tr>
                                <td>Address:</td>
                                <td>{address}</td>
                            </tr>
                            <tr>
                                <td>Phone number:</td>
                                <td>{phoneNumber}</td>
                            </tr>
                        </tbody>
                    </Table>
                    <div className='text-end'>
                        <small><em>Last update : {lastUpdate}</em></small>
                    </div>
                </LoadingContent>
            </Row>
            <Row>
                <Col>
                    <Button variant="primary" onClick={() => navigate('/patients')} ><FontAwesomeIcon icon={faArrowLeft} /></Button>
                    <LoadingButton variant="danger" onClick={removePatient} className="float-end" isLoading={deletePatientApi.loading} disabled={loading || error}>
                        <FontAwesomeIcon icon={faTrashCan} />
                    </LoadingButton>
                    <Button variant="primary" onClick={showModal} className="float-end mx-2" disabled={loading || error}>
                        <FontAwesomeIcon icon={faPen} />
                    </Button>
                </Col>
            </Row>

        </Profile >

    )

}
