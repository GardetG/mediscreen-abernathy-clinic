import { useCallback, useEffect, useState } from "react";
import { Button, Table } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFileLines, faPlus } from "@fortawesome/free-solid-svg-icons";
import { Section } from "../common/Section";
import { LoadingContent } from "../common/LoadingContent";
import { BottomPagination } from "../common/BottomPagination";
import { PatientForm } from "../form/PatientForm";
import { useNavigate } from "react-router-dom";
import { useModal } from "../../helper/useModal";
import { useApi } from "../../helper/useApi";
import { patientService } from "../../service/PatientService";

// Component to display pages of patients
export const PatientsPage = () => {
    const [page, setPage] = useState(1)
    const { state, showModal, hideModal } = useModal()
    const { data, error, loading, request } = useApi(patientService.getPatientsPage);
    const navigate = useNavigate();

    // request the page
    const load = useCallback(() => {
        request(page, 10);
    }, [page, request])

    // load the page when the page is updated
    useEffect(() => {
        load()
    }, [load]);

    return (
        <Section title={"My Patients"} titleContent={
            <Button variant="success" className="float-end" onClick={showModal}><FontAwesomeIcon icon={faPlus} /></Button>
        }>
            <PatientForm show={state} close={hideModal} callback={load} />
            <LoadingContent isLoading={loading} error={error}>
                <Table responsive striped hover className="m-0 align-middle">
                    <thead>
                        <tr>
                            <th width={'3%'}>#</th>
                            <th width={'20%'}>Firstname</th>
                            <th>Lastname</th>
                            <th width={'20%'}>Birthdate</th>
                            <th width={'5%'}></th>
                        </tr>
                    </thead>
                    <tbody>
                        {data?.content.map(patient =>
                            <tr key={patient.patientId}>
                                <td>{patient.patientId}</td>
                                <td>{patient.firstname}</td>
                                <td>{patient.lastname}</td>
                                <td>{patient.dateOfBirth}</td>
                                <td>
                                    <Button variant="primary" onClick={() => { navigate('/patients/' + patient.patientId) }} ><FontAwesomeIcon icon={faFileLines} /></Button>
                                </td>
                            </tr>
                        )}

                    </tbody>
                </Table>
                <BottomPagination currentPage={page} totalPages={data?.totalPages} selectPage={setPage} />
            </LoadingContent>
        </Section>
    )

}