import { securedHttp } from "./axios/SecuredHttp";

// Get patients by page and size of the page
const getPatientsPage = (page, size) => {
    return securedHttp()
        .get(`/patient?page=${page - 1}&size=${size}`)
        .then((response) => response.data)
}

// Get patient by id
const getPatient = (id) => {
    return securedHttp()
        .get(`/patient/${id}`)
        .then((response) => response.data)
};

// Add a patient
const addPatient = (patient) => {
    return securedHttp()
        .post(`/patient/add`, patient)
        .then((response) => response.data)
};

// Updte a patient
const updatePatient = (patient) => {
    return securedHttp()
        .put(`/patient/${patient.patientId}`, patient)
        .then((response) => response.data)
};

// delete a patient
const deletePatient = id => {
    return securedHttp()
        .delete(`/patient/${id}`)
        .then((response) => response.data)
};

export const patientService = {
    getPatientsPage,
    getPatient,
    addPatient,
    updatePatient,
    deletePatient
};