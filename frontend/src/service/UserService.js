import { securedHttp } from "./axios/SecuredHttp";

// Get users by page and size of page
const getUsersPage = (page, size) => {
    return securedHttp()
        .get(`/auth/account?page=${page - 1}&size=${size}`)
        .then((response) => response.data)
};

// Get user by id
const getUser = (id) => {
    return securedHttp()
        .get(`/auth/account/${id}`)
        .then((response) => response.data)
};

// Assign a role to a user by id
const assignRole = (id, role) => {
    return securedHttp()
        .patch(`/auth/account/${id}/assignRole`, role)
        .then((response) => response.data)
};

// Reset user password by id
const resetPassword = (id, password) => {
    return securedHttp()
        .patch(`/auth/account/${id}/resetPassword`, password)
        .then((response) => response.data)
};

export const userService = {
    getUsersPage,
    getUser,
    assignRole,
    resetPassword,
};