import { securedHttp } from "./axios/SecuredHttp";

// Get medical record by patient id and by record page and size of the page
const getMedicalHistory = (patientId, page, size) => {
    return securedHttp()
        .get(`/patHistory/patient/${patientId}?page=${page - 1}&size=${size}`)
        .then((response) => response.data)
};

// Get a medical record by medical record
const getMedicalRecord = (id) => {
    return securedHttp()
        .get(`/patHistory/${id}`)
        .then((response) => response.data)
};

// Add a medical record
const addMedicalRecord = (record) => {
    return securedHttp()
        .post(`/patHistory/add`, record)
        .then((response) => response.data)
};

// Update a medical record
const updateMedicalRecord = (record) => {
    return securedHttp()
        .put(`/patHistory/${record.recordId}`, record)
        .then((response) => response.data)
};

// Delete a medical record
const deleteMedicalRecord = (id) => {
    return securedHttp()
        .delete(`/patHistory/${id}`)
        .then((response) => response.data)
};

export const historyService = {
    getMedicalHistory,
    getMedicalRecord,
    addMedicalRecord,
    updateMedicalRecord,
    deleteMedicalRecord
};