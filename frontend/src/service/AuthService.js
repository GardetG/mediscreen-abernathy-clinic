import { publicHttp } from "./axios/PublicHttp";

const login = (credentials) => {
    return publicHttp()
        .post(`/auth/login`, credentials)
        .then((response) => response.data)
};

const register = (user) => {
    return publicHttp()
        .post(`/auth/register`, user)
        .then((response) => response.data)
};

export const authService = {
    login,
    register
}