import axios from "axios";

// Instance of Axios used for public call to the Api
const apiUrl = "http://localhost:8080"
let axiosInstance = axios.create({baseURL : apiUrl});

export const publicHttp = () => axiosInstance;