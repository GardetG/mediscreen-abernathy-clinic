import axios from "axios";
import { publicHttp } from "./PublicHttp";

// Instance of Axios used for authenticated call to the Api.
const apiUrl = "http://localhost:8080"
let axiosInstance = axios.create({ baseURL: apiUrl });

export const securedHttp = () => axiosInstance;

// Configure interceptors for authenticated call.
export const configSecuredHttp = (user, onLogin, onLogout) => {
    axiosInstance = axios.create({ baseURL: apiUrl });

    // Interceptor to add token to request
    axiosInstance.interceptors.request.use((config) => {
        config.headers.Authorization = "Bearer "+ user?.token;
        return config;
    });

    // Interceptor to logout the user on 401 response
    axiosInstance.interceptors.response.use((response) => {
        return response;
    }, (error) => {
        if (error.response.status === 401) {
            refresh(user?.refreshToken, onLogin, onLogout)
            return;
        }
        return Promise.reject(error);
    });

}

const refresh = (refreshToken, onLogin, onLogout) => {
    const payload = {refreshToken: refreshToken}
    publicHttp()
    .post(`/auth/refresh`, payload)
    .then((response) => response.data)
    .then((response) => onLogin(response))
    .catch(onLogout())
}