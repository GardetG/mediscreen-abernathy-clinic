import { securedHttp } from "./axios/SecuredHttp";

// Get report by Id
const getReportById = (id) => {
    const payload = {patientId: id}
    return securedHttp()
        .post(`/assess/id`, payload)
        .then((response) => response.data)
};

// Get report by lastname
const getReportByLastname = (lastname) => {
    console.log(lastname)
    const payload = {lastname: lastname}
    return securedHttp()
        .post(`/assess/familyName`, payload)
        .then((response) => response.data)
};

export const reportService = {
    getReportById,
    getReportByLastname,
};