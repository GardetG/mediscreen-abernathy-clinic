import { useState } from "react"

// Local storage hook to synchronise storage and state.
export const useLocalStorage = (keyName) => {
    // Initialize with local storage value
    const [storedValue, setStoredValue] = useState(() => {
        const value = window.localStorage.getItem(keyName);
        if (value) {
            return JSON.parse(value)
        } else {
            return null;
        }
    })

    // on update save change in local storage
    const setValue = (newValue) => {
        if (newValue === null) {
            window.localStorage.removeItem(keyName);
        } else {
            window.localStorage.setItem(keyName, JSON.stringify(newValue));
        }
        setStoredValue(newValue);
    }

    return [storedValue, setValue];
}