import React, { useContext, createContext } from 'react'
import { configSecuredHttp } from '../service/axios/SecuredHttp';
import { useLocalStorage } from './useLocalStorage';

// Authentication context
export const AuthContext = createContext();

// Authentication context provider
export const AuthProvider = ({ children }) => {
    // bind authenticated user state with local storage
    const [user, setUser] = useLocalStorage("user");

    // Set user on login
    const handleLogin = (user) => {
        setUser(user);
    };

    // Clear user on logout
    const handleLogout = () => {
        setUser(null);
    };

    const value = {
        user,
        onLogin: handleLogin,
        onLogout: handleLogout
    };

    configSecuredHttp(user, handleLogin, handleLogout)

    return (
        <AuthContext.Provider value={value}>
            {children}
        </AuthContext.Provider>
    );
};

// Authentication context hook
export const useAuth = () => {
    return useContext(AuthContext);
}
