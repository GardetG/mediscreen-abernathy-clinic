import { useState } from "react";

// Custom hook to handle the state of a modal
export const useModal = () => {
    const [state, setState] = useState(false)

    const showModal = () => {
        setState(true);
    }

    const hideModal = () => {
        setState(false)
    }

    return {
        state,
        showModal,
        hideModal
    }

}