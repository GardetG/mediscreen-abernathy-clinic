import { useCallback, useState } from "react"

// Custom hook to call an api request and return loading state, response or error
export const useApi = (apiCall, onSuccess, onError) => {
    const [data, setData] = useState(null)
    const [error, setError] = useState(null)
    const [loading, setLoading] = useState(false)

    // perform the request and set response or error.
    const request = useCallback((...args) => {
        setLoading(true);
        apiCall(...args)
            .then((response) => {
                setData(response);
                setError(null);
                // on success call the onSuccess callback
                if (onSuccess) {
                    onSuccess(response)
                }
            })
            .catch((error) => {
                switch ( error.response.status) {
                    case 401:
                    case 404:
                    case 409:
                    case 503:
                        setError(`${error.response.status} : ${error.response.data}`);
                        break; 
                    case 400:
                        setError(`Sorry, a Bad Request error occured.`);
                        break; 
                    default:
                        setError(`Sorry, an unexpected error occured.`);
                }
                setData(null);
                // on error call the onError callback
                if (onError) {
                    onError(error);
                }
            })
            .finally(() => setLoading(false));
    }, [apiCall, onError, onSuccess])

    return {
        data,
        error,
        loading,
        request,
        setData
    }

}