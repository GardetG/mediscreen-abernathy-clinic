import './App.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { AuthProvider } from './context/AuthContext';
import { HomepageLayout } from './component/layout/HomepageLayout';
import { DashboardLayout } from './component/layout/DashboardLayout';
import { Login } from './pages/Login';
import { Register } from './pages/Register';
import Home from './pages/Home';
import Patients from './pages/Patients';
import Admin from './pages/Admin';
import PatientProfile from './pages/PatientProfile';
import { PrivateRoute } from './route/PrivateRoute';
import { PublicRoute } from './route/PublicRoute';

function App() {

  return (
    <BrowserRouter>
      <AuthProvider>
        <Routes>
          <Route element={<HomepageLayout />} >
            <Route index element={
              <PublicRoute>
                <Login />
              </PublicRoute>} />
            <Route path="login" element={
              <PublicRoute>
                <Login />
              </PublicRoute>} />
            <Route path="register" element={
              <PublicRoute>
                <Register />
              </PublicRoute>} />
          </Route>

          <Route element={<DashboardLayout />} >
            <Route path="home" element={<PrivateRoute><Home /></PrivateRoute>} />
            <Route path="patients" element={
              <PrivateRoute role={["ADMIN", "DOCTOR", "ORGANIZER"]}>
                <Patients />
              </PrivateRoute>} />
            <Route path="patients/:id" element={
              <PrivateRoute role={["ADMIN", "DOCTOR", "ORGANIZER"]}>
                <PatientProfile />
              </PrivateRoute>} />
            <Route path="admin" element={
              <PrivateRoute role={["ADMIN"]}>
                <Admin />
              </PrivateRoute>} />
          </Route>

          <Route path="*" element={<NoMatch />} />

        </Routes>
      </AuthProvider>
    </BrowserRouter>
  );
}

const NoMatch = () => {
  return (<div>Error 404: Nothing here</div>);
}

export default App;
