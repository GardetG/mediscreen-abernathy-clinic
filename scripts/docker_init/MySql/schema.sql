CREATE SCHEMA IF NOT EXISTS `mediscreen`;

USE `mediscreen`;

CREATE TABLE IF NOT EXISTS `patient` (
  `patient_id` int NOT NULL AUTO_INCREMENT,
  `firstname` varchar(20) NOT NULL,
  `lastname` varchar(20) NOT NULL,
  `date_of_birth` date NOT NULL,
  `gender` int NOT NULL,
  `address` varchar(50) DEFAULT NULL,
  `phone_number` char(12) DEFAULT NULL,
  `last_update`date NOT NULL,
  PRIMARY KEY (`patient_id`)
);

CREATE TABLE IF NOT EXISTS `token` (
  `token_id` int NOT NULL AUTO_INCREMENT,
  `expiry_date` datetime NOT NULL,
  `token` varchar(255) NOT NULL,
  `user_id` int NOT NULL,
  PRIMARY KEY (`token_id`)
);

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int NOT NULL AUTO_INCREMENT,
  `firstname` varchar(20) NOT NULL,
  `lastname` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` char(60) NOT NULL,
  `role` varchar(10) NOT NULL,
  PRIMARY KEY (`user_id`)
);

INSERT INTO `user` (`user_id`, `firstname`, `lastname`, `email`, `password`, `role`)
  values (1, 'Admin', 'Principal', 'admin@abernathy.com', '$2a$10$LyalOVgvR9fcTvjCT/SBleVB6ra85MRkJZ4DlHPPimxf/7y.dA4uS', 'ADMIN');

INSERT INTO `user` (`user_id`, `firstname`, `lastname`, `email`, `password`, `role`)
  values (2, 'Doctor', 'Test', 'doctor@abernathy.com', '$2a$10$LyalOVgvR9fcTvjCT/SBleVB6ra85MRkJZ4DlHPPimxf/7y.dA4uS', 'DOCTOR');