mkdir .temp
cp -r docs/* .temp

mkdir .temp/javadoc
cp -r build/docs/aggregateJavadoc/* .temp/javadoc

mkdir .temp/patientinfo
mkdir .temp/patientinfo/report
cp -r patientinfo/build/reports/tests/test/* .temp/patientinfo/report
mkdir .temp/patientinfo/jacoco
cp -r patientinfo/build/jacocoHtml/* .temp/patientinfo/jacoco

mkdir .temp/patienthistory
mkdir .temp/patienthistory/report
cp -r patienthistory/build/reports/tests/test/* .temp/patienthistory/report
mkdir .temp/patienthistory/jacoco
cp -r patienthistory/build/jacocoHtml/* .temp/patienthistory/jacoco

mkdir .temp/patientreport
mkdir .temp/patientreport/report
cp -r patientreport/build/reports/tests/test/* .temp/patientreport/report
mkdir .temp/patientreport/jacoco
cp -r patientreport/build/jacocoHtml/* .temp/patientreport/jacoco

mkdir .temp/apigateway
mkdir .temp/apigateway/report
cp -r apigateway/build/reports/tests/test/* .temp/apigateway/report
mkdir .temp/apigateway/jacoco
cp -r apigateway/build/jacocoHtml/* .temp/apigateway/jacoco

mkdir .temp/authserver
mkdir .temp/authserver/report
cp -r authserver/build/reports/tests/test/* .temp/authserver/report
mkdir .temp/authserver/jacoco
cp -r authserver/build/jacocoHtml/* .temp/authserver/jacoco

mv .temp public