INSERT INTO `patient` (`patient_id`, `firstname`, `lastname`, `date_of_birth`, `gender`, `address`, `phone_number`, `last_update`)
  values (1, 'Test', 'TestNone', '1966-12-31', 0, '1 Brookside St', '100-222-3333', '2022-01-01');

INSERT INTO `patient` (`patient_id`, `firstname`, `lastname`, `date_of_birth`, `gender`, `address`, `phone_number`, `last_update`)
  values (2, 'Test', 'TestBorderline', '1945-06-24', 1, '2 High St', '200-333-4444', '2022-01-01');

INSERT INTO `patient` (`patient_id`, `firstname`, `lastname`, `date_of_birth`, `gender`, `address`, `phone_number`, `last_update`)
  values (3, 'Test', 'TestInDanger', '2004-06-18', 1, '3 Club Road&phone', '300-444-5555', '2022-01-01');

INSERT INTO `patient` (`patient_id`, `firstname`, `lastname`, `date_of_birth`, `gender`, `address`, `phone_number`, `last_update`)
  values (4, 'Test', 'TestEarlyOnset', '2002-06-28', 0, '4 Valley Dr', '400-555-6666', '2022-01-01');