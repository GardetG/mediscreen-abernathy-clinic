CREATE SCHEMA IF NOT EXISTS `mediscreen`;

USE `mediscreen`;

CREATE TABLE IF NOT EXISTS `patient` (
  `patient_id` int NOT NULL AUTO_INCREMENT,
  `firstname` varchar(20) NOT NULL,
  `lastname` varchar(20) NOT NULL,
  `date_of_birth` date NOT NULL,
  `gender` int NOT NULL,
  `address` varchar(50) DEFAULT NULL,
  `phone_number` char(12) DEFAULT NULL,
  `last_update`date NOT NULL,
  PRIMARY KEY (`patient_id`)
);