package com.mediscreen.patientinfo.client;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.mediscreen.patientinfo.exception.ClientBadResponseException;
import com.mediscreen.patientinfo.exception.ClientException;
import com.mediscreen.patientinfo.exception.ClientUnavailableException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

@SpringBootTest
class PatientHistoryClientTest {

  @Autowired
  private PatientHistoryClient patientHistoryRestService;

  @MockBean
  private RestTemplate restTemplate;

  @DisplayName("Clear patient history should send a request to PatientHistory service")
  @Test
  void clearPatientHistoryTest() throws ClientException {
    // Given
    int patientId = 1;
    doNothing().when(restTemplate).delete(anyString());

    // When
    patientHistoryRestService.clearPatientHistory(patientId);

    // Then
    verify(restTemplate, times(1)).delete("/patHistory/patient/1");
  }

  @DisplayName("Clear patient history when PatientHistory service returns an unexpected response should throw an exception")
  @Test
  void clearPatientHistoryWhenErrorTest() {
    // Given
    int patientId = 1;
    doThrow(new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR)).when(restTemplate)
        .delete(anyString());

    // When
    assertThatThrownBy(() -> patientHistoryRestService.clearPatientHistory(patientId))

        // Then
        .isInstanceOf(ClientBadResponseException.class)
        .hasMessageContaining("Unable to clear medical history. Unexpected Response.");
    verify(restTemplate, times(1)).delete("/patHistory/patient/1");
  }

  @DisplayName("Clear patient history when PatientHistory service is unavailable should throw an exception")
  @Test
  void clearPatientHistoryWhenUnavailableTest() {
    // Given
    int patientId = 1;
    doThrow(new ResourceAccessException("Connection refused")).when(restTemplate)
        .delete(anyString());

    // When
    assertThatThrownBy(() -> patientHistoryRestService.clearPatientHistory(patientId))

        // Then
        .isInstanceOf(ClientUnavailableException.class)
        .hasMessageContaining("Unable to clear medical history. Service Unavailable.");
    verify(restTemplate, times(1)).delete("/patHistory/patient/1");
  }

}