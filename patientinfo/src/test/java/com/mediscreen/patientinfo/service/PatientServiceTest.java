package com.mediscreen.patientinfo.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.mediscreen.patientinfo.client.PatientHistoryClient;
import com.mediscreen.patientinfo.domain.Gender;
import com.mediscreen.patientinfo.domain.Patient;
import com.mediscreen.patientinfo.dto.PatientDto;
import com.mediscreen.patientinfo.dto.SearchDto;
import com.mediscreen.patientinfo.exception.ClientException;
import com.mediscreen.patientinfo.exception.ClientUnavailableException;
import com.mediscreen.patientinfo.exception.PatientAlreadyExistsException;
import com.mediscreen.patientinfo.exception.PatientNotFoundException;
import com.mediscreen.patientinfo.repository.PatientRepository;
import com.mediscreen.patientinfo.utils.TestUtils;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

@SpringBootTest
class PatientServiceTest {

  @Autowired
  private PatientService patientService;

  @MockBean
  private PatientRepository patientRepository;
  @MockBean
  private PatientHistoryClient patientHistoryClient;

  @Captor
  private ArgumentCaptor<Patient> patientCaptor;
  @Captor
  private ArgumentCaptor<Example<Patient>> exampleCaptor;

  private Patient patientTest;
  private PatientDto patientTestDto;
  private final LocalDate birthDate = LocalDate.ofYearDay(2000, 1);
  private final LocalDate lastUpdateDate = LocalDate.ofYearDay(2022, 1);

  @BeforeEach
  void setUp() {
    patientTest = TestUtils.createPatient(1, "Patient", "Test", birthDate,
        Gender.FEMALE, "Address", "000-000-0000", lastUpdateDate);
    patientTestDto = new PatientDto(1, "Patient", "Test", birthDate,
        Gender.FEMALE, "Address", "000-000-0000", lastUpdateDate);
  }

  @DisplayName("Get page of all patients should return page of patients")
  @Test
  void getAllPatientsTest() {
    // Given
    Pageable pageable = PageRequest.of(0, 5);
    LocalDate birthDate1 = LocalDate.parse("2000-01-01");
    LocalDate birthDate2 = LocalDate.parse("2000-01-02");
    Page<Patient> patientsPage = new PageImpl<>(List.of(
        new Patient("Patient1", "Test", birthDate1, Gender.FEMALE),
        new Patient("Patient2", "test", birthDate2, Gender.MALE)
    ), pageable, 2);
    when(patientRepository.findAll(any(Pageable.class))).thenReturn(patientsPage);

    // When
    Page<PatientDto> actualPatientsPage = patientService.getAllPatients(pageable);

    // Then
    assertThat(actualPatientsPage).usingRecursiveComparison()
        .isEqualTo(new PageImpl<>(List.of(
            new PatientDto(0, "Patient1", "Test", birthDate1, Gender.FEMALE, null, null, null),
            new PatientDto(0, "Patient2", "test", birthDate2, Gender.MALE, null, null, null)
        ), pageable, 2));
    verify(patientRepository, times(1)).findAll(pageable);
  }

  @DisplayName("Get page of all patients when no patients found should return an empty page")
  @Test
  void getAllPatientsWhenEmptyTest() {
    // Given
    Pageable pageable = PageRequest.of(0, 5);
    when(patientRepository.findAll(any(Pageable.class))).thenReturn(Page.empty());

    // When
    Page<PatientDto> actualPatients = patientService.getAllPatients(pageable);

    // Then
    assertThat(actualPatients).isEmpty();
    verify(patientRepository, times(1)).findAll(pageable);
  }

  @DisplayName("Search patient with example should return list of matching patients")
  @Test
  void searchPatientTest() {
    // Given
    SearchDto searchDto = new SearchDto(null, "Test",null, null);
    when(patientRepository.findAll(Mockito.<Example<Patient>>any())).thenReturn(List.of(patientTest));

    // When
    List<PatientDto> actualPatients = patientService.searchPatients(searchDto);

    // Then
    assertThat(actualPatients).usingRecursiveFieldByFieldElementComparator()
        .containsOnly(patientTestDto);
    verify(patientRepository, times(1)).findAll(exampleCaptor.capture());
    assertThat(exampleCaptor.getValue().getProbe()).usingRecursiveComparison()
        .isEqualTo(new Patient(null, "Test", null, null));
  }

  @DisplayName("Search patient with example should return list of matching patients")
  @Test
  void searchPatientWhenEmptyTest() {
    // Given
    SearchDto searchDto = new SearchDto(null, "NonExisting",null, null);
    when(patientRepository.findAll(Mockito.<Example<Patient>>any())).thenReturn(Collections.emptyList());

    // When
    List<PatientDto> actualPatients = patientService.searchPatients(searchDto);

    // Then
    assertThat(actualPatients).isEmpty();
    verify(patientRepository, times(1)).findAll(Mockito.<Example<Patient>>any());
  }

  @DisplayName("Get patient by id should return a patient Dto")
  @Test
  void getPatientByIdTest() throws PatientNotFoundException {
    // Given
    when(patientRepository.findById(anyInt())).thenReturn(Optional.of(patientTest));

    // When
    PatientDto actualPatientDto = patientService.getPatientById(1);

    // Then
    assertThat(actualPatientDto).usingRecursiveComparison().isEqualTo(patientTestDto);
    verify(patientRepository, times(1)).findById(1);
  }

  @DisplayName("Get patient by id when not found should throws an exception")
  @Test
  void getPatientByIdWhenNotFoundTest() {
    // Given
    when(patientRepository.findById(anyInt())).thenReturn(Optional.empty());

    // When
    assertThatThrownBy(() -> patientService.getPatientById(9))

        // Then
        .isInstanceOf(PatientNotFoundException.class)
        .hasMessageContaining("Patient not found");
    verify(patientRepository, times(1)).findById(9);
  }

  @DisplayName("Update a patient should update patient entity")
  @Test
  void updatePatientTest() throws PatientNotFoundException, PatientAlreadyExistsException {
    // Given
    PatientDto patientUpdate = new PatientDto(1, "Update", "Test",
        birthDate, Gender.MALE, "UpdateAddress", "111-111-1111", null);
    when(patientRepository.findById(anyInt())).thenReturn(Optional.of(patientTest));
    when(patientRepository.exists(any())).thenReturn(false);
    LocalDate updateDate = LocalDate.ofYearDay(2022, 1);
    doAnswer(invocation -> TestUtils.mockUpdate(invocation.getArgument(0), updateDate))
        .when(patientRepository).saveAndFlush(any(Patient.class));

    // When
    PatientDto updatedPatientDto = patientService.updatePatient(1, patientUpdate);

    // Then
    assertThat(updatedPatientDto).usingRecursiveComparison().ignoringFields("lastUpdate")
        .isEqualTo(patientUpdate);
    assertThat(updatedPatientDto.getLastUpdate()).isEqualTo(updateDate);
    verify(patientRepository, times(1)).findById(1);
    verify(patientRepository, times(1)).exists(exampleCaptor.capture());
    verify(patientRepository, times(1)).saveAndFlush(patientCaptor.capture());
    Patient expectedPatient = TestUtils.createPatient(1, "Update", "Test",
        birthDate, Gender.MALE, "UpdateAddress", "111-111-1111", updateDate);
    assertThat(patientCaptor.getValue()).usingRecursiveComparison().isEqualTo(TestUtils
        .createPatient(1, "Update", "Test", birthDate, Gender.MALE,
            "UpdateAddress", "111-111-1111", updateDate));
    assertThat(exampleCaptor.getValue().getProbe()).usingRecursiveComparison()
        .isEqualTo(new Patient("Update", "Test", birthDate, null));
  }

  @DisplayName("Update a patient with names and birthdate unchanged should not check if already exists")
  @Test
  void updatePatientWithNamesAndBirthdayUnchangedTest()
      throws PatientNotFoundException, PatientAlreadyExistsException {
    // Given
    PatientDto patientUpdate =
        new PatientDto(0, "Patient", "Test", LocalDate.ofYearDay(2000, 1), Gender.MALE,
            "UpdateAddress", "111-111-1111", null);
    when(patientRepository.findById(anyInt())).thenReturn(Optional.of(patientTest));

    // When
    patientService.updatePatient(1, patientUpdate);

    // Then
    verify(patientRepository, times(1)).findById(1);
    verify(patientRepository, times(0)).exists(any());
    verify(patientRepository, times(1)).saveAndFlush(any(Patient.class));
  }

  @DisplayName("Update a patient with already existing names and birthdate combination should throw an exception")
  @Test
  void updatePatientWhenAlreadyExistTest() {
    // Given
    PatientDto patientUpdate =
        new PatientDto(0, "AlreadyExist", "Test", LocalDate.ofYearDay(1990, 1), Gender.MALE,
            "Address", "111-111-1111", null);
    when(patientRepository.findById(anyInt())).thenReturn(Optional.of(patientTest));
    when(patientRepository.exists(any())).thenReturn(true);

    // When
    assertThatThrownBy(() -> patientService.updatePatient(1, patientUpdate))

        // Then
        .isInstanceOf(PatientAlreadyExistsException.class)
        .hasMessageContaining("A patient with same names and birthdate already exists");
    verify(patientRepository, times(1)).findById(1);
    verify(patientRepository, times(1)).exists(any());
    verify(patientRepository, times(0)).saveAndFlush(any(Patient.class));
  }

  @DisplayName("Update a not found patient should throw an exception")
  @Test
  void updatePatientWhenNotFoundTest() {
    // Given
    PatientDto patientUpdate =
        new PatientDto(9, "Update", "Test", LocalDate.parse("2000-01-01"), Gender.MALE,
            "UpdateAddress", "111-111-1111", null);
    when(patientRepository.findById(anyInt())).thenReturn(Optional.empty());

    // When
    assertThatThrownBy(() -> patientService.updatePatient(9, patientUpdate))

        // Then
        .isInstanceOf(PatientNotFoundException.class)
        .hasMessageContaining("Patient not found");
    verify(patientRepository, times(1)).findById(9);
    verify(patientRepository, times(0)).exists(any());
    verify(patientRepository, times(0)).saveAndFlush(any(Patient.class));
  }

  @DisplayName("Add a patient should save new patient entity")
  @Test
  void AddPatientTest() throws PatientAlreadyExistsException {
    // Given
    PatientDto patientAdd =
        new PatientDto(0, "newPatient", "Test", LocalDate.parse("2000-01-01"), Gender.FEMALE,
            "Address", "000-000-0000", null);
    when(patientRepository.exists(any())).thenReturn(false);
    LocalDate creationDate = LocalDate.ofYearDay(2022, 1);
    doAnswer(invocation -> TestUtils.mockCreate(invocation.getArgument(0), creationDate))
        .when(patientRepository).save(any(Patient.class));

    // When
    PatientDto addedPatient = patientService.addPatient(patientAdd);

    // Then
    assertThat(addedPatient).usingRecursiveComparison().ignoringFields("patientId", "lastUpdate")
        .isEqualTo(patientAdd);
    assertThat(addedPatient.getPatientId()).isEqualTo(1);
    assertThat(addedPatient.getLastUpdate()).isEqualTo(creationDate);
    verify(patientRepository, times(1)).exists(exampleCaptor.capture());
    verify(patientRepository, times(1)).save(patientCaptor.capture());
    assertThat(exampleCaptor.getValue().getProbe()).usingRecursiveComparison()
        .isEqualTo(new Patient("newPatient", "Test", LocalDate.parse("2000-01-01"), null));
    assertThat(patientCaptor.getValue()).usingRecursiveComparison().isEqualTo(TestUtils
        .createPatient(1, "newPatient", "Test",
            LocalDate.parse("2000-01-01"), Gender.FEMALE, "Address", "000-000-0000", creationDate));
  }

  @DisplayName("Add a patient with already existing names and birthdate combination should throw an exception")
  @Test
  void AddPatientWhenAlreadyExistTest() {
    // Given
    PatientDto patientAdd =
        new PatientDto(0, "AlreadyExist", "Test", LocalDate.ofYearDay(1990, 1), Gender.FEMALE,
            "Address", "111-111-1111", null);
    when(patientRepository.exists(any())).thenReturn(true);

    // When
    assertThatThrownBy(() -> patientService.addPatient(patientAdd))

        // Then
        .isInstanceOf(PatientAlreadyExistsException.class)
        .hasMessageContaining("A patient with same names and birthdate already exists");
    verify(patientRepository, times(1)).exists(any());
    verify(patientRepository, times(0)).save(any(Patient.class));
  }

  @DisplayName("Remove a patient by id should delete it")
  @Test
  void removePatientTest() throws PatientNotFoundException, ClientException {
    // Given
    when(patientRepository.findById(anyInt())).thenReturn(Optional.of(patientTest));

    // When
    patientService.removePatient(1);

    // Then
    verify(patientRepository, times(1)).findById(1);
    verify(patientRepository, times(1)).delete(patientCaptor.capture());
    assertThat(patientCaptor.getValue()).isEqualTo(patientTest);
    verify(patientHistoryClient, times(1)).clearPatientHistory(1);
  }

  @DisplayName("Remove a patient by id when not found should throws an exception")
  @Test
  void removePatientWhenNotFoundTest() throws ClientException {
    // Given
    when(patientRepository.findById(anyInt())).thenReturn(Optional.empty());

    // When
    assertThatThrownBy(() -> patientService.removePatient(9))

        // Then
        .isInstanceOf(PatientNotFoundException.class)
        .hasMessageContaining("Patient not found");
    verify(patientRepository, times(1)).findById(9);
    verify(patientRepository, times(0)).delete(any(Patient.class));
    verify(patientHistoryClient, times(0)).clearPatientHistory(9);
  }

  @DisplayName("Remove a patient while a ClientException is thrown should forward the exception")
  @Test
  void removePatientWhenClientExceptionTest() throws ClientException {
    // Given
    when(patientRepository.findById(anyInt())).thenReturn(Optional.of(patientTest));
    doThrow(new ClientUnavailableException("Unable to clear medical history."))
        .when(patientHistoryClient).clearPatientHistory(anyInt());

    // When
    assertThatThrownBy(() -> patientService.removePatient(1))

        // Then
        .isInstanceOf(ClientException.class);
    verify(patientRepository, times(1)).findById(1);
    verify(patientRepository, times(1)).delete(any(Patient.class));
    verify(patientHistoryClient, times(1)).clearPatientHistory(1);
  }

}