package com.mediscreen.patientinfo.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import com.mediscreen.patientinfo.domain.Gender;
import com.mediscreen.patientinfo.dto.PatientDto;
import com.mediscreen.patientinfo.dto.SearchDto;
import com.mediscreen.patientinfo.exception.ClientUnavailableException;
import com.mediscreen.patientinfo.exception.PatientAlreadyExistsException;
import com.mediscreen.patientinfo.exception.PatientNotFoundException;
import com.mediscreen.patientinfo.exception.ClientBadResponseException;
import com.mediscreen.patientinfo.service.PatientService;
import java.time.LocalDate;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest
class PatientControllerTest {

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private PatientService patientService;

  @Captor
  private ArgumentCaptor<PatientDto> patientDtoCaptor;
  @Captor
  private ArgumentCaptor<SearchDto> searchDtoCaptor;

  private final ObjectMapper mapper = new ObjectMapper().registerModule(new JavaTimeModule());

  @DisplayName("GET all patients page should return 200 with patients page")
  @Test
  void getAllPatientsPageTest() throws Exception {
    // GIVEN
    LocalDate birthDate1 = LocalDate.parse("2000-01-01");
    LocalDate birthDate2 = LocalDate.parse("2000-01-02");
    Page<PatientDto> patientsPage = new PageImpl<>(List.of(
        new PatientDto(1, "Patient1", "Test", birthDate1, Gender.FEMALE, null, null, null),
        new PatientDto(2, "Patient2", "test", birthDate2, Gender.MALE, null, null, null)
    ));
    when(patientService.getAllPatients(any(Pageable.class)))
        .thenReturn(patientsPage);

    // WHEN
    mockMvc.perform(get("/patient?page=0&size=5"))

        // THEN
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.content", hasSize(2)))
        .andExpect(jsonPath("$.content[0].patientId", is(1)))
        .andExpect(jsonPath("$.content[0].firstname", is("Patient1")))
        .andExpect(jsonPath("$.content[1].patientId", is(2)))
        .andExpect(jsonPath("$.content[1].firstname", is("Patient2")));
    verify(patientService, times(1)).getAllPatients(PageRequest.of(0, 5));
  }

  @DisplayName("POST a patient search should return 200 with list of matching patient")
  @Test
  void searchPatientTest() throws Exception {
    // GIVEN
    LocalDate birthDate =  LocalDate.parse("2000-01-01");
    LocalDate updateDate = LocalDate.parse("2022-01-01");
    SearchDto searchDto = new SearchDto(null, "Test", null,null);
    when(patientService.searchPatients(any(SearchDto.class)))
        .thenReturn(List.of(new PatientDto(1, "NewPatient", "Test", birthDate, Gender.FEMALE,"Address", "000-000-0001", updateDate)));


    // WHEN
    mockMvc.perform(post("/patient/search")
            .contentType(MediaType.APPLICATION_JSON)
            .content(mapper.writeValueAsString(searchDto)))

        // THEN
        .andExpect(status().isOk())
        .andExpect(jsonPath("$", hasSize(1)))
        .andExpect(jsonPath("$[0].patientId", is(1)))
        .andExpect(jsonPath("$[0].firstname", is("NewPatient")))
        .andExpect(jsonPath("$[0].lastname", is("Test")));
    verify(patientService, times(1)).searchPatients(searchDtoCaptor.capture());
    assertThat(searchDtoCaptor.getValue()).usingRecursiveComparison().isEqualTo(searchDto);
  }

  @DisplayName("GET patient by Id should return 200 with patient")
  @Test
  void getPatientByIdTest() throws Exception {
    // GIVEN
    LocalDate updateDate = LocalDate.parse("2022-01-01");
    PatientDto patient = new PatientDto(1, "Patient1", "Test", LocalDate.parse("2000-01-01"), Gender.FEMALE,"Address", "000-000-0001", updateDate);
    when(patientService.getPatientById(anyInt())).thenReturn(patient);
    // WHEN
    mockMvc.perform(get("/patient/1"))

        // THEN
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.patientId", is(1)))
        .andExpect(jsonPath("$.firstname", is("Patient1")))
        .andExpect(jsonPath("$.lastname", is("Test")))
        .andExpect(jsonPath("$.dateOfBirth", is("2000-01-01")))
        .andExpect(jsonPath("$.gender", is("F")))
        .andExpect(jsonPath("$.address", is("Address")))
        .andExpect(jsonPath("$.phoneNumber", is("000-000-0001")))
        .andExpect(jsonPath("$.lastUpdate", is("2022-01-01")));
    verify(patientService, times(1)).getPatientById(1);
  }

  @DisplayName("GET patient by Id when not found should return 404")
  @Test
  void getPatientByIdWhenNotFoundTest() throws Exception {
    // GIVEN
    when(patientService.getPatientById(anyInt())).
        thenThrow(new PatientNotFoundException("Patient not found", 9));

    // WHEN
    mockMvc.perform(get("/patient/9"))

        // THEN
        .andExpect(status().isNotFound())
        .andExpect(jsonPath("$", is("Patient not found")));
    verify(patientService, times(1)).getPatientById(9);
  }

  @DisplayName("PUT update on patient should return 200")
  @Test
  void updatePatientTest() throws Exception {
    // GIVEN
    LocalDate updateDate = LocalDate.parse("2022-01-01");
    PatientDto patientDto = new PatientDto(0, "UpdatePatient", "Test", LocalDate.parse("2000-01-01"), Gender.FEMALE,"Address", "000-000-0001", null);
    when(patientService.updatePatient(anyInt(), any(PatientDto.class)))
        .thenReturn(new PatientDto(1, "UpdatePatient", "Test", LocalDate.parse("2000-01-01"), Gender.FEMALE,"Address", "000-000-0001", updateDate));

    // WHEN
    mockMvc.perform(put("/patient/1")
            .contentType(MediaType.APPLICATION_JSON)
            .content(mapper.writeValueAsString(patientDto)))

        // THEN
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.patientId", is(1)))
        .andExpect(jsonPath("$.firstname", is("UpdatePatient")))
        .andExpect(jsonPath("$.lastname", is("Test")))
        .andExpect(jsonPath("$.dateOfBirth", is("2000-01-01")))
        .andExpect(jsonPath("$.gender", is("F")))
        .andExpect(jsonPath("$.address", is("Address")))
        .andExpect(jsonPath("$.phoneNumber", is("000-000-0001")))
        .andExpect(jsonPath("$.lastUpdate", is("2022-01-01")));
    verify(patientService, times(1)).updatePatient(eq(1), patientDtoCaptor.capture());
    assertThat(patientDtoCaptor.getValue()).usingRecursiveComparison().isEqualTo(patientDto);
  }

  @DisplayName("PUT update on patient with invalid dto should return 422")
  @Test
  void updatePatientWhenInvalidTest() throws Exception {
    // GIVEN
    PatientDto patientDto = new PatientDto(0, "", "TooLongLastnameToBeValid", LocalDate.parse("2999-01-01"),null,null, "WrongFormat", null);

    // WHEN
    mockMvc.perform(put("/patient/9")
            .contentType(MediaType.APPLICATION_JSON)
            .content(mapper.writeValueAsString(patientDto)))

        // THEN
        .andExpect(status().isUnprocessableEntity())
        .andExpect(jsonPath("$.firstname", is("must not be blank")))
        .andExpect(jsonPath("$.lastname", is("size must be between 0 and 20")))
        .andExpect(jsonPath("$.dateOfBirth", is("must be a past date")))
        .andExpect(jsonPath("$.gender", is("must not be null")))
        .andExpect(jsonPath("$.phoneNumber", is("must match 000-000-0000 format")));
    verify(patientService, times(0)).updatePatient(anyInt(), any(PatientDto.class));
  }

  @DisplayName("PUT update on patient when already existing should return 409")
  @Test
  void updatePatientWhenAlreadyExistsTest() throws Exception {
    // GIVEN
    PatientDto patientDto = new PatientDto(0, "AlreadyExists", "Test", LocalDate.parse("2000-01-01"), Gender.FEMALE,"Address", "000-000-0001", null);
    when(patientService.updatePatient(anyInt(), any(PatientDto.class)))
        .thenThrow(new PatientAlreadyExistsException("A patient with same names and birthdate already exists"));

    // WHEN
    mockMvc.perform(put("/patient/9")
            .contentType(MediaType.APPLICATION_JSON)
            .content(mapper.writeValueAsString(patientDto)))

        // THEN
        .andExpect(status().isConflict())
        .andExpect(jsonPath("$", is("A patient with same names and birthdate already exists")));
    verify(patientService, times(1)).updatePatient(anyInt(), any(PatientDto.class));
  }

  @DisplayName("PUT update on patient when not found should return 404")
  @Test
  void updatePatientWhenNotFoundTest() throws Exception {
    // GIVEN
    PatientDto patientDto = new PatientDto(0, "NotFound", "Test", LocalDate.parse("2000-01-01"), Gender.FEMALE,"Address", "000-000-0001", null);
    when(patientService.updatePatient(anyInt(), any(PatientDto.class)))
        .thenThrow(new PatientNotFoundException("Patient not found", 9));

    // WHEN
    mockMvc.perform(put("/patient/9")
            .contentType(MediaType.APPLICATION_JSON)
            .content(mapper.writeValueAsString(patientDto)))

        // THEN
        .andExpect(status().isNotFound())
        .andExpect(jsonPath("$", is("Patient not found")));
    verify(patientService, times(1)).updatePatient(anyInt(), any(PatientDto.class));
  }

  @DisplayName("POST a new patient should return 201")
  @Test
  void addPatientTest() throws Exception {
    // GIVEN
    LocalDate updateDate = LocalDate.parse("2022-01-01");
    PatientDto patientDto = new PatientDto(0, "NewPatient", "Test", LocalDate.parse("2000-01-01"), Gender.FEMALE,"Address", "000-000-0001", null);
    when(patientService.addPatient(any(PatientDto.class)))
        .thenReturn(new PatientDto(1, "NewPatient", "Test", LocalDate.parse("2000-01-01"), Gender.FEMALE,"Address", "000-000-0001", updateDate));

    // WHEN
    mockMvc.perform(post("/patient/add")
            .contentType(MediaType.APPLICATION_JSON)
            .content(mapper.writeValueAsString(patientDto)))

        // THEN
        .andExpect(status().isCreated())
        .andExpect(jsonPath("$.patientId", is(1)))
        .andExpect(jsonPath("$.firstname", is("NewPatient")))
        .andExpect(jsonPath("$.lastname", is("Test")))
        .andExpect(jsonPath("$.dateOfBirth", is("2000-01-01")))
        .andExpect(jsonPath("$.gender", is("F")))
        .andExpect(jsonPath("$.address", is("Address")))
        .andExpect(jsonPath("$.phoneNumber", is("000-000-0001")))
        .andExpect(jsonPath("$.lastUpdate", is("2022-01-01")));
    verify(patientService, times(1)).addPatient(patientDtoCaptor.capture());
    assertThat(patientDtoCaptor.getValue()).usingRecursiveComparison().isEqualTo(patientDto);
  }

  @DisplayName("POST a new patient with invalid dto should return 422")
  @Test
  void addPatientWhenInvalidTest() throws Exception {
    // GIVEN
    PatientDto patientDto = new PatientDto(1, "TooLongFirstnameToBeValid", "", LocalDate.parse("2999-01-01"),null,null, "0000-000-000", null);

    // WHEN
    mockMvc.perform(post("/patient/add")
            .contentType(MediaType.APPLICATION_JSON)
            .content(mapper.writeValueAsString(patientDto)))

        // THEN
        .andExpect(status().isUnprocessableEntity())
        .andExpect(jsonPath("$.firstname", is("size must be between 0 and 20")))
        .andExpect(jsonPath("$.lastname", is("must not be blank")))
        .andExpect(jsonPath("$.dateOfBirth", is("must be a past date")))
        .andExpect(jsonPath("$.gender", is("must not be null")))
        .andExpect(jsonPath("$.phoneNumber", is("must match 000-000-0000 format")));
    verify(patientService, times(0)).addPatient(any(PatientDto.class));
  }

  @DisplayName("POST a new patient when already existing should return 409")
  @Test
  void addPatientWhenAlreadyExistsTest() throws Exception {
    // GIVEN
    PatientDto patientDto = new PatientDto(1, "AlreadyExists", "Test", LocalDate.parse("2000-01-01"), Gender.FEMALE,"Address", "000-000-0001", null);
    when(patientService.addPatient(any(PatientDto.class)))
        .thenThrow(new PatientAlreadyExistsException("A patient with sames name and birthdate already exists"));

    // WHEN
    mockMvc.perform(post("/patient/add")
            .contentType(MediaType.APPLICATION_JSON)
            .content(mapper.writeValueAsString(patientDto)))

        // THEN
        .andExpect(status().isConflict())
        .andExpect(jsonPath("$", is("A patient with sames name and birthdate already exists")));
    verify(patientService, times(1)).addPatient(any(PatientDto.class));
  }

  @DisplayName("DELETE patient should return 204")
  @Test
  void removePatientTest() throws Exception {
    // WHEN
    mockMvc.perform(delete("/patient/1"))

        // THEN
        .andExpect(status().isNoContent());
    verify(patientService, times(1)).removePatient(1);
  }

  @DisplayName("DELETE patient when not found should return 404")
  @Test
  void RemovePatientWhenNotFoundTest() throws Exception {
    // GIVEN
    doThrow(new PatientNotFoundException("Patient not found", 9)).when(patientService).removePatient(anyInt());

    // WHEN
    mockMvc.perform(delete("/patient/9"))

        // THEN
        .andExpect(status().isNotFound())
        .andExpect(jsonPath("$", is("Patient not found")));
    verify(patientService, times(1)).removePatient(9);
  }

  @DisplayName("DELETE patient when PatientHistory return unexpected response should return 502")
  @Test
  void RemovePatientWhenPatientHistoryBadResponseTest() throws Exception {
    // GIVEN
    doThrow(new ClientBadResponseException("Unable to clear medical history.", "Error 500"))
        .when(patientService).removePatient(anyInt());

    // WHEN
    mockMvc.perform(delete("/patient/1"))

        // THEN
        .andExpect(status().isBadGateway())
        .andExpect(jsonPath("$", is("Unable to clear medical history. Unexpected Response.")));
    verify(patientService, times(1)).removePatient(1);
  }

  @DisplayName("DELETE patient when PatientHistory service is unavailable should return 503")
  @Test
  void RemovePatientWhenPatientHistoryUnavailableTest() throws Exception {
    // GIVEN
    doThrow(new ClientUnavailableException("Unable to clear medical history."))
        .when(patientService).removePatient(anyInt());

    // WHEN
    mockMvc.perform(delete("/patient/1"))

        // THEN
        .andExpect(status().isServiceUnavailable())
        .andExpect(jsonPath("$", is("Unable to clear medical history. Service Unavailable.")));
    verify(patientService, times(1)).removePatient(1);
  }

}