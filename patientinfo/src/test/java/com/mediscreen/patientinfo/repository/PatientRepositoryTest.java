package com.mediscreen.patientinfo.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.mediscreen.patientinfo.domain.Gender;
import com.mediscreen.patientinfo.domain.Patient;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@DataJpaTest
@EnableJpaAuditing
class PatientRepositoryTest {

  @Autowired
  private PatientRepository patientRepository;

  private final ExampleMatcher matcher = ExampleMatcher
      .matchingAll()
      .withIgnoreCase()
      .withIgnorePaths("patientId");

  @DisplayName("Find all patients should return a page containing all patients")
  @Test
  void findAllTest() {
    // Given
    Pageable pageable = PageRequest.of(0, 5);

    // When
    Page<Patient> actualPatients = patientRepository.findAll(pageable);

    //Then
    assertThat(actualPatients.getContent()).hasSize(4);
  }

  @DisplayName("Find all patients with example should return a list of matching patients")
  @Test
  void findAllWithExampleTest() {
    // Given
    Patient patient = new Patient(null, "TestNone", null, null);
    Example<Patient> criteria = Example.of(patient, matcher);

    // When
    List<Patient> actualPatients = patientRepository.findAll(criteria);

    //Then
    assertThat(actualPatients).hasSize(1);
  }

  @DisplayName("Find patient by Id should return an optional of the patient")
  @Test
  void findByIdTest() {
    // Given
    int patientId = 1;

    // When
    Optional<Patient> actualPatient = patientRepository.findById(patientId);

    //Then
    assertThat(actualPatient).isPresent();
  }

  @DisplayName("Find patient by Id when patient is not found should return an empty optional")
  @Test
  void findByIdWhenNotFoundTest() {
    // Given
    int patientId = 9;

    // When
    Optional<Patient> actualPatient = patientRepository.findById(patientId);

    //Then
    assertThat(actualPatient).isEmpty();
  }

  @DisplayName("Check patient existence by example when existing should return true")
  @Test
  void existsByExampleTest() {
    // Given
    String firstname = "Test";
    String lastname = "TestNone";
    LocalDate dateOfBirth = LocalDate.parse("1966-12-31");
    Patient patient = new Patient(firstname, lastname, dateOfBirth, null);
    Example<Patient> criteria = Example.of(patient, matcher);

    // When
    boolean result = patientRepository.exists(criteria);

    //Then
    assertThat(result).isTrue();
  }

  @DisplayName("Check patient existence by example when not existing should return false")
  @Test
  void notExistsByExampleTest() {
    // Given
    String firstname = "Test";
    String lastname = "PatientNonExisting";
    LocalDate dateOfBirth = LocalDate.now();
    Patient patient = new Patient(firstname, lastname, dateOfBirth, null);
    Example<Patient> criteria = Example.of(patient, matcher);

    // When
    boolean result = patientRepository.exists(criteria);

    //Then
    assertThat(result).isFalse();
  }

  @DisplayName("Save a patient should save it and return the saved patient with a defined id")
  @Test
  void saveTest() {
    // Given
    LocalDate birthDate = LocalDate.ofYearDay(2000,1);
    Patient patient = new Patient("Patient", "Test", birthDate, Gender.FEMALE);
    patient.setAddress("Address");
    patient.setPhoneNumber("000-000-0000");

    // When
    Patient actualPatient = patientRepository.save(patient);

    // Then
    assertThat(actualPatient)
        .usingRecursiveComparison().ignoringFields("patientId", "lastUpdate").isEqualTo(patient);
    assertThat(actualPatient.getPatientId()).isNotZero();
    assertThat(actualPatient.getLastUpdate()).isNotNull();
  }

  @DisplayName("Save an existing patient should update it")
  @Test
  void saveUpdateTest() {
    // Given
    Patient patient = patientRepository.findById(1)
        .orElseThrow(IllegalStateException::new);
    patient.setFirstname("TestUpdate");
    LocalDate lastUpdate = patient.getLastUpdate();

    // When
    Patient actualPatient = patientRepository.saveAndFlush(patient);

    // Then
    assertThat(actualPatient).usingRecursiveComparison().isEqualTo(patient);
    assertThat(actualPatient.getFirstname()).isEqualTo("TestUpdate");
    assertThat(actualPatient.getLastUpdate()).isNotEqualTo(lastUpdate);
  }

  @DisplayName("Save an patient violating unique constraint should throw an exception")
  @Test
  void saveWhenAlreadyExistsTest() {
    // Given
    LocalDate dateOfBirth = LocalDate.parse("1966-12-31");
    Patient patient = new Patient("Test", "TestNone", dateOfBirth, Gender.MALE);

    // When
    assertThatThrownBy(() -> patientRepository.save(patient))

        // Then
        .isInstanceOf(DataIntegrityViolationException.class);
  }

  @DisplayName("Delete a patient should remove it from the database")
  @Test
  void deleteTest() {
    // Given
    LocalDate birthDate = LocalDate.ofYearDay(2000,1);
    Patient patientTest = new Patient("Patient", "Test", birthDate, Gender.FEMALE);
    Patient patient = patientRepository.save(patientTest);

    // When
    patientRepository.delete(patient);

        // Then
    assertThat(patientRepository.findById(patient.getPatientId())).isEmpty();
  }

}