package com.mediscreen.patientinfo.integration;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.jayway.jsonpath.JsonPath;
import java.net.URI;
import org.json.JSONObject;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
class PatientInformationIntegrationTest {

  @Autowired
  private MockMvc mockMvc;
  @Autowired
  private RestTemplate restTemplate;

  @DisplayName("As a doctor I want to view the personal information of my patients to check their identity")
  @Test
  void ViewPatientPersonalInformationTest() throws Exception {
    // WHEN A doctor request page with all patients' resume
    MvcResult result = mockMvc.perform(get("/patient?page=0&size=5"))

        // THEN He receives a page with patients' resume
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.content", hasSize(4)))
        .andReturn();

    // WHEN A doctor choose a patient and request personal information
    String json = result.getResponse().getContentAsString();
    int id = JsonPath.read(json, "$.content[0].patientId");
    mockMvc.perform(get("/patient/" + id))

        // THEN He receives the patient's personal information
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.patientId", is(1)))
        .andExpect(jsonPath("$.firstname", is("Test")))
        .andExpect(jsonPath("$.lastname", is("TestNone")))
        .andExpect(jsonPath("$.dateOfBirth", is("1966-12-31")))
        .andExpect(jsonPath("$.gender", is("F")))
        .andExpect(jsonPath("$.address", is("1 Brookside St")))
        .andExpect(jsonPath("$.phoneNumber", is("100-222-3333")))
        .andExpect(jsonPath("$.lastUpdate").isNotEmpty());
  }

  @DisplayName("As a doctor I want to update the personal information of my patients to keep it updated")
  @Test
  void UpdatePatientPersonalInformationTest() throws Exception {
    // GIVEN The personal information to update
    JSONObject jsonObject = new JSONObject()
        .put("firstname", "Update")
        .put("lastname", "Test")
        .put("dateOfBirth", "1991-05-13")
        .put("gender", "F")
        .put("address", "1 Test St")
        .put("phoneNumber", "999-999-9999");

    // WHEN A doctor update the patient's personal information
    mockMvc.perform(put("/patient/2")
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonObject.toString()))

        // THEN The patient's personam information are up to date
        .andExpect(status().isOk());
    mockMvc.perform(get("/patient/2"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.patientId", is(2)))
        .andExpect(jsonPath("$.firstname", is("Update")))
        .andExpect(jsonPath("$.lastname", is("Test")))
        .andExpect(jsonPath("$.dateOfBirth", is("1991-05-13")))
        .andExpect(jsonPath("$.gender", is("F")))
        .andExpect(jsonPath("$.address", is("1 Test St")))
        .andExpect(jsonPath("$.phoneNumber", is("999-999-9999")))
        .andExpect(jsonPath("$.lastUpdate").isNotEmpty());
  }

  @DisplayName("As a doctor I want to add the personal information of my patients to fix appointments")
  @Test
  void addPatientPersonalInformationTest() throws Exception {
    // GIVEN The personal information to add
    JSONObject jsonObject = new JSONObject()
        .put("firstname", "New")
        .put("lastname", "Test")
        .put("dateOfBirth", "1995-05-12")
        .put("gender", "F")
        .put("address", "1 Test St")
        .put("phoneNumber", "999-999-9999");

    // WHEN A doctor add the patient's personal information
    MvcResult result = mockMvc.perform(post("/patient/add")
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonObject.toString()))

        // THEN The patient's personam information are added
        .andExpect(status().isCreated())
        .andReturn();
    String json = result.getResponse().getContentAsString();
    int id = JsonPath.read(json, "$.patientId");
    mockMvc.perform(get("/patient/" + id))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.patientId", is(id)))
        .andExpect(jsonPath("$.firstname", is("New")))
        .andExpect(jsonPath("$.lastname", is("Test")))
        .andExpect(jsonPath("$.dateOfBirth", is("1995-05-12")))
        .andExpect(jsonPath("$.gender", is("F")))
        .andExpect(jsonPath("$.address", is("1 Test St")))
        .andExpect(jsonPath("$.phoneNumber", is("999-999-9999")))
        .andExpect(jsonPath("$.lastUpdate").isNotEmpty());
  }

  @DisplayName("As a doctor I want to remove the personal information of my patients to respect their privacy")
  @Test
  void removePatientPersonalInformationTest() throws Exception {
    // Given PatientHistory service available
    MockRestServiceServer mockServer = MockRestServiceServer.createServer(restTemplate);
    mockServer.expect(ExpectedCount.once(),
            requestTo(new URI("http://localhost:8082/patHistory/patient/3")))
        .andExpect(method(HttpMethod.DELETE))
        .andRespond(withStatus(HttpStatus.NO_CONTENT));

    // WHEN A doctor remove the patient's personal information
    mockMvc.perform(delete("/patient/3"))

        // THEN The patient's personam information are deleted
        .andExpect(status().isNoContent());
    mockMvc.perform(get("/patient/3"))
        .andExpect(status().isNotFound());
  }

  @DisplayName("As a doctor/service I want to search patients to fetch patients matching the criteria")
  @Test
  void SearchPatientPersonalInformationTest() throws Exception {
    // GIVEN the search criteria
    JSONObject jsonObject = new JSONObject()
        .put("lastname", "testearlyonset");

    // WHEN A doctor request to search patients
    mockMvc.perform(post("/patient/search")
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonObject.toString()))

        // THEN He receives a list of patients matching the criteria
        .andExpect(status().isOk())
        .andExpect(jsonPath("$", hasSize(1)))
        .andExpect(jsonPath("$[0].patientId", is(4)))
        .andExpect(jsonPath("$[0].firstname", is("Test")))
        .andExpect(jsonPath("$[0].lastname", is("TestEarlyOnset")))
        .andReturn();
  }

}
