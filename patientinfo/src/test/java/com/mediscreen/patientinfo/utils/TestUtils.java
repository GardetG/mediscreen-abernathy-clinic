package com.mediscreen.patientinfo.utils;

import com.mediscreen.patientinfo.domain.Gender;
import com.mediscreen.patientinfo.domain.Patient;
import java.time.LocalDate;
import org.springframework.test.util.ReflectionTestUtils;

/**
 * Utility class used for testing
 */
public class TestUtils {

  /**
   * Create an instance of Patient for testing purposes and manually set the fields usually handled
   * by the framework and database.
   *
   * @param patientId   id of the patient
   * @param firstname   firstname of the patient
   * @param lastname    lastname of the patient
   * @param birthDate   date of birth of the patient
   * @param gender      gender of the patient
   * @param address     address of the patient
   * @param phoneNumber phone number of the patient
   * @param lastUpdate  date of the last update of the patient information
   * @return patient
   */
  public static Patient createPatient(int patientId, String firstname, String lastname,
                                      LocalDate birthDate, Gender gender, String address,
                                      String phoneNumber, LocalDate lastUpdate) {
    Patient patient = new Patient(firstname, lastname, birthDate, gender);
    patient.setAddress(address);
    patient.setPhoneNumber(phoneNumber);
    ReflectionTestUtils.setField(patient, "patientId", patientId);
    ReflectionTestUtils.setField(patient, "lastUpdate", lastUpdate);
    return patient;
  }

  /**
   * Mock the update of a patient in repository by manually setting the last update date.
   *
   * @param patient patient on which apply update
   * @param lastUpdate date of the mocked update
   * @return patient
   */
  public static Patient mockUpdate(Patient patient, LocalDate lastUpdate) {
    ReflectionTestUtils.setField(patient, "lastUpdate", lastUpdate);
    return patient;
  }

  /**
   * Mock the creation of a patient in repository by manually setting id and update date.
   *
   * @param patient patient on which apply the creation
   * @param lastUpdate date of the mocked creation
   * @return patient
   */
  public static Patient mockCreate(Patient patient, LocalDate lastUpdate) {
    ReflectionTestUtils.setField(patient, "patientId", 1);
    ReflectionTestUtils.setField(patient, "lastUpdate", lastUpdate);
    return patient;
  }

}
