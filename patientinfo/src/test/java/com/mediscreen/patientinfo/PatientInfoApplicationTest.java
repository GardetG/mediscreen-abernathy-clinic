package com.mediscreen.patientinfo;

import static org.assertj.core.api.Assertions.assertThat;

import com.mediscreen.patientinfo.controller.PatientController;
import com.mediscreen.patientinfo.repository.PatientRepository;
import com.mediscreen.patientinfo.service.PatientService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class PatientInfoApplicationTest {

  @Autowired
  private PatientRepository patientRepository;
  @Autowired
  private PatientService patientService;
  @Autowired
  private PatientController patientController;

  @Test
  void contextLoads() {
    assertThat(patientRepository).isNotNull();
    assertThat(patientService).isNotNull();
    assertThat(patientController).isNotNull();
  }

}