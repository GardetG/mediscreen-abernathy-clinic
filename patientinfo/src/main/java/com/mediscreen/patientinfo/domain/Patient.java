package com.mediscreen.patientinfo.domain;

import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 * Entity class of a Patient with personal information.
 */
@Entity
@Table(name = "patient", uniqueConstraints = {@UniqueConstraint(name = "UniquePatient",
    columnNames = {"firstname", "lastname", "date_of_birth"})})
@EntityListeners(AuditingEntityListener.class)
public class Patient {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "patient_id")
  private int patientId;
  @Column(name = "firstname")
  private String firstname;
  @Column(name = "lastname")
  private String lastname;
  @Column(name = "date_of_birth")
  private LocalDate dateOfBirth;
  @Column(name = "gender")
  private Gender gender;
  @Column(name = "address")
  private String address;
  @Column(name = "phone_number")
  private String phoneNumber;
  @LastModifiedDate
  @Column(name = "last_update")
  private LocalDate lastUpdate;

  /**
   * Constructor method to create a new Patient entity.
   *
   * @param firstname   firstname of the patient
   * @param lastname    lastname of the patient
   * @param dateOfBirth date of birth of the patient
   * @param gender      gender of the patient
   */
  public Patient(String firstname, String lastname, LocalDate dateOfBirth,
                 Gender gender) {
    this.firstname = firstname;
    this.lastname = lastname;
    this.dateOfBirth = dateOfBirth;
    this.gender = gender;
  }

  private Patient() {
    // private constructor needed by hibernate
  }

  public int getPatientId() {
    return patientId;
  }

  public String getFirstname() {
    return firstname;
  }

  public void setFirstname(String firstname) {
    this.firstname = firstname;
  }

  public String getLastname() {
    return lastname;
  }

  public void setLastname(String lastname) {
    this.lastname = lastname;
  }

  public LocalDate getDateOfBirth() {
    return dateOfBirth;
  }

  public void setDateOfBirth(LocalDate dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }

  public Gender getGender() {
    return gender;
  }

  public void setGender(Gender gender) {
    this.gender = gender;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public LocalDate getLastUpdate() {
    return lastUpdate;
  }

}
