package com.mediscreen.patientinfo.repository;

import com.mediscreen.patientinfo.domain.Patient;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;

/**
 * Repository interface for {@link Patient} entities.
 */
@Repository
public interface PatientRepository extends JpaRepository<Patient, Integer> {

  /**
   * Find all patients and return a page of patients according to the paging restriction provided
   * in the {@link Pageable} object.
   *
   * @param pageable to request a paged result, can be {@link Pageable#unpaged()}, must not be null
   * @return a Page of {@link Patient Patients}
   */
  @NonNull
  Page<Patient> findAll(@NonNull Pageable pageable);

  /**
   * Find all patients matching the given Example.
   * In case no match could be found then an empty list is returned.
   *
   * @param example Example use for finding patient, must not be null
   * @param <S>     {@link Patient} or derived entity
   * @return a List of {@link Patient} matching the given Example
   */
  @NonNull
  <S extends Patient> List<S> findAll(@NonNull Example<S> example);

  /**
   * Find a patient by the provided id and return an Optional of it.
   * If no patient with such id could be found then an empty Optional is returned.
   *
   * @param patientId id of the {@link Patient}
   * @return an Optional of the {@link Patient}
   */
  Optional<Patient> findById(int patientId);

  /**
   * Checks if a patient that match the given Example exists.
   *
   * @param example Example used for the existence check, must not be null.
   * @return true if a {@link Patient} matching the Example exists
   */
  <S extends Patient> boolean exists(@NonNull Example<S> example);

  /**
   * Save a new patient or update an existing one.
   * If unique constraints are violated then a DataIntegrityViolationException is thrown.
   *
   * @param patient {@link Patient} to save or update
   * @param <T>     {@link Patient} or derived entity
   * @return saved {@link Patient}
   */
  @NonNull
  <T extends Patient> T save(@NonNull T patient);

  /**
   * Delete a patient.
   *
   * @param patient {@link Patient} to delete
   */
  void delete(@NonNull Patient patient);

}
