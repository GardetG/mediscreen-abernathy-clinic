package com.mediscreen.patientinfo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Patient Info main class.
 */
@SpringBootApplication
public class PatientInfoApplication {

  public static void main(String[] args) {
    SpringApplication.run(PatientInfoApplication.class, args);
  }

}
