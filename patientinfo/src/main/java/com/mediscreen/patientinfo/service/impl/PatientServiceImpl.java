package com.mediscreen.patientinfo.service.impl;

import com.mediscreen.patientinfo.client.PatientHistoryClient;
import com.mediscreen.patientinfo.domain.Patient;
import com.mediscreen.patientinfo.dto.PatientDto;
import com.mediscreen.patientinfo.dto.SearchDto;
import com.mediscreen.patientinfo.exception.PatientAlreadyExistsException;
import com.mediscreen.patientinfo.exception.PatientNotFoundException;
import com.mediscreen.patientinfo.repository.PatientRepository;
import com.mediscreen.patientinfo.service.PatientService;
import com.mediscreen.patientinfo.utils.PatientMapper;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service class implementation to manage {@link Patient}.
 */
@Service
public class PatientServiceImpl implements PatientService {

  @Autowired
  private PatientRepository patientRepository;
  @Autowired
  private PatientHistoryClient patientHistoryClient;

  private final ExampleMatcher matcher = ExampleMatcher.matchingAll()
      .withIgnoreCase()
      .withIgnorePaths("patientId");

  /**
   * {@inheritDoc}
   */
  @Override
  public Page<PatientDto> getAllPatients(Pageable pageable) {
    return patientRepository.findAll(pageable)
        .map(PatientMapper::toDto);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public List<PatientDto> searchPatients(SearchDto searchDto) {
    Patient reference = PatientMapper.toEntity(searchDto);
    Example<Patient> criteria = Example.of(reference, matcher);
    return patientRepository.findAll(criteria)
        .stream()
        .map(PatientMapper::toDto)
        .collect(Collectors.toList());
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public PatientDto getPatientById(int patientId) throws PatientNotFoundException {
    Patient patient = getPatientOrThrowsException(patientId);
    return PatientMapper.toDto(patient);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  @Transactional
  public PatientDto updatePatient(int patientId, PatientDto patientUpdate)
      throws PatientNotFoundException, PatientAlreadyExistsException {
    Patient patientToUpdate = getPatientOrThrowsException(patientId);
    if (hasUniqueConstraintFieldsChanged(patientUpdate, patientToUpdate)) {
      checkUniqueConstraintViolation(patientUpdate);
    }
    PatientMapper.updateEntity(patientToUpdate, patientUpdate);
    patientRepository.saveAndFlush(patientToUpdate);
    return PatientMapper.toDto(patientToUpdate);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  @Transactional
  public PatientDto addPatient(PatientDto patientAdd) throws PatientAlreadyExistsException {
    checkUniqueConstraintViolation(patientAdd);
    Patient patientToAdd = PatientMapper.toEntity(patientAdd);
    patientRepository.save(patientToAdd);
    return PatientMapper.toDto(patientToAdd);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  @Transactional
  public void removePatient(int patientId) throws PatientNotFoundException {
    Patient patientToRemove = getPatientOrThrowsException(patientId);
    patientRepository.delete(patientToRemove);
    patientHistoryClient.clearPatientHistory(patientId);
  }

  /**
   * Get the requested patient by the provided id.
   * If no patient with such id could be found then an PatientNotFoundException is thrown.
   *
   * @param patientId id of the patient
   * @return the requested patient
   * @throws PatientNotFoundException when patient not found
   */
  private Patient getPatientOrThrowsException(int patientId) throws PatientNotFoundException {
    return patientRepository.findById(patientId)
        .orElseThrow(() -> new PatientNotFoundException("Patient not found.", patientId));
  }

  /**
   * Check if any fields with unique constraint change on update like names or date of birth.
   *
   * @param patientDto PatientDto with update information
   * @param patient    patient to update
   * @return true if any fields change on update
   */
  private boolean hasUniqueConstraintFieldsChanged(PatientDto patientDto, Patient patient) {
    return !(new EqualsBuilder()
        .append(patientDto.getFirstname(), patient.getFirstname())
        .append(patientDto.getLastname(), patient.getLastname())
        .append(patientDto.getDateOfBirth(), patient.getDateOfBirth())
        .isEquals());
  }

  /**
   * Check if a patient with same unique constraint fields that the provided ones already exists.
   * If such patient exists then an PatientAlreadyExistsException is thrown.
   *
   * @param patient PatientDto with update information
   * @throws PatientAlreadyExistsException when patient already exists
   */
  private void checkUniqueConstraintViolation(PatientDto patient)
      throws PatientAlreadyExistsException {
    Patient reference =
        new Patient(patient.getFirstname(), patient.getLastname(), patient.getDateOfBirth(), null);
    Example<Patient> criteria = Example.of(reference, matcher);
    if (patientRepository.exists(criteria)) {
      throw new PatientAlreadyExistsException(
          "A patient with same names and birthdate already exists.");
    }
  }

}
