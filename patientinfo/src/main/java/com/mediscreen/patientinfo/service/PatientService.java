package com.mediscreen.patientinfo.service;

import com.mediscreen.patientinfo.client.PatientHistoryClient;
import com.mediscreen.patientinfo.domain.Patient;
import com.mediscreen.patientinfo.dto.PatientDto;
import com.mediscreen.patientinfo.dto.SearchDto;
import com.mediscreen.patientinfo.exception.PatientAlreadyExistsException;
import com.mediscreen.patientinfo.exception.PatientNotFoundException;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service interface to manage {@link Patient}.
 */
public interface PatientService {

  /**
   * Get all patients and return a pages of the patients according to the paging restriction
   * provided in the {@link Pageable} object.
   *
   * @param pageable to request a paged result, can be @link Pageable#unpaged()}, must not be null
   * @return a Page of {@link PatientDto}
   */
  Page<PatientDto> getAllPatients(Pageable pageable);

  /**
   * Search all patients corresponding to the criteria provided by the Dto.
   *
   * @param searchDto {@link SearchDto} with criteria to search patients
   * @return a List af {@link PatientDto} matching the criteria
   */
  List<PatientDto> searchPatients(SearchDto searchDto);

  /**
   * Get a patient by the provided id.
   * If no patient with such id could be found then an {@link PatientNotFoundException} is thrown.
   *
   * @param patientId id of the {@link Patient}
   * @return the requested {@link PatientDto}
   * @throws PatientNotFoundException when patient not found
   */
  PatientDto getPatientById(int patientId) throws PatientNotFoundException;

  /**
   * Update a patient with the information provided by the Dto.
   * If the patient could not be found by the provided id or the patient already exists then
   * respectively an {@link PatientNotFoundException} or {@link PatientAlreadyExistsException}
   * is thrown.
   *
   * @param patientId     id of the {@link Patient}
   * @param patientUpdate {@link PatientDto} with the patient's information to update
   * @return the updated {@link PatientDto}
   * @throws PatientNotFoundException      when patient not found
   * @throws PatientAlreadyExistsException when patient already exists.
   */
  PatientDto updatePatient(int patientId, PatientDto patientUpdate) throws PatientNotFoundException,
      PatientAlreadyExistsException;

  /**
   * Add a new patient with the information provided by the Dto.
   * If the patient already exists then an {@link PatientAlreadyExistsException} is thrown.
   *
   * @param patientAdd {@link PatientDto} with the patient's information to add
   * @return the added {@link PatientDto}
   * @throws PatientAlreadyExistsException when patient already exists.
   */
  PatientDto addPatient(PatientDto patientAdd) throws PatientAlreadyExistsException;

  /**
   * Remove the patient by to the provided id.
   * If no patient with such id could be found then an {@link PatientNotFoundException} is thrown.
   * To ensure data consistency a request to clear the patient's medical history is made through
   * {@link PatientHistoryClient}, if the request failed with exception then the removal of the
   * patient is rolled back.
   *
   * @param patientId id of the {@link Patient}
   * @throws PatientNotFoundException when patient not found
   */
  void removePatient(int patientId) throws PatientNotFoundException;

}
