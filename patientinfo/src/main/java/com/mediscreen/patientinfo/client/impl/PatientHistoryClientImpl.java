package com.mediscreen.patientinfo.client.impl;

import com.mediscreen.patientinfo.client.PatientHistoryClient;
import com.mediscreen.patientinfo.exception.ClientBadResponseException;
import com.mediscreen.patientinfo.exception.ClientUnavailableException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

/**
 * Client class implementation to perform request to the PatientHistory service.
 */
@Service
public class PatientHistoryClientImpl implements PatientHistoryClient {

  @Autowired
  private RestTemplate restTemplate;

  /**
   * {@inheritDoc}
   */
  @Override
  public void clearPatientHistory(int patientId) {
    String url = String.format("/patHistory/patient/%s", patientId);
    try {
      restTemplate.delete(url);
    } catch (RestClientResponseException ex) {
      String response = String.format("Error: %d - %s", ex.getRawStatusCode(), ex.getStatusText());
      throw new ClientBadResponseException("Unable to clear medical history.", response);
    } catch (ResourceAccessException ex) {
      throw new ClientUnavailableException("Unable to clear medical history.");
    }
  }

}
