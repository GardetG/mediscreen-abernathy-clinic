package com.mediscreen.patientinfo.client;

import com.mediscreen.patientinfo.domain.Patient;
import com.mediscreen.patientinfo.exception.ClientBadResponseException;
import com.mediscreen.patientinfo.exception.ClientException;
import com.mediscreen.patientinfo.exception.ClientUnavailableException;

/**
 * Client interface to perform request to the PatientHistory service.
 */
public interface PatientHistoryClient {

  /**
   * Request to clear the patient's medical history by the provided patient id.
   *
   * @param patientId id of the {@link Patient}
   * @throws ClientBadResponseException when PatientHistory service returned an unexpected response
   * @throws ClientUnavailableException when PatientHistory service is unavailable
   *
   */
  void clearPatientHistory(int patientId) throws ClientException;

}
