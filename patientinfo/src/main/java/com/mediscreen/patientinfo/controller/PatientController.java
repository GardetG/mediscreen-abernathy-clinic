package com.mediscreen.patientinfo.controller;

import com.mediscreen.patientinfo.dto.PatientDto;
import com.mediscreen.patientinfo.dto.SearchDto;
import com.mediscreen.patientinfo.exception.ClientException;
import com.mediscreen.patientinfo.exception.PatientAlreadyExistsException;
import com.mediscreen.patientinfo.exception.PatientNotFoundException;
import com.mediscreen.patientinfo.service.PatientService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import java.util.List;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller class exposing API end-points to manage patients' personal information.
 */
@RestController
@RequestMapping("/patient")
public class PatientController {

  private static final Logger LOGGER = LoggerFactory.getLogger(PatientController.class);

  @Autowired
  private PatientService patientService;

  /**
   * GET the requested page of patients' personal information.
   *
   * @param pageable to request a paged result
   * @return HTTP 200 with the Page of {@link PatientDto}
   */
  @Operation(description = "View all patients' resume by pages", tags = {"Patient"})
  @GetMapping("")
  public ResponseEntity<Page<PatientDto>> getAllPatientsResumePage(Pageable pageable) {
    LOGGER.info("Request: Get page {} of patients", pageable.getPageNumber());
    Page<PatientDto> patientsResumePage = patientService.getAllPatients(pageable);

    LOGGER.info("Response: Page {} of patients sent", pageable.getPageNumber());
    return ResponseEntity.ok(patientsResumePage);
  }

  /**
   * POST a search request with the criteria provided by the request payload.
   *
   * @param searchDto {@link SearchDto} with the criteria to search
   * @return HTTP 200 with a List of {@link PatientDto} matching patients
   */
  @Operation(description = "Search patients matching the request payload", tags = {"Patient"})
  @PostMapping("/search")
  public ResponseEntity<List<PatientDto>> searchPatients(@RequestBody SearchDto searchDto) {
    LOGGER.info("Request: Search patients");
    List<PatientDto> patients = patientService.searchPatients(searchDto);

    LOGGER.info("Response: Search patient result sent");
    return ResponseEntity.ok(patients);
  }

  /**
   * GET the requested patient's personal information by the provided id.
   *
   * @param patientId id of the patient
   * @return HTTP 200 with {@link PatientDto}
   * @throws PatientNotFoundException when patient not found
   */
  @Operation(description = "View a patient", tags = {"Patient"})
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "OK"),
      @ApiResponse(responseCode = "404", description = "NOT FOUND - Patient not found",
          content = @Content),
  })
  @GetMapping("/{patientId}")
  public ResponseEntity<PatientDto> getPatientById(@PathVariable int patientId)
      throws PatientNotFoundException {
    LOGGER.info("Request: Get patient {} personal information", patientId);
    PatientDto patient = patientService.getPatientById(patientId);

    LOGGER.info("Response: Patient {} personal information sent", patientId);
    return ResponseEntity.ok(patient);
  }

  /**
   * PUT an update on a patient's personal information with the information provided by the request
   * payload.
   *
   * @param patientId  id of the patient
   * @param patientDto {@link PatientDto} with the patient's personal information to update
   * @return HTTP 200 with updated {@link PatientDto}
   * @throws PatientNotFoundException      when patient not found
   * @throws PatientAlreadyExistsException when patient already exists
   */
  @Operation(description = "Update a patient", tags = {"Patient"})
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "OK - Patient updated"),
      @ApiResponse(responseCode = "404", description = "NOT FOUND - Patient not found",
          content = @Content),
      @ApiResponse(responseCode = "409",
          description = "CONFLICT - A Patient with same names and birthdate already exists",
          content = @Content),
      @ApiResponse(responseCode = "422",
          description = "UNPROCESSABLE ENTITY - Some fields of the provided patient are invalid",
          content = @Content)
  })
  @PutMapping("/{patientId}")
  public ResponseEntity<PatientDto> updatePatient(@PathVariable int patientId,
                                                  @Valid @RequestBody PatientDto patientDto)
      throws PatientNotFoundException, PatientAlreadyExistsException {
    LOGGER.info("Request: Update patient {} personal information", patientId);
    PatientDto patient = patientService.updatePatient(patientId, patientDto);

    LOGGER.info("Response: Patient {} personal information updated", patientId);
    return ResponseEntity.ok(patient);
  }

  /**
   * POST a new patient's personal information with the information provided by the request payload.
   *
   * @param patientDto {@link PatientDto} with the patient's personal information to add
   * @return HTTP 201 with added {@link PatientDto}
   * @throws PatientAlreadyExistsException when patient already exists
   */
  @Operation(description = "Add a new patient", tags = {"Patient"})
  @ApiResponses(value = {
      @ApiResponse(responseCode = "201", description = "CREATED - Patient added"),
      @ApiResponse(responseCode = "409",
          description = "CONFLICT - A Patient with same names and birthdate already exists",
          content = @Content),
      @ApiResponse(responseCode = "422",
          description = "UNPROCESSABLE ENTITY - Some fields of the provided patient are invalid",
          content = @Content)
  })
  @PostMapping("/add")
  public ResponseEntity<PatientDto> addPatient(@Valid @RequestBody PatientDto patientDto)
      throws PatientAlreadyExistsException {
    LOGGER.info("Request: Add a new patient");
    PatientDto patient = patientService.addPatient(patientDto);

    LOGGER.info("Response: New patient {} added", patient.getPatientId());
    return ResponseEntity.status(HttpStatus.CREATED).body(patient);
  }

  /**
   * DELETE a patient's personal information by the provided record id.
   *
   * @param patientId id of the patient
   * @return 201
   * @throws PatientNotFoundException when patient not found
   * @throws ClientException          when the request to external service fails
   */
  @Operation(description = "Remove a patient", tags = {"Patient"})
  @ApiResponses(value = {
      @ApiResponse(responseCode = "204", description = "NO CONTENT - Patient removed"),
      @ApiResponse(responseCode = "404", description = "NOT FOUND - Patient not found",
          content = @Content),
      @ApiResponse(responseCode = "502",
          description = "BAD GATEWAY - Unable to remove patient since PatientHistory returns an "
              + "unexpected response while clearing medical history", content = @Content),
      @ApiResponse(responseCode = "503",
          description = "SERVICE UNAVAILABLE - Unable to remove patient since PatientHistory is "
              + "unavailable to clear medical history", content = @Content),
  })
  @DeleteMapping("/{patientId}")
  public ResponseEntity<Void> removePatient(@PathVariable int patientId)
      throws PatientNotFoundException {
    LOGGER.info("Request: Remove patient {} personal information", patientId);
    patientService.removePatient(patientId);

    LOGGER.info("Response: Patient {} personal information removed", patientId);
    return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
  }

}
