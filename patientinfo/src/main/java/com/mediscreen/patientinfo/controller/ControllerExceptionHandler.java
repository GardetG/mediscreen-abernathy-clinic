package com.mediscreen.patientinfo.controller;

import com.mediscreen.patientinfo.exception.ClientBadResponseException;
import com.mediscreen.patientinfo.exception.ClientUnavailableException;
import com.mediscreen.patientinfo.exception.PatientAlreadyExistsException;
import com.mediscreen.patientinfo.exception.PatientNotFoundException;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * Class handling exceptions thrown by Service in Controller and generate the corresponding HTTP
 * response.
 */
@ControllerAdvice
public class ControllerExceptionHandler {

  private static final Logger LOGGER = LoggerFactory.getLogger(ControllerExceptionHandler.class);

  /**
   * Handle {@link PatientNotFoundException} thrown when the patient can't be found.
   *
   * @param ex instance of the exception
   * @return HTTP 404 response
   */
  @ExceptionHandler(PatientNotFoundException.class)
  public ResponseEntity<String> handlePatientNotFoundException(PatientNotFoundException ex) {
    LOGGER.warn("{} Id: {}", ex.getMessage(), ex.getId());
    String error = ex.getMessage();
    LOGGER.info("Response: 404 {}", error);
    return ResponseEntity.status(HttpStatus.NOT_FOUND).body(error);
  }

  /**
   * Handle PatientAlreadyExistsException thrown when the patient already exists.
   *
   * @param ex instance of the exception
   * @return HTTP 409 response
   */
  @ExceptionHandler(PatientAlreadyExistsException.class)
  public ResponseEntity<String> handlePatientAlreadyExistsException(
      PatientAlreadyExistsException ex) {
    LOGGER.warn(ex.getMessage());
    String error = ex.getMessage();
    LOGGER.info("Response: 409 {}", error);
    return ResponseEntity.status(HttpStatus.CONFLICT).body(error);
  }

  /**
   * Handle ClientBadResponseException thrown when others services are unavailable.
   *
   * @param ex instance of the exception
   * @return HTTP 503 response
   */
  @ExceptionHandler(ClientBadResponseException.class)
  public ResponseEntity<String> handleClientBadResponseException(ClientBadResponseException ex) {
    LOGGER.error("{} {}", ex.getMessage(), ex.getResponse());
    HttpStatus status = HttpStatus.BAD_GATEWAY;
    String error = ex.getMessage();
    LOGGER.info("Response: {} - {}", status.value(), status.getReasonPhrase());
    return ResponseEntity.status(status).body(error);
  }

  /**
   * Handle ClientUnavailableException thrown when others services are unavailable.
   *
   * @param ex instance of the exception
   * @return HTTP 503 response
   */
  @ExceptionHandler(ClientUnavailableException.class)
  public ResponseEntity<String> handleClientUnavailableException(ClientUnavailableException ex) {
    LOGGER.error(ex.getMessage());
    HttpStatus status = HttpStatus.SERVICE_UNAVAILABLE;
    String error = ex.getMessage();
    LOGGER.info("Response: {} - {}", status.value(), status.getReasonPhrase());
    return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body(error);
  }

  /**
   * Handle MethodArgumentNotValidException thrown when validation failed.
   *
   * @param ex instance of the exception
   * @return HTTP 422 response with information on invalid fields
   */
  @ExceptionHandler(MethodArgumentNotValidException.class)
  public ResponseEntity<Map<String, String>> handleValidationExceptions(
      MethodArgumentNotValidException ex) {
    LOGGER.warn("Invalid Dto: {}", ex.getMessage());
    Map<String, String> errors = new HashMap<>();
    ex.getBindingResult().getAllErrors().forEach(error -> {
      String fieldName = ((FieldError) error).getField();
      String errorMessage = error.getDefaultMessage();
      errors.put(fieldName, errorMessage);
    });
    LOGGER.info("Response : 422 invalid DTO");
    return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(errors);
  }

}
