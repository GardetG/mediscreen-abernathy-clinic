package com.mediscreen.patientinfo.utils;

import com.mediscreen.patientinfo.domain.Patient;
import com.mediscreen.patientinfo.dto.PatientDto;
import com.mediscreen.patientinfo.dto.SearchDto;

/**
 * Mapper Class to map {@link Patient} entity into Dto.
 */
public class PatientMapper {

  private PatientMapper() {
    throw new IllegalStateException("Utility class");
  }

  /**
   * Map a Patient entity into PatientDto.
   *
   * @param patient {@link Patient} to map
   * @return corresponding {@link PatientDto} mapped
   */
  public static PatientDto toDto(Patient patient) {
    return new PatientDto(
        patient.getPatientId(),
        patient.getFirstname(),
        patient.getLastname(),
        patient.getDateOfBirth(),
        patient.getGender(),
        patient.getAddress(),
        patient.getPhoneNumber(),
        patient.getLastUpdate()
    );
  }

  /**
   * Update a Patient entity by mapping PatientDto fields.
   *
   * @param patient {@link Patient} to update
   * @param patientDto {@link PatientDto} to map
   */
  public static void updateEntity(Patient patient, PatientDto patientDto) {
    patient.setFirstname(patientDto.getFirstname());
    patient.setLastname(patientDto.getLastname());
    patient.setDateOfBirth(patientDto.getDateOfBirth());
    patient.setGender(patientDto.getGender());
    patient.setAddress(patientDto.getAddress());
    patient.setPhoneNumber(patientDto.getPhoneNumber());
  }

  /**
   * Map a PatientDto into Patient.
   *
   * @param patientDto {@link PatientDto} to map
   * @return corresponding {@link Patient} mapped
   */
  public static Patient toEntity(PatientDto patientDto) {
    Patient patient = new Patient(
        patientDto.getFirstname(),
        patientDto.getLastname(),
        patientDto.getDateOfBirth(),
        patientDto.getGender()
    );
    patient.setAddress(patientDto.getAddress());
    patient.setPhoneNumber(patientDto.getPhoneNumber());
    return patient;
  }

  /**
   * Map a SearchDto into a reference Patient.
   *
   * @param searchDto {@link SearchDto} to map
   * @return corresponding {@link Patient} mapped
   */
  public static Patient toEntity(SearchDto searchDto) {
    return new Patient(
        searchDto.getFirstname(),
        searchDto.getLastname(),
        searchDto.getDateOfBirth(),
        searchDto.getGender()
    );
  }

}
