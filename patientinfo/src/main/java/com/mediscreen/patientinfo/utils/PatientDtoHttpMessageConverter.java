package com.mediscreen.patientinfo.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import com.mediscreen.patientinfo.dto.PatientDto;
import java.io.IOException;
import java.util.Map;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.lang.NonNull;

/**
 * Custom HttpMessageConverter class to convert form-urlencoded message into the corresponding
 * patient Dto.
 */
public class PatientDtoHttpMessageConverter extends AbstractHttpMessageConverter<PatientDto> {

  private static final FormHttpMessageConverter formHttpMessageConverter =
      new FormHttpMessageConverter();
  private static final ObjectMapper mapper = new ObjectMapper();

  @Override
  protected boolean supports(@NonNull Class<?> clazz) {
    return (PatientDto.class == clazz);
  }

  @Override
  @NonNull
  protected PatientDto readInternal(@NonNull Class<? extends PatientDto> clazz,
                                    @NonNull HttpInputMessage inputMessage) throws
      IOException, HttpMessageNotReadableException {
    Map<String, String> vals = formHttpMessageConverter.read(null, inputMessage).toSingleValueMap();
    mapper.registerModule(new ParameterNamesModule())
        .registerModule(new JavaTimeModule());
    return mapper.convertValue(vals, PatientDto.class);
  }

  @Override
  protected void writeInternal(@NonNull PatientDto myObject,
                               @NonNull HttpOutputMessage outputMessage) {
    // Not Implemented. Converter only use to deserialize form-urlencoded message.
  }

}