package com.mediscreen.patientinfo.exception;

/**
 * Exception thrown to prevent duplicate when trying to save the requested patient.
 */
public class PatientAlreadyExistsException extends Exception {

  public PatientAlreadyExistsException(String s) {
    super(s);
  }

}
