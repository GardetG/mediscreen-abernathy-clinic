package com.mediscreen.patientinfo.exception;

/**
 * Exception thrown when the requested external service fails to respond.
 */
public class ClientUnavailableException extends ClientException {

  public ClientUnavailableException(String msg) {
    super(String.format("%s Service Unavailable.", msg));
  }

}
