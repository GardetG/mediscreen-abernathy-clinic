package com.mediscreen.patientinfo.exception;

/**
 * Exception thrown when the requested external service returns an unexpected response.
 */
public class ClientBadResponseException extends ClientException {

  private final String response;

  public ClientBadResponseException(String msg, String response) {
    super(String.format("%s Unexpected Response.", msg));
    this.response = response;
  }

  public String getResponse() {
    return response;
  }

}
