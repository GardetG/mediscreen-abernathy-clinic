package com.mediscreen.patientinfo.exception;

/**
 * Abstract Exception for Exceptions thrown when the client send request to an external service.
 */
public abstract class ClientException extends RuntimeException {

  protected ClientException(String msg) {
    super(msg);
  }

}
