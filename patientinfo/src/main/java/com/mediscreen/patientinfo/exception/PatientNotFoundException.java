package com.mediscreen.patientinfo.exception;

/**
 * Exception thrown when the requested patient is not found.
 */
public class PatientNotFoundException extends Exception {

  private final int id;

  public PatientNotFoundException(String s, int id) {
    super(s);
    this.id = id;
  }

  public int getId() {
    return id;
  }

}
