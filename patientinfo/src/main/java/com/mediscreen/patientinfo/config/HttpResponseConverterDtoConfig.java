package com.mediscreen.patientinfo.config;

import com.mediscreen.patientinfo.utils.PatientDtoHttpMessageConverter;
import java.nio.charset.StandardCharsets;
import java.util.List;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Configuration class to add the patient Dto converter to handle form-urlencoded message.
 */
@Configuration
public class HttpResponseConverterDtoConfig implements WebMvcConfigurer {

  @Override
  public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
    converters.add(converter());
  }

  private PatientDtoHttpMessageConverter converter() {
    PatientDtoHttpMessageConverter converter = new PatientDtoHttpMessageConverter();
    MediaType mediaType = new MediaType("application", "x-www-form-urlencoded",
        StandardCharsets.UTF_8);
    converter.setSupportedMediaTypes(List.of(mediaType));
    return converter;
  }

}