package com.mediscreen.patientinfo.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

/**
 * Configuration class to enable Jpa Auditing.
 */
@Configuration
@EnableJpaAuditing
public class JpaAuditingConfig { }