package com.mediscreen.patientinfo.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;

/**
 * Configuration class for service client with url:
 * - patientHistoryUrl: Url of the PatientHistory service.
 */
@Component
@ConfigurationProperties
public class ClientConfig {

  private String patientHistoryUrl;

  public String getPatientHistoryUrl() {
    return patientHistoryUrl;
  }

  public void setPatientHistoryUrl(String patientHistoryUrl) {
    this.patientHistoryUrl = patientHistoryUrl;
  }

  /**
   * RestTemplate Bean.
   *
   * @return restTemplate
   */
  @Bean
  public RestTemplate restTemplate() {
    RestTemplate restTemplate = new RestTemplate();
    restTemplate.setUriTemplateHandler(new DefaultUriBuilderFactory(patientHistoryUrl));
    return restTemplate;
  }

}
