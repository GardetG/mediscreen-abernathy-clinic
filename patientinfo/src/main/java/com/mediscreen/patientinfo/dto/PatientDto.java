package com.mediscreen.patientinfo.dto;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.mediscreen.patientinfo.domain.Gender;
import java.time.LocalDate;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * Dto Class for patient with personal information.
 */
public class PatientDto {

  @JsonProperty(access = JsonProperty.Access.READ_ONLY)
  private final int patientId;
  @NotBlank @Size(max = 20)
  @JsonAlias("given")
  private final String firstname;
  @NotBlank @Size(max = 20)
  @JsonAlias("family")
  private final String lastname;
  @Past @NotNull
  @JsonAlias("dob")
  private final LocalDate dateOfBirth;
  @NotNull
  @JsonAlias("sex")
  private final Gender gender;
  @Size(max = 50)
  private final String address;
  @Pattern(regexp = "^$|^\\d{3}-\\d{3}-\\d{4}$", message = "must match 000-000-0000 format")
  @JsonAlias("phone")
  private final String phoneNumber;
  @JsonProperty(access = JsonProperty.Access.READ_ONLY)
  private final LocalDate lastUpdate;

  /**
   * Constructor method to create a new Patient Dto.
   *
   * @param patientId   id of the patient
   * @param firstname   fistname of the patient
   * @param lastname    lastname of the patient
   * @param dateOfBirth date of birth of the patient
   * @param gender      gender of the patient
   * @param address     address of the patient
   * @param phoneNumber phone number of the patient
   * @param lastUpdate  last update date of the patient
   */
  @JsonCreator
  public PatientDto(int patientId, String firstname, String lastname, LocalDate dateOfBirth,
                    Gender gender, String address, String phoneNumber, LocalDate lastUpdate) {
    this.patientId = patientId;
    this.firstname = firstname;
    this.lastname = lastname;
    this.dateOfBirth = dateOfBirth;
    this.gender = gender;
    this.address = address;
    this.phoneNumber = phoneNumber;
    this.lastUpdate = lastUpdate;
  }

  public int getPatientId() {
    return patientId;
  }

  public String getFirstname() {
    return firstname;
  }

  public String getLastname() {
    return lastname;
  }

  public LocalDate getDateOfBirth() {
    return dateOfBirth;
  }

  public Gender getGender() {
    return gender;
  }

  public String getAddress() {
    return address;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public LocalDate getLastUpdate() {
    return lastUpdate;
  }

}
