package com.mediscreen.patienthistory.integration;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.jayway.jsonpath.JsonPath;
import com.mediscreen.patienthistory.config.TestConfig;
import com.mediscreen.patienthistory.repository.RecordRepository;
import org.json.JSONObject;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

@SpringBootTest
@AutoConfigureMockMvc
@Import(TestConfig.class)
class PatientHistoryIntegrationTest {

  @Autowired
  private MockMvc mockMvc;

  @Autowired
  private RecordRepository recordRepository;

  @DisplayName("As a doctor I want to see the medical history of my patient to remind me health issues he had in the past")
  @Test
  void viewPatientPersonalInformationTest() throws Exception {
    // GIVEN a patient
    int patientId = 4;

    // WHEN the doctor request page of the patient's medical history
    MvcResult result = mockMvc.perform(get(String.format("/patHistory/patient/%d", patientId)))

        // THEN He receives a page with patient's medical records
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.content", hasSize(4)))
        .andReturn();

    // GIVEN a medical record choose by the doctor
    String json = result.getResponse().getContentAsString();
    String recordId = JsonPath.read(json, "$.content[0].recordId");

    // WHEN the doctor request it
    mockMvc.perform(get(String.format("/patHistory/%s", recordId)))

        // THEN he receives the corresponding medical record
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.recordId", is(recordId)))
        .andExpect(jsonPath("$.notes",
            containsString("Patient: TestEarlyOnset Practitioner's notes/recommendations:")));
  }

  @DisplayName("As a doctor I want to update the medical history of my patient to keep track of treatments compatibility")
  @Test
  void updateMedicalRecordTest() throws Exception {
    // Given a patient's medical record
    String recordId = "record1.1";
    JSONObject jsonObject = new JSONObject()
        .put("patientId", 1)
        .put("notes", "Test notes update");

    // WHEN a doctor request update of the medical record
    mockMvc.perform(put(String.format("/patHistory/%s", recordId))
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonObject.toString()))

        // THEN the medical record is updated
        .andExpect(status().isOk());
    mockMvc.perform(get(String.format("/patHistory/%s", recordId)))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.recordId", is(recordId)))
        .andExpect(jsonPath("$.notes", is("Test notes update")))
        .andExpect(jsonPath("$.updatedDate").isNotEmpty());
  }

  @DisplayName("As a doctor I want to add a note to my patient history to check is my advices are followed")
  @Test
  void addMedicalRecordTest() throws Exception {
    // Given a medical record to add
    JSONObject jsonObject = new JSONObject()
        .put("patientId", 5)
        .put("notes", "Test notes new");

    // WHEN a doctor request to add the medical record
    MvcResult result = mockMvc.perform(post("/patHistory/add")
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonObject.toString()))

        // THEN the medical record is added
        .andExpect(status().isCreated())
        .andReturn();
    String json = result.getResponse().getContentAsString();
    String recordId = JsonPath.read(json, "$.recordId");
    mockMvc.perform(get(String.format("/patHistory/%s", recordId)))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.recordId", is(recordId)))
        .andExpect(jsonPath("$.patientId", is(5)))
        .andExpect(jsonPath("$.notes", is("Test notes new")))
        .andExpect(jsonPath("$.createdDate").isNotEmpty())
        .andExpect(jsonPath("$.updatedDate").isNotEmpty());
  }

  @DisplayName("As a doctor I want to remove a medical record of my patient to avoid any duplicate")
  @Test
  void removeMedicalRecordTest() throws Exception {
    // Given a medical record
    String recordId = "record2.1";

    // WHEN A doctor request to remove the medical record
    mockMvc.perform(delete(String.format("/patHistory/%s", recordId)))

        // THEN the medical record is removed
        .andExpect(status().isNoContent());
    mockMvc.perform(get(String.format("/patHistory/%s", recordId)))
        .andExpect(status().isNotFound());
  }

  @DisplayName("As a doctor I want to clear my patient's medical history to respect their privacy")
  @Test
  void clearPatientHistoryTest() throws Exception {
    // Given a patient
    int patientId = 3;

    // WHEN A doctor request to clear the patient's medical history
    mockMvc.perform(delete(String.format("/patHistory/patient/%d", patientId)))

        // THEN all medical records of the patient's medical history are removed
        .andExpect(status().isNoContent());
    mockMvc.perform(get(String.format("/patHistory/patient/%d", patientId)))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.content", hasSize(0)));
  }

  @DisplayName("As a service I want to fetch patient's medical history to analyze it")
  @Test
  void fetchPatientMedicalHistoryTest() throws Exception {
    // GIVEN a patient
    int patientId = 4;

    // WHEN A service request plain patient's medical history
    mockMvc.perform(get(String.format("/patHistory/patient/%d/plain", patientId)))

        // THEN It receives a list with patient's medical records
        .andExpect(status().isOk())
        .andExpect(jsonPath("$", hasSize(4)))
        .andReturn();
  }

}
