package com.mediscreen.patienthistory.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.mediscreen.patienthistory.dto.RecordDto;
import com.mediscreen.patienthistory.excepion.RecordNotFoundException;
import com.mediscreen.patienthistory.service.HistoryService;
import java.time.LocalDateTime;
import java.util.List;
import org.json.JSONObject;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest
class HistoryControllerTest {

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private HistoryService historyService;

  @MockBean
  private MappingMongoConverter mappingMongoConverter;

  @Captor
  private ArgumentCaptor<RecordDto> recordCaptor;

  @DisplayName("GET patient's medical history should return 200 with patient's medical records list")
  @Test
  void getPatientHistoryTest() throws Exception {
    // GIVEN
    LocalDateTime date = LocalDateTime.parse("2000-01-01T00:00");
    List<RecordDto> recordsDto = List.of(
        new RecordDto("record1", 1, "Test notes1", date, date),
        new RecordDto("record2", 1, "Test notes2", date, date)
    );
    when(historyService.getPatientHistory(anyInt())).thenReturn(recordsDto);

    // WHEN
    mockMvc.perform(get("/patHistory/patient/1/plain"))

        // THEN
        .andExpect(status().isOk())
        .andExpect(jsonPath("$", hasSize(2)))
        .andExpect(jsonPath("$[0].recordId", is("record1")))
        .andExpect(jsonPath("$[1].recordId", is("record2")));
    verify(historyService, times(1)).getPatientHistory(1);
  }

  @DisplayName("GET patient's medical history should return 200 with patient's medical records page")
  @Test
  void getPatientHistoryPageTest() throws Exception {
    // GIVEN
    LocalDateTime date = LocalDateTime.parse("2000-01-01T00:00");
    Page<RecordDto> recordDtoPage = new PageImpl<>(List.of(
        new RecordDto("record1", 1, "Test notes1", date, date),
        new RecordDto("record2", 1, "Test notes2", date, date)
    ));
    when(historyService.getPatientHistory(anyInt(), any(Pageable.class))).thenReturn(recordDtoPage);

    // WHEN
    mockMvc.perform(get("/patHistory/patient/1?page=0&size=5"))

        // THEN
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.content", hasSize(2)))
        .andExpect(jsonPath("$.content[0].recordId", is("record1")))
        .andExpect(jsonPath("$.content[1].recordId", is("record2")));
    verify(historyService, times(1)).getPatientHistory(1, PageRequest.of(0, 5));
  }

  @DisplayName("GET medical record by id should return 200 with record")
  @Test
  void getMedicalRecordTest() throws Exception {
    // GIVEN
    LocalDateTime creationDate = LocalDateTime.parse("2000-01-01T00:00");
    LocalDateTime updateDate = LocalDateTime.parse("2022-01-01T00:00");
    RecordDto record = new RecordDto("record1", 1, "Test notes 1", creationDate, updateDate);
    when(historyService.getMedicalRecord(anyString())).thenReturn(record);
    // WHEN
    mockMvc.perform(get("/patHistory/record1"))

        // THEN
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.recordId", is("record1")))
        .andExpect(jsonPath("$.patientId", is(1)))
        .andExpect(jsonPath("$.notes", is("Test notes 1")))
        .andExpect(jsonPath("$.createdDate", is("01-01-2000 00:00:00")))
        .andExpect(jsonPath("$.updatedDate", is("01-01-2022 00:00:00")));
    verify(historyService, times(1)).getMedicalRecord("record1");
  }

  @DisplayName("GET medical record by id when not found should return 404")
  @Test
  void getMedicalRecordWhenNotFoundTest() throws Exception {
    // GIVEN
    when(historyService.getMedicalRecord(anyString())).
        thenThrow(new RecordNotFoundException("Record not found", "nonExisting"));

    // WHEN
    mockMvc.perform(get("/patHistory/nonExisting"))

        // THEN
        .andExpect(status().isNotFound())
        .andExpect(jsonPath("$", is("Record not found")));
    verify(historyService, times(1)).getMedicalRecord("nonExisting");
  }

  @DisplayName("PUT update on medical record should return 200")
  @Test
  void updateMedicalRecordTest() throws Exception {
    // GIVEN
    JSONObject jsonObject = new JSONObject()
        .put("patientId", 1)
        .put("notes", "Test notes update");
    LocalDateTime creationDate = LocalDateTime.parse("2000-01-01T00:00");
    LocalDateTime updateDate = LocalDateTime.parse("2022-01-01T00:00");
    RecordDto recordDto = new RecordDto("record1", 1, "Test notes update", creationDate, updateDate);
        when(historyService.updateMedicalRecord(anyString(), any(RecordDto.class)))
        .thenReturn(recordDto);

    // WHEN
    mockMvc.perform(put("/patHistory/record1")
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonObject.toString()))

        // THEN
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.recordId", is("record1")))
        .andExpect(jsonPath("$.patientId", is(1)))
        .andExpect(jsonPath("$.notes", is("Test notes update")));
    verify(historyService, times(1)).updateMedicalRecord(eq("record1"), recordCaptor.capture());
    assertThat(recordCaptor.getValue()).usingRecursiveComparison()
        .isEqualTo(new RecordDto(null, 1, "Test notes update", null, null));
  }

  @DisplayName("PUT update on medical record with invalid dto should return 422")
  @Test
  void updateMedicalRecordWhenInvalidTest() throws Exception {
    // GIVEN
    JSONObject jsonObject = new JSONObject()
        .put("patientId", 0)
        .put("notes", "");

    // WHEN
    mockMvc.perform(put("/patHistory/record1")
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonObject.toString()))

        // THEN
        .andExpect(status().isUnprocessableEntity())
        .andExpect(jsonPath("$.patientId", is("must be greater than or equal to 1")))
        .andExpect(jsonPath("$.notes", is("must not be blank")));
    verify(historyService, times(0))
        .updateMedicalRecord(anyString(), any(RecordDto.class));
  }

  @DisplayName("PUT update on medical record when not found should return 404")
  @Test
  void updateMedicalRecordWhenNotFoundTest() throws Exception {
    // GIVEN
    JSONObject jsonObject = new JSONObject()
        .put("patientId", 1)
        .put("notes", "Test notes update");
    when(historyService.updateMedicalRecord(anyString(), any(RecordDto.class)))
        .thenThrow(new RecordNotFoundException("Record not found", "nonExisting"));

    // WHEN
    mockMvc.perform(put("/patHistory/nonExisting")
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonObject.toString()))

        // THEN
        .andExpect(status().isNotFound())
        .andExpect(jsonPath("$", is("Record not found")));
    verify(historyService, times(1))
        .updateMedicalRecord(anyString(), any(RecordDto.class));
  }

  @DisplayName("POST new on medical record should return 201")
  @Test
  void addMedicalRecordTest() throws Exception {
    // GIVEN
    JSONObject jsonObject = new JSONObject()
        .put("patientId", 1)
        .put("notes", "Test notes new");
    LocalDateTime creationDate = LocalDateTime.parse("2000-01-01T00:00");
    RecordDto recordDto = new RecordDto("record1", 1, "Test notes new", creationDate, creationDate);
    when(historyService.addMedicalRecord(any(RecordDto.class)))
        .thenReturn(recordDto);

    // WHEN
    mockMvc.perform(post("/patHistory/add")
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonObject.toString()))

        // THEN
        .andExpect(status().isCreated())
        .andExpect(jsonPath("$.recordId", is("record1")))
        .andExpect(jsonPath("$.notes", is("Test notes new")));
    verify(historyService, times(1)).addMedicalRecord(recordCaptor.capture());
    assertThat(recordCaptor.getValue()).usingRecursiveComparison()
        .isEqualTo(new RecordDto(null, 1, "Test notes new", null, null));
  }

  @DisplayName("POST new medical record with invalid dto should return 422")
  @Test
  void addMedicalRecordWhenInvalidTest() throws Exception {
    // GIVEN
    JSONObject jsonObject = new JSONObject()
        .put("patientId", 0)
        .put("notes", "");

    // WHEN
    mockMvc.perform(post("/patHistory/add")
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonObject.toString()))

        // THEN
        .andExpect(status().isUnprocessableEntity())
        .andExpect(jsonPath("$.patientId", is("must be greater than or equal to 1")))
        .andExpect(jsonPath("$.notes", is("must not be blank")));
    verify(historyService, times(0)).addMedicalRecord(any(RecordDto.class));
  }

  @DisplayName("DELETE medical record by id should return 204")
  @Test
  void removeMedicalRecordTest() throws Exception {
    // GIVEN
    doNothing().when(historyService).removeMedicalRecord(anyString());

    // WHEN
    mockMvc.perform(delete("/patHistory/record1"))

        // THEN
        .andExpect(status().isNoContent());
    verify(historyService, times(1)).removeMedicalRecord("record1");
  }

  @DisplayName("DELETE medical record by id when not found should return 404")
  @Test
  void removeMedicalRecordWhenNotFoundTest() throws Exception {
    // GIVEN
    doThrow(new RecordNotFoundException("Record not found", "nonExisting")).when(historyService)
        .removeMedicalRecord(anyString());

    // WHEN
    mockMvc.perform(delete("/patHistory/nonExisting"))

        // THEN
        .andExpect(status().isNotFound())
        .andExpect(jsonPath("$", is("Record not found")));
    verify(historyService, times(1)).removeMedicalRecord("nonExisting");
  }

  @DisplayName("DELETE medical history should return 204")
  @Test
  void clearPatientHistoryTest() throws Exception {
    // GIVEN
    doNothing().when(historyService).clearPatientHistory(anyInt());

    // WHEN
    mockMvc.perform(delete("/patHistory/patient/1"))

        // THEN
        .andExpect(status().isNoContent());
    verify(historyService, times(1)).clearPatientHistory(1);
  }

}