package com.mediscreen.patienthistory.config;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.data.repository.init.Jackson2RepositoryPopulatorFactoryBean;

/**
 * Configuration class for testing.
 */
@TestConfiguration
public class TestConfig {

  /**
   * Declare a repository populator factory Bean to load initial data in mongoDb embedded from a
   * data.json file for testing.
   *
   * @return RepositoryPopulatorFactory
   */
  @Bean
  public Jackson2RepositoryPopulatorFactoryBean repositoryPopulator() {
    Resource sourceData = new ClassPathResource("data.json");
    Jackson2RepositoryPopulatorFactoryBean factory = new Jackson2RepositoryPopulatorFactoryBean();
    factory.setResources(new Resource[] {sourceData});
    return factory;
  }

}