package com.mediscreen.patienthistory;

import static org.assertj.core.api.Assertions.assertThat;

import com.mediscreen.patienthistory.controller.HistoryController;
import com.mediscreen.patienthistory.repository.RecordRepository;
import com.mediscreen.patienthistory.service.HistoryService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class PatientHistoryApplicationTest {

  @Autowired
  private RecordRepository recordRepository;
  @Autowired
  private HistoryService historyService;
  @Autowired
  private HistoryController historyController;

  @Test
  void contextLoads() {
    assertThat(recordRepository).isNotNull();
    assertThat(historyService).isNotNull();
    assertThat(historyController).isNotNull();
  }

}