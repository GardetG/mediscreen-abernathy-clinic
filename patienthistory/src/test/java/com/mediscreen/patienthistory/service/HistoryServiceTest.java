package com.mediscreen.patienthistory.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.mediscreen.patienthistory.domain.Record;
import com.mediscreen.patienthistory.dto.RecordDto;
import com.mediscreen.patienthistory.excepion.RecordNotFoundException;
import com.mediscreen.patienthistory.repository.RecordRepository;
import com.mediscreen.patienthistory.utils.TestUtils;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

@SpringBootTest
class HistoryServiceTest {

  @Autowired
  private HistoryService historyService;

  @MockBean
  private RecordRepository recordRepository;

  @Captor
  private ArgumentCaptor<Record> recordCaptor;

  @DisplayName("Get patient's medical history should return list of patient's medical records")
  @Test
  void getPatientHistoryTest() {
    // Given
    int patientId = 1;
    LocalDateTime date = LocalDateTime.parse("2000-01-01T00:00");
    List<Record> records = List.of(
        TestUtils.createRecord("record1", patientId, "Test notes1", date, date),
        TestUtils.createRecord("record2", patientId, "Test notes2", date, date)
    );
    when(recordRepository.findAllByPatientId(anyInt())).thenReturn(records);

    // When
    List<RecordDto> actualRecords = historyService.getPatientHistory(patientId);

    // Then
    assertThat(actualRecords).usingRecursiveComparison()
        .isEqualTo(List.of(
            new RecordDto("record1", patientId, "Test notes1", date, date),
            new RecordDto("record2", patientId, "Test notes2", date, date)
        ));
    verify(recordRepository, times(1)).findAllByPatientId(patientId);
  }

  @DisplayName("Get patient's medical history when no medical records found should return an empty list")
  @Test
  void getPatientHistoryWhenEmptyTest() {
    // Given
    int patientId = 9;
    when(recordRepository.findAllByPatientId(anyInt())).thenReturn(Collections.emptyList());

    // When
    List<RecordDto> actualRecords = historyService.getPatientHistory(patientId);

    // Then
    assertThat(actualRecords).isEmpty();
    verify(recordRepository, times(1)).findAllByPatientId(patientId);
  }

  @DisplayName("Get patient's medical history with pageable should return page of patient's medical records")
  @Test
  void getPatientHistoryPageTest() {
    // Given
    int patientId = 1;
    LocalDateTime date = LocalDateTime.parse("2000-01-01T00:00");
    Pageable pageable = PageRequest.of(0, 5);
    Page<Record> recordPage = new PageImpl<>(List.of(
        TestUtils.createRecord("record1", patientId, "Test notes1", date, date),
        TestUtils.createRecord("record2", patientId, "Test notes2", date, date)
    ), pageable, 2);
    when(recordRepository.findAllByPatientId(anyInt(), any(Pageable.class))).thenReturn(recordPage);

    // When
    Page<RecordDto> actualRecordsPage = historyService.getPatientHistory(patientId, pageable);

    // Then
    assertThat(actualRecordsPage).usingRecursiveComparison()
        .isEqualTo(new PageImpl<>(List.of(
            new RecordDto("record1", patientId, "Test notes1", date, date),
            new RecordDto("record2", patientId, "Test notes2", date, date)
        ), pageable, 2));
    verify(recordRepository, times(1)).findAllByPatientId(patientId, pageable);
  }

  @DisplayName("Get patient's medical history with pageable when no medical records found should return an empty page")
  @Test
  void getPatientHistoryPageWhenEmptyTest() {
    // Given
    int patientId = 9;
    Pageable pageable = PageRequest.of(0, 5);
    when(recordRepository.findAllByPatientId(anyInt(), any(Pageable.class)))
        .thenReturn(Page.empty());

    // When
    Page<RecordDto> actualRecordsPage = historyService.getPatientHistory(patientId, pageable);

    // Then
    assertThat(actualRecordsPage).isEmpty();
    verify(recordRepository, times(1)).findAllByPatientId(patientId, pageable);
  }

  @DisplayName("Get medical record by id should return a record Dto")
  @Test
  void getMedicalRecordTest() throws RecordNotFoundException {
    // Given
    String recordId = "record";
    LocalDateTime date = LocalDateTime.parse("2000-01-01T00:00");
    Record medicalRecord = TestUtils.createRecord(recordId, 1, "Test notes", date, date);
    when(recordRepository.findById(anyString())).thenReturn(Optional.of(medicalRecord));

    // When
    RecordDto actualRecordDto = historyService.getMedicalRecord(recordId);

    // Then
    assertThat(actualRecordDto).usingRecursiveComparison()
        .isEqualTo(new RecordDto(recordId, 1, "Test notes", date, date));
    verify(recordRepository, times(1)).findById(recordId);
  }

  @DisplayName("Get medical record by id when not found should throws an exception")
  @Test
  void getMedicalRecordWhenNotFoundTest() {
    // Given
    String recordId = "nonExisting";
    when(recordRepository.findById(anyString())).thenReturn(Optional.empty());

    // When
    assertThatThrownBy(() -> historyService.getMedicalRecord(recordId))

        // Then
        .isInstanceOf(RecordNotFoundException.class)
        .hasMessageContaining("Record not found");
    verify(recordRepository, times(1)).findById(recordId);
  }

  @DisplayName("Update a medical record should update it in the repository and return Dto")
  @Test
  void UpdateMedicalRecordTest() throws RecordNotFoundException {
    // Given
    String recordId = "record";
    RecordDto recordUpdate = new RecordDto(null, 1, "Test notes update", null, null);
    LocalDateTime creationDate = LocalDateTime.parse("2000-01-01T00:00");
    Record medicalRecord = TestUtils.createRecord(recordId, 1, "Test notes", creationDate, creationDate);
    when(recordRepository.findById(anyString())).thenReturn(Optional.of(medicalRecord));
    LocalDateTime updateDate = LocalDateTime.parse("2022-01-01T00:00");
    doAnswer(invocation -> TestUtils.mockUpdate(invocation.getArgument(0), updateDate))
        .when(recordRepository).save(any(Record.class));

    // When
    RecordDto actualRecord = historyService.updateMedicalRecord(recordId, recordUpdate);

    // Then;
    assertThat(actualRecord).usingRecursiveComparison()
        .isEqualTo(new RecordDto(recordId, 1, "Test notes update", creationDate, updateDate));
    verify(recordRepository, times(1)).findById(recordId);
    verify(recordRepository, times(1)).save(recordCaptor.capture());
    assertThat(recordCaptor.getValue()).isEqualTo(medicalRecord);
    assertThat(medicalRecord.getNotes()).isEqualTo("Test notes update");
  }

  @DisplayName("Update a medical record when not found should throws an exception")
  @Test
  void updateMedicalRecordWhenNotFoundTest() {
    // Given
    String recordId = "nonExisting";
    RecordDto recordUpdate = new RecordDto(recordId, 1, "Test updated note", null, null);
    when(recordRepository.findById(anyString())).thenReturn(Optional.empty());

    // When
    assertThatThrownBy(() -> historyService.updateMedicalRecord(recordId, recordUpdate))

        // Then
        .isInstanceOf(RecordNotFoundException.class)
        .hasMessageContaining("Record not found");
    verify(recordRepository, times(1)).findById(recordId);
  }

  @DisplayName("Add a medical record should add it in the repository and return Dto")
  @Test
  void AddMedicalRecordTest() {
    // Given
    RecordDto recordCreate = new RecordDto(null, 1, "Test notes new", null, null);
    LocalDateTime creationDate = LocalDateTime.parse("2022-01-01T00:00");
    doAnswer(invocation -> TestUtils.mockCreate(invocation.getArgument(0), creationDate))
        .when(recordRepository).save(any(Record.class));

    // When
    RecordDto actualRecord = historyService.addMedicalRecord(recordCreate);

    // Then;
    assertThat(actualRecord).usingRecursiveComparison()
        .isEqualTo(new RecordDto("newRecord", 1, "Test notes new", creationDate, creationDate));
    verify(recordRepository, times(1)).save(recordCaptor.capture());
    assertThat(recordCaptor.getValue()).usingRecursiveComparison()
        .isEqualTo(TestUtils.createRecord("newRecord", 1, "Test notes new", creationDate, creationDate));
  }

  @DisplayName("Remove medical record should remove it from repository")
  @Test
  void removeMedicalRecordTest() throws RecordNotFoundException {
    // Given
    String recordId = "record";
    LocalDateTime date = LocalDateTime.parse("2000-01-01T00:00");
    Record medicalRecord = TestUtils.createRecord(recordId, 1, "Test notes", date, date);
    when(recordRepository.findById(anyString())).thenReturn(Optional.of(medicalRecord));

    // When
    historyService.removeMedicalRecord(recordId);

    // Then
    verify(recordRepository, times(1)).findById(recordId);
    verify(recordRepository, times(1)).delete(recordCaptor.capture());
    assertThat(recordCaptor.getValue()).isEqualTo(medicalRecord);
  }

  @DisplayName("Remove medical record when not found should throws an exception")
  @Test
  void removeMedicalRecordWhenNotFoundTest() {
    // Given
    String recordId = "nonExisting";
    when(recordRepository.findById(anyString())).thenReturn(Optional.empty());

    // When
    assertThatThrownBy(() -> historyService.removeMedicalRecord(recordId))

        // Then
        .isInstanceOf(RecordNotFoundException.class)
        .hasMessageContaining("Record not found");
    verify(recordRepository, times(1)).findById(recordId);
    verify(recordRepository, times(0)).delete(any(Record.class));
  }

  @DisplayName("Clear patient's medical history should remove all patient's medical record")
  @Test
  void clearPatientHistoryTest() {
    // Given
    int patientId = 1;

    // When
    historyService.clearPatientHistory(patientId);

    // Then
    verify(recordRepository, times(1)).deleteAllByPatientId(patientId);
  }

}