package com.mediscreen.patienthistory.utils;

import com.mediscreen.patienthistory.domain.Record;
import java.time.LocalDateTime;
import org.springframework.test.util.ReflectionTestUtils;

/**
 * Utility class used for testing
 */
public class TestUtils {

  /**
   * Create an instance of Record for testing purposes and manually set the fields usually handled
   * by the framework and database.
   *
   * @param recordId id of the record
   * @param patientId id of the patient
   * @param notes medical notes
   * @param createdDate date of creation
   * @param updatedDate date of last update
   * @return record
   */
  public static Record createRecord(String recordId, int patientId, String notes,
                                  LocalDateTime createdDate, LocalDateTime updatedDate) {
    Record medicalRecord = new Record(patientId, notes);
    ReflectionTestUtils.setField(medicalRecord, "recordId", recordId);
    ReflectionTestUtils.setField(medicalRecord, "createdDate", createdDate);
    ReflectionTestUtils.setField(medicalRecord, "updatedDate", updatedDate);
    return medicalRecord;
  }

  /**
   * Mock the update of a record in repository by manually setting the last update date.
   *
   * @param medicalRecord record on which apply update
   * @param updatedDate date of the mocked update
   * @return Record
   */
  public static Record mockUpdate(Record medicalRecord, LocalDateTime updatedDate) {
    ReflectionTestUtils.setField(medicalRecord, "updatedDate", updatedDate);
    return medicalRecord;
  }

  /**
   * Mock the creation of a record in repository by manually setting id and creation date.
   *
   * @param medicalRecord record on which apply update
   * @param createdDate date of the mocked creation
   * @return Record
   */
  public static Record mockCreate(Record medicalRecord, LocalDateTime createdDate) {
    ReflectionTestUtils.setField(medicalRecord, "recordId", "newRecord");
    ReflectionTestUtils.setField(medicalRecord, "createdDate", createdDate);
    ReflectionTestUtils.setField(medicalRecord, "updatedDate", createdDate);
    return medicalRecord;
  }

}
