package com.mediscreen.patienthistory.repository;

import static org.assertj.core.api.Assertions.assertThat;

import com.mediscreen.patienthistory.domain.Record;
import com.mediscreen.patienthistory.config.TestConfig;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.config.EnableMongoAuditing;

@DataMongoTest
@EnableMongoAuditing
@Import(TestConfig.class)
class RecordRepositoryTest {

  @Autowired
  private RecordRepository recordRepository;

  @DisplayName("Find all medical record should return a page of the patient's records")
  @Test
  void findAllByPatientIdTest() {
    // Given
    int patientId = 4;

    // When
    List<Record> actualRecords = recordRepository.findAllByPatientId(patientId);

    // Then
    assertThat(actualRecords)
        .hasSize(4)
        .extracting("patientId").containsOnly(patientId);
  }

  @DisplayName("Find all medical record with pageable should return a page of the patient's records")
  @Test
  void findAllByPatientIdPageableTest() {
    // Given
    int patientId = 4;
    Pageable pageable = Pageable.unpaged();

    // When
    Page<Record> actualRecords = recordRepository.findAllByPatientId(patientId, pageable);

    // Then
    assertThat(actualRecords.getContent())
        .hasSize(4)
        .extracting("patientId").containsOnly(patientId);
  }

  @DisplayName("Find by id should return an optional of the record")
  @Test
  void findByIdTest() {
    // Given
    String recordId = recordRepository.findAllByPatientId(1).get(0).getRecordId();

    // When
    Optional<Record> actualRecord = recordRepository.findById(recordId);

    // Then
    assertThat(actualRecord)
        .isPresent();
  }

  @DisplayName("Find by id when record not found should return an empty optional")
  @Test
  void findByIdWhenNotFoundTest() {
    // Given
    String recordId = "999";

    // When
    Optional<Record> actualRecord = recordRepository.findById(recordId);

    // Then
    assertThat(actualRecord).isEmpty();
  }

  @DisplayName("Save a new medical record should save it and return the saved record")
  @Test
  void saveTest() {
    // Given
    Record medicalRecord = new Record(1, "Test notes");

    // When
    Record actualRecord = recordRepository.save(medicalRecord);

    // Then
    assertThat(actualRecord).usingRecursiveComparison().isEqualTo(medicalRecord);
    assertThat(actualRecord.getRecordId()).isNotBlank();
    assertThat(actualRecord.getCreatedDate()).isNotNull();
    assertThat(actualRecord.getUpdatedDate()).isNotNull();
  }

  @DisplayName("Save an existing medical record should update it")
  @Test
  void saveUpdateTest() {
    // Given
    Record medicalRecord = recordRepository.findAllByPatientId(1).get(0);
    medicalRecord.setNotes("Update notes");
    LocalDateTime lastUpdate = medicalRecord.getUpdatedDate();

    // When
    Record actualRecord = recordRepository.save(medicalRecord);

    // Then
    assertThat(actualRecord).usingRecursiveComparison().isEqualTo(medicalRecord);
    assertThat(actualRecord.getNotes()).isEqualTo("Update notes");
    assertThat(actualRecord.getUpdatedDate()).isAfter(lastUpdate);
  }

  @DisplayName("delete a medical record should remove it")
  @Test
  void deleteTest() {
    // Given
    Record medicalRecord = recordRepository.findAllByPatientId(2).get(0);

    // When
    recordRepository.delete(medicalRecord);

    // Then
    assertThat(recordRepository.findById(medicalRecord.getRecordId())).isEmpty();
  }

  @DisplayName("delete a medical record should remove it")
  @Test
  void deleteAllByPatientIdTest() {
    // Given
    int patientId = 3;
    Pageable pageable = Pageable.unpaged();

    // When
    recordRepository.deleteAllByPatientId(patientId);

    // Then
    assertThat(recordRepository.findAllByPatientId(patientId, pageable)).isEmpty();
  }

}