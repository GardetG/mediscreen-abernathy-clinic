package com.mediscreen.patienthistory.utils;

import com.mediscreen.patienthistory.domain.Record;
import com.mediscreen.patienthistory.dto.RecordDto;

/**
 * Mapper Class to map {@link Record} document into Dto.
 */
public class RecordMapper {

  private RecordMapper() {
    throw new IllegalStateException("Utility class");
  }

  /**
   * Map a Record document into RecordDto.
   *
   * @param medicalRecord {@link Record} to map
   * @return corresponding {@link RecordDto} mapped
   */
  public static RecordDto toDto(Record medicalRecord) {
    return new RecordDto(
        medicalRecord.getRecordId(),
        medicalRecord.getPatientId(),
        medicalRecord.getNotes(),
        medicalRecord.getCreatedDate(),
        medicalRecord.getUpdatedDate()
    );
  }

  /**
   * Map a Record Dto into Record document.
   *
   * @param medicalRecordDto {@link RecordDto} to map
   * @return corresponding {@link Record} mapped
   */
  public static Record toDocument(RecordDto medicalRecordDto) {
    return new Record(
        medicalRecordDto.getPatientId(),
        medicalRecordDto.getNotes()
    );
  }

}
