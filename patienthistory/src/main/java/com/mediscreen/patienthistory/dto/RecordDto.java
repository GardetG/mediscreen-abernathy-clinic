package com.mediscreen.patienthistory.dto;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDateTime;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

/**
 * Dto Class for medical record with observation notes and creation and update date.
 */
public class RecordDto {

  @JsonProperty(access = JsonProperty.Access.READ_ONLY)
  private final String recordId;
  @JsonAlias("patId")
  @Min(1)
  private final int patientId;
  @JsonAlias("e")
  @NotBlank
  private final String notes;
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
  @JsonProperty(access = JsonProperty.Access.READ_ONLY)
  private final LocalDateTime createdDate;
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
  @JsonProperty(access = JsonProperty.Access.READ_ONLY)
  private final LocalDateTime updatedDate;

  /**
   * Constructor method to create a new Record Dto.
   *
   * @param recordId    id of the record
   * @param patientId   id of the patient
   * @param notes       observation notes of the record
   * @param createdDate creation date of the record
   * @param updatedDate update date of the record
   */
  public RecordDto(String recordId, int patientId, String notes, LocalDateTime createdDate,
                   LocalDateTime updatedDate) {
    this.recordId = recordId;
    this.patientId = patientId;
    this.notes = notes;
    this.createdDate = createdDate;
    this.updatedDate = updatedDate;
  }

  public String getRecordId() {
    return recordId;
  }

  public int getPatientId() {
    return patientId;
  }

  public String getNotes() {
    return notes;
  }

  public LocalDateTime getCreatedDate() {
    return createdDate;
  }

  public LocalDateTime getUpdatedDate() {
    return updatedDate;
  }

}
