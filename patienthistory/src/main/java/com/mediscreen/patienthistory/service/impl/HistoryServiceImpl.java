package com.mediscreen.patienthistory.service.impl;

import com.mediscreen.patienthistory.domain.Record;
import com.mediscreen.patienthistory.dto.RecordDto;
import com.mediscreen.patienthistory.excepion.RecordNotFoundException;
import com.mediscreen.patienthistory.repository.RecordRepository;
import com.mediscreen.patienthistory.service.HistoryService;
import com.mediscreen.patienthistory.utils.RecordMapper;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service implementation class to manage {@link Record} of the patient's medical history.
 */
@Service
public class HistoryServiceImpl implements HistoryService {

  @Autowired
  private RecordRepository recordRepository;

  /**
   * {@inheritDoc}
   */
  @Override
  public List<RecordDto> getPatientHistory(int patientId) {
    return recordRepository.findAllByPatientId(patientId)
        .stream()
        .map(RecordMapper::toDto)
        .collect(Collectors.toList());
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Page<RecordDto> getPatientHistory(int patientId, Pageable pageable) {
    return recordRepository.findAllByPatientId(patientId, pageable)
        .map(RecordMapper::toDto);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public RecordDto getMedicalRecord(String recordId) throws RecordNotFoundException {
    Record medicalRecord = getRecordOrThrowsException(recordId);
    return RecordMapper.toDto(medicalRecord);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  @Transactional
  public RecordDto updateMedicalRecord(String recordId, RecordDto recordUpdate)
      throws RecordNotFoundException {
    Record medicalRecord = getRecordOrThrowsException(recordId);
    medicalRecord.setNotes(recordUpdate.getNotes());
    recordRepository.save(medicalRecord);
    return RecordMapper.toDto(medicalRecord);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  @Transactional
  public RecordDto addMedicalRecord(RecordDto recordCreate) {
    Record medicalRecord = RecordMapper.toDocument(recordCreate);
    recordRepository.save(medicalRecord);
    return RecordMapper.toDto(medicalRecord);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  @Transactional
  public void removeMedicalRecord(String recordId) throws RecordNotFoundException {
    Record medicalRecord = getRecordOrThrowsException(recordId);
    recordRepository.delete(medicalRecord);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  @Transactional
  public void clearPatientHistory(int patientId) {
    recordRepository.deleteAllByPatientId(patientId);
  }

  /**
   * Get the requested record by the provided id.
   * If no record with such id is found then an RecordNotFoundException is thrown.
   *
   * @param recordId id of the record
   * @return the requested record
   * @throws RecordNotFoundException when record not found
   */
  private Record getRecordOrThrowsException(String recordId) throws RecordNotFoundException {
    return recordRepository.findById(recordId)
        .orElseThrow(() -> new RecordNotFoundException("Record not found.", recordId));
  }

}
