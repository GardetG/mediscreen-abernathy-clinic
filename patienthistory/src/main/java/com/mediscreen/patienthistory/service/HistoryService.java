package com.mediscreen.patienthistory.service;

import com.mediscreen.patienthistory.domain.Record;
import com.mediscreen.patienthistory.dto.RecordDto;
import com.mediscreen.patienthistory.excepion.RecordNotFoundException;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service interface to manage {@link Record} of the patient's medical history.
 */
public interface HistoryService {

  /**
   * Get the patient's medical history by the provided id and return a list of the patient's
   * medical records.
   *
   * @param patientId id of the patient
   * @return a List of {@link RecordDto}
   */
  List<RecordDto> getPatientHistory(int patientId);

  /**
   * Get the patient's medical history by the provided patient id and return a page of the patient's
   * medical records according to the paging restriction provided in the {@link Pageable} object.
   *
   * @param patientId id of the patient
   * @param pageable  to request a paged result, can be {@link Pageable#unpaged()}, must not be null
   * @return a Page of {@link RecordDto}.
   */
  Page<RecordDto> getPatientHistory(int patientId, Pageable pageable);

  /**
   * Get the requested medical record by the provided record id.
   * If no record with such id could be then an {@link RecordNotFoundException} is thrown.
   *
   * @param recordId id of the {@link Record}
   * @return the requested {@link RecordDto}
   * @throws RecordNotFoundException when record not found
   */
  RecordDto getMedicalRecord(String recordId) throws RecordNotFoundException;

  /**
   * Update a medical record with the information provided by the Dto.
   * If no record with such id could be found then an {@link RecordNotFoundException} is thrown.
   * Medical record can't be transferred between patient, attempt to update record's patient id wiil
   * be ignored.
   *
   * @param recordId     id of the {@link Record}
   * @param recordUpdate {@link RecordDto} with the record's information to update
   * @return the updated {@link RecordDto}
   * @throws RecordNotFoundException when record not found
   */
  RecordDto updateMedicalRecord(String recordId, RecordDto recordUpdate)
      throws RecordNotFoundException;

  /**
   * Add a new medical record with the information provided by the Dto.
   *
   * @param recordCreate {@link RecordDto} with the record's information to add
   * @return the added {@link RecordDto}
   */
  RecordDto addMedicalRecord(RecordDto recordCreate);

  /**
   * Remove a medical record by the provided record id.
   * If no record with such id could be found then an {@link RecordNotFoundException} is thrown.
   *
   * @param recordId id of the {@link Record}
   * @throws RecordNotFoundException when record not found
   */
  void removeMedicalRecord(String recordId) throws RecordNotFoundException;

  /**
   * Clear the entire patient's medical history by the provided patient id.
   *
   * @param patientId id of the patient
   */
  void clearPatientHistory(int patientId);

}
