package com.mediscreen.patienthistory.excepion;

/**
 * Exception thrown when the requested record is not found.
 */
public class RecordNotFoundException extends Exception {

  private final String id;

  public RecordNotFoundException(String s, String id) {
    super(s);
    this.id = id;
  }

  public String getId() {
    return id;
  }

}
