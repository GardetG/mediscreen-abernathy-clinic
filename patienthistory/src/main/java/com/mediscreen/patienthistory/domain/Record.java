package com.mediscreen.patienthistory.domain;

import java.time.LocalDateTime;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * Document class of a patient's medical Record composing the medical history with observation
 * notes and date of creation and update.
 */
@Document(collection = "Record")
public class Record {

  @Id
  private String recordId;
  @Field("patient_id")
  private int patientId;
  @Field("notes")
  private String notes;
  @CreatedDate
  @Field("created_date")
  private LocalDateTime createdDate;
  @LastModifiedDate
  @Field("updated_date")
  private LocalDateTime updatedDate;

  /**
   * Constructor method to create a new medical Record document.
   *
   * @param patientId id of the patient
   * @param notes     observation notes.
   */
  public Record(int patientId, String notes) {
    this.patientId = patientId;
    this.notes = notes;
  }

  private Record() {
    // private constructor needed by MongoDb
  }

  public String getRecordId() {
    return recordId;
  }

  public int getPatientId() {
    return patientId;
  }

  public String getNotes() {
    return notes;
  }

  public void setNotes(String notes) {
    this.notes = notes;
  }

  public LocalDateTime getCreatedDate() {
    return createdDate;
  }

  public LocalDateTime getUpdatedDate() {
    return updatedDate;
  }

}
