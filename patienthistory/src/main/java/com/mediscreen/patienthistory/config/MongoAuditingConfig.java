package com.mediscreen.patienthistory.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.EnableMongoAuditing;

/**
 * Configuration class to enable Mongo Auditing.
 */
@Configuration
@EnableMongoAuditing
public class MongoAuditingConfig { }