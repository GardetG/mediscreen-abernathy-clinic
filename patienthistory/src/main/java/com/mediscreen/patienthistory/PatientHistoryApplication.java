package com.mediscreen.patienthistory;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Patient History main class.
 */
@SpringBootApplication
public class PatientHistoryApplication {

  public static void main(String[] args) {
    SpringApplication.run(PatientHistoryApplication.class, args);
  }

}
