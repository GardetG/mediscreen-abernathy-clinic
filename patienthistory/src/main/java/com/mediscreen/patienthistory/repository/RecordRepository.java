package com.mediscreen.patienthistory.repository;

import com.mediscreen.patienthistory.domain.Record;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Repository;

/**
 * Repository interface for {@link Record} documents.
 */
@Repository
public interface RecordRepository extends MongoRepository<Record, String> {

  /**
   * Find all medical records by the provided patient id and return a list of records.
   *
   * @param patientId id of the patient
   * @return a List of {@link Record Records}
   */
  List<Record> findAllByPatientId(int patientId);

  /**
   * Find all medical records by the provided patient id and return a page of records according to
   * the paging restriction provided in the {@link Pageable} object.
   *
   * @param patientId id of the patient
   * @param pageable  to request a paged result, can be {@link Pageable#unpaged()}, must not be null
   * @return a Page of {@link Record Records}
   */
  Page<Record> findAllByPatientId(int patientId, Pageable pageable);

  /**
   * Find a medical record by the provided record id and return an Optional of it.
   * If no record with such id could be found then an empty Optional is returned.
   *
   * @param recordId id of the record
   * @return an Optional of {@link Record}
   */
  @NonNull
  Optional<Record> findById(@NonNull String recordId);

  /**
   * Save a new medical record or update an existing one.
   *
   * @param medicalRecord {@link Record} to save or update
   * @param <T>           {@link Record} or derived entity
   * @return saved {@link Record}
   */
  @NonNull
  <T extends Record> T save(@NonNull T medicalRecord);

  /**
   * Delete a medical record.
   *
   * @param medicalRecord {@link Record} to delete
   */
  void delete(@NonNull Record medicalRecord);

  /**
   * Delete all medical records of the patient by the provided patient id.
   *
   * @param patientId id of the patient
   */
  void deleteAllByPatientId(int patientId);

}
