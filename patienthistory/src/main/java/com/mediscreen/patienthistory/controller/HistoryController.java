package com.mediscreen.patienthistory.controller;

import com.mediscreen.patienthistory.domain.Record;
import com.mediscreen.patienthistory.dto.RecordDto;
import com.mediscreen.patienthistory.excepion.RecordNotFoundException;
import com.mediscreen.patienthistory.service.HistoryService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import java.util.List;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller class exposing API end-points to manage patient's medical history.
 */
@RestController
@RequestMapping("/patHistory")
public class HistoryController {

  private static final Logger LOGGER = LoggerFactory.getLogger(HistoryController.class);

  @Autowired
  private HistoryService historyService;

  /**
   * GET the requested plain patient's medical history by the provided patient id.
   *
   * @param patientId id of the patient
   * @return HTTP 200 with the List of patient's medical {@link RecordDto}
   */
  @Operation(description = "View plain patient's medical history", tags = {"Medical History"})
  @GetMapping("/patient/{patientId}/plain")
  public ResponseEntity<List<RecordDto>> getPatientHistory(@PathVariable int patientId) {
    LOGGER.info("Request: Get patient {}'s plain medical history", patientId);
    List<RecordDto> patientHistory = historyService.getPatientHistory(patientId);

    LOGGER.info("Response: Patient {}'s plain medical history sent", patientId);
    return ResponseEntity.ok(patientHistory);
  }

  /**
   * GET the requested page of the patient's medical history by the provided patient id.
   *
   * @param patientId id of the patient
   * @param pageable  to request a paged result
   * @return HTTP 200 with the Page of patient's medical {@link RecordDto}
   */
  @Operation(description = "View paginated medical history", tags = {"Medical History"})
  @GetMapping("/patient/{patientId}")
  public ResponseEntity<Page<RecordDto>> getPatientHistory(@PathVariable int patientId,
                                                                           Pageable pageable) {
    LOGGER.info("Request: Get page {} of patient {}'s medical history ",
        pageable.getPageNumber(), patientId);
    Page<RecordDto> patientHistoryPage = historyService.getPatientHistory(patientId, pageable);

    LOGGER.info("Response: Page {} of patient {}'s medical history sent",
        pageable.getPageNumber(), patientId);
    return ResponseEntity.ok(patientHistoryPage);
  }

  /**
   * Get the requested medical record by the provided record id.
   *
   * @param recordId id of the {@link Record}
   * @return HTTP 200 with {@link RecordDto}
   * @throws RecordNotFoundException when record not found
   */
  @Operation(description = "View a medical record", tags = {"Medical History"})
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "OK"),
      @ApiResponse(responseCode = "404", description = "NOT FOUND - Record not found",
          content = @Content),
  })
  @GetMapping("/{recordId}")
  public ResponseEntity<RecordDto> getMedicalRecord(@PathVariable String recordId)
      throws RecordNotFoundException {
    LOGGER.info("Request: Get medical record {}", recordId);
    RecordDto medicalRecord = historyService.getMedicalRecord(recordId);

    LOGGER.info("Response: Medical record {} sent", recordId);
    return ResponseEntity.ok(medicalRecord);
  }

  /**
   * PUT an update on a medical record with the information provided by the request payload.
   *
   * @param recordId id of the {@link Record}
   * @param recordDto {@link RecordDto} with the record information to update
   * @return HTTP 200 with updated {@link RecordDto}
   * @throws RecordNotFoundException when record not found
   */
  @Operation(description = "Update a medical record", tags = {"Medical History"})
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "OK - Record updated"),
      @ApiResponse(responseCode = "404", description = "NOT FOUND - Record not found",
          content = @Content),
      @ApiResponse(responseCode = "422",
          description = "UNPROCESSABLE ENTITY - Some fields of the provided record are invalid",
          content = @Content)
  })
  @PutMapping("/{recordId}")
  public ResponseEntity<RecordDto> updateMedicalRecord(@PathVariable String recordId,
                                                       @Valid @RequestBody RecordDto recordDto)
      throws RecordNotFoundException {
    LOGGER.info("Request: Update medical record {}", recordId);
    RecordDto medicalRecord = historyService.updateMedicalRecord(recordId, recordDto);

    LOGGER.info("Response: Medical record {} updated", recordId);
    return ResponseEntity.ok(medicalRecord);
  }

  /**
   * POST a new medical record with the information provided by the request payload.
   *
   * @param recordDto {@link RecordDto} with the record information to add
   * @return HTTP 201 with added {@link RecordDto}
   */
  @Operation(description = "Add a new medical record", tags = {"Medical History"})
  @ApiResponses(value = {
      @ApiResponse(responseCode = "201", description = "CREATED - Record added"),
      @ApiResponse(responseCode = "422",
          description = "UNPROCESSABLE ENTITY - Some fields of the provided record are invalid",
          content = @Content)
  })
  @PostMapping("/add")
  public ResponseEntity<RecordDto> addMedicalRecord(@Valid @RequestBody RecordDto recordDto) {
    LOGGER.info("Request: Add new medical record");
    RecordDto medicalRecord = historyService.addMedicalRecord(recordDto);

    LOGGER.info("Response: Medical record {} added", recordDto.getRecordId());
    return ResponseEntity.status(HttpStatus.CREATED).body(medicalRecord);
  }

  /**
   * DELETE a medical record by the provided record id.
   *
   * @param recordId id of the {@link Record}
   * @return HTTP 204
   * @throws RecordNotFoundException when record not found
   */
  @Operation(description = "Remove a medical record", tags = {"Medical History"})
  @ApiResponses(value = {
      @ApiResponse(responseCode = "204", description = "NO CONTENT - Record removed"),
      @ApiResponse(responseCode = "404", description = "NOT FOUND - Record not found",
          content = @Content),
  })
  @DeleteMapping("/{recordId}")
  public ResponseEntity<Void> removeMedicalRecord(@PathVariable String recordId)
      throws RecordNotFoundException {
    LOGGER.info("Request: Remove medical record {}", recordId);
    historyService.removeMedicalRecord(recordId);

    LOGGER.info("Response: Medical record {} removed", recordId);
    return ResponseEntity.noContent().build();
  }

  /**
   * DELETE the entire patient's medical history by the provided patient id.
   *
   * @param patientId id of the patient
   * @return HTTP 204
   */
  @Operation(description = "Clear a patient's medical history", tags = {"Medical History"})
  @ApiResponses(value = {
      @ApiResponse(responseCode = "204", description = "NO CONTENT - Medical history cleared"),
  })
  @DeleteMapping("/patient/{patientId}")
  public ResponseEntity<Void> clearPatientHistory(@PathVariable int patientId) {
    LOGGER.info("Request: Clear patient {} medical history", patientId);
    historyService.clearPatientHistory(patientId);

    LOGGER.info("Response: Patient {} medical history cleared",  patientId);
    return ResponseEntity.noContent().build();
  }

}
