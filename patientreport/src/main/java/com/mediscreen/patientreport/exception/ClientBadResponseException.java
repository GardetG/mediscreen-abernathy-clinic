package com.mediscreen.patientreport.exception;

/**
 * Exception thrown when the requested external service returns an unexpected response to Client.
 */
public class ClientBadResponseException extends RuntimeException {

  private final String response;

  public ClientBadResponseException(String msg, String response) {
    super(String.format("%s Unexpected Response.", msg));
    this.response = response;
  }

  public ClientBadResponseException(String msg, String response, Throwable cause) {
    super(String.format("%s Unexpected Response.", msg), cause);
    this.response = response;
  }

  public String getResponse() {
    return response;
  }

}
