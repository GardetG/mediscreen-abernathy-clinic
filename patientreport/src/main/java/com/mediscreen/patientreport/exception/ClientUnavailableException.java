package com.mediscreen.patientreport.exception;

/**
 * Exception thrown when the requested external service fails to respond to Client.
 */
public class ClientUnavailableException extends RuntimeException {

  public ClientUnavailableException(String msg) {
    super(String.format("%s Service Unavailable.", msg));
  }

  public ClientUnavailableException(String msg, Throwable cause) {
    super(String.format("%s Service Unavailable.", msg), cause);
  }

}
