package com.mediscreen.patientreport.exception;

/**
 * Exception thrown when the requested patient is not found.
 */
public class PatientNotFoundException extends RuntimeException {

  public PatientNotFoundException(String s) {
    super(s);
  }

}
