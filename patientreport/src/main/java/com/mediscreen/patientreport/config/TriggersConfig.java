package com.mediscreen.patientreport.config;

import java.util.ArrayList;
import java.util.List;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * Configuration class for the diabetes triggers to search in the patient's medical history.
 */
@Component
@ConfigurationProperties
@PropertySource("classpath:triggers.properties")
public class TriggersConfig {

  List<String> diabetesTriggers = new ArrayList<>();

  public List<String> getDiabetesTriggers() {
    return diabetesTriggers;
  }

  public void setDiabetesTriggers(List<String> diabetesTriggers) {
    this.diabetesTriggers = diabetesTriggers;
  }

}