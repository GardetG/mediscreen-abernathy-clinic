package com.mediscreen.patientreport.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

/**
 * Configuration class for service client with url:
 * - patientInfoUrl: Url of the PatientInfo service.
 * - patientHistoryUrl: Url of the PatientHistory service.
 */
@Component
@ConfigurationProperties
public class ClientConfig {

  private String patientInfoUrl;
  private String patientHistoryUrl;

  public void setPatientInfoUrl(String patientInfoUrl) {
    this.patientInfoUrl = patientInfoUrl;
  }

  public void setPatientHistoryUrl(String patientHistoryUrl) {
    this.patientHistoryUrl = patientHistoryUrl;
  }

  /**
   * WebClient Bean for PatientInfo service.
   *
   * @return WebClient
   */
  @Bean("infoWebClient")
  public WebClient patientInfoWebClient() {
    return WebClient.builder()
        .baseUrl(patientInfoUrl)
        .build();
  }

  /**
   * WebClient Bean for PatientHistory service.
   *
   * @return WebClient
   */
  @Bean("historyWebClient")
  public WebClient patientHistoryWebClient() {
    return WebClient.builder()
        .baseUrl(patientHistoryUrl)
        .build();
  }

}