package com.mediscreen.patientreport.dto;

import com.mediscreen.patientreport.constant.Risk;

/**
 * Dto class of a patient's diabetes assessment report with id and results.
 */
public class ReportDto {

  private final int patientId;
  private final String report;
  private final Risk risk;

  /**
   * Constructor method to create a new ReportDto.
   *
   * @param patientId id of the patient
   * @param report    report message
   * @param risk      diabetes risk level
   */
  public ReportDto(int patientId, String report, Risk risk) {
    this.patientId = patientId;
    this.report = report;
    this.risk = risk;
  }

  public int getPatientId() {
    return patientId;
  }

  public String getReport() {
    return report;
  }

  public Risk getRisk() {
    return risk;
  }

}
