package com.mediscreen.patientreport.dto;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Dto class for holding parameter for report request by id.
 */
public class RequestByIdDto {

  @JsonAlias("patId")
  private final int patientId;

  /**
   * Constructor method to create new RequestByIdDto.
   *
   * @param patientId id of the patient
   */
  @JsonCreator
  public RequestByIdDto(@JsonProperty("patientId") int patientId) {
    this.patientId = patientId;
  }

  public int getPatientId() {
    return patientId;
  }

}
