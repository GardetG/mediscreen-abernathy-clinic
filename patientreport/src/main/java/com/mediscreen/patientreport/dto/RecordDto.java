package com.mediscreen.patientreport.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Dto class for medical record with observation notes.
 */
public class RecordDto {

  private final int patientId;
  private final String notes;

  /**
   * Constructor method to create a new RecordDto.
   *
   * @param patientId id of the patient
   * @param notes     observation notes of the record
   */
  @JsonCreator
  public RecordDto(@JsonProperty("patientId") int patientId,
                   @JsonProperty("notes") String notes) {
    this.patientId = patientId;
    this.notes = notes;
  }

  public int getPatientId() {
    return patientId;
  }

  public String getNotes() {
    return notes;
  }

}
