package com.mediscreen.patientreport.dto;

import com.mediscreen.patientreport.constant.Gender;
import java.time.LocalDate;

/**
 * Dto class for patients search criteria.
 */
public class SearchDto {

  private final String firstname;
  private final String lastname;
  private final LocalDate dateOfBirth;
  private final Gender gender;

  /**
   * Constructor method to create a new SearchDto.
   *
   * @param firstname   firstname of the patient
   * @param lastname    lastname of the patient
   * @param dateOfBirth date of Birth of the patient
   * @param gender      gender of the patient
   */
  public SearchDto(String firstname, String lastname, LocalDate dateOfBirth,
                   Gender gender) {
    this.firstname = firstname;
    this.lastname = lastname;
    this.dateOfBirth = dateOfBirth;
    this.gender = gender;
  }

  public String getFirstname() {
    return firstname;
  }

  public String getLastname() {
    return lastname;
  }

  public LocalDate getDateOfBirth() {
    return dateOfBirth;
  }

  public Gender getGender() {
    return gender;
  }

}
