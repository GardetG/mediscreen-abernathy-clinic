package com.mediscreen.patientreport.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.mediscreen.patientreport.constant.Gender;
import java.time.LocalDate;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

/**
 * Dto class for patient with personal information.
 */
public class PatientDto {

  private final int id;
  private final String firstname;
  private final String lastname;
  @Past(message = "must be a past date")
  private final LocalDate dateOfBirth;
  @NotNull(message = "must be a valid gender")
  private final Gender gender;

  /**
   * Constructor method to create a new PatientDto.
   *
   * @param id          id of the patient
   * @param firstname   firstname of the patient
   * @param lastname    lastname of the patient
   * @param dateOfBirth date of birth of the patient
   * @param gender      gender of the patient
   */
  @JsonCreator
  public PatientDto(@JsonProperty("patientId") int id,
                    @JsonProperty("firstname") String firstname,
                    @JsonProperty("lastname") String lastname,
                    @JsonProperty("dateOfBirth") LocalDate dateOfBirth,
                    @JsonProperty("gender") Gender gender) {
    this.id = id;
    this.firstname = firstname;
    this.lastname = lastname;
    this.dateOfBirth = dateOfBirth;
    this.gender = gender;
  }

  public int getId() {
    return id;
  }

  public String getFirstname() {
    return firstname;
  }

  public String getLastname() {
    return lastname;
  }

  public LocalDate getDateOfBirth() {
    return dateOfBirth;
  }

  public Gender getGender() {
    return gender;
  }

}
