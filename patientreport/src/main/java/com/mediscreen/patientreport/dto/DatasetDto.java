package com.mediscreen.patientreport.dto;

import com.mediscreen.patientreport.constant.Gender;

/**
 * Dto class holding the dataset used to perform assessment on a patient risk of diabetes.
 */
public class DatasetDto {

  private final Gender gender;
  private final int age;
  private final int triggersCount;

  /**
   * Constructor method to create un new DatasetDto.
   *
   * @param gender        gender of the patient
   * @param age           age of the patient
   * @param triggersCount number of triggers in patient's medical history
   */
  public DatasetDto(Gender gender, int age, int triggersCount) {
    this.gender = gender;
    this.age = age;
    this.triggersCount = triggersCount;
  }

  public Gender getGender() {
    return gender;
  }

  public int getAge() {
    return age;
  }

  public int getTriggersCount() {
    return triggersCount;
  }

}
