package com.mediscreen.patientreport.dto;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Dto class for holding parameter for report request by lastname.
 */
public class RequestByLastnameDto {

  @JsonAlias("familyName")
  private final String lastname;

  /**
   * Constructor method to create new RequestByLastnameDto.
   *
   * @param lastname lastname of the patients
   */
  @JsonCreator
  public RequestByLastnameDto(@JsonProperty("lastname") String lastname) {
    this.lastname = lastname;
  }

  public String getLastname() {
    return lastname;
  }

}
