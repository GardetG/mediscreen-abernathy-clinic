package com.mediscreen.patientreport;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.reactive.config.EnableWebFlux;

/**
 * Patient Report main class.
 */
@SpringBootApplication
@EnableWebFlux
public class PatientReportApplication {

  public static void main(String[] args) {
    SpringApplication.run(PatientReportApplication.class, args);
  }

}
