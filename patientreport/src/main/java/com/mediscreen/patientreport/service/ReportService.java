package com.mediscreen.patientreport.service;

import com.mediscreen.patientreport.dto.ReportDto;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Service interface to generate diabetes assessment {@link ReportDto} on the patients.
 */
public interface ReportService {

  /**
   * Get the diabetes assessment report of a patient by the provided id.
   *
   * @param id id of the patient
   * @return a Mono of {@link ReportDto}
   */
  Mono<ReportDto> getReportById(int id);

  /**
   * Get the diabetes assessment reports of all patients sharing the provided lastname.
   *
   * @param lastname lastname of the patients
   * @return a Flux of {@link ReportDto}
   */
  Flux<ReportDto> getReportByLastname(String lastname);

}
