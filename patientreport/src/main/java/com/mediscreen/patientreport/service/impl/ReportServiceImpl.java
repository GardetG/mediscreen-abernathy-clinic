package com.mediscreen.patientreport.service.impl;

import com.mediscreen.patientreport.client.PatientInfoClient;
import com.mediscreen.patientreport.constant.Risk;
import com.mediscreen.patientreport.dto.DatasetDto;
import com.mediscreen.patientreport.dto.PatientDto;
import com.mediscreen.patientreport.dto.ReportDto;
import com.mediscreen.patientreport.dto.SearchDto;
import com.mediscreen.patientreport.exception.ClientBadResponseException;
import com.mediscreen.patientreport.exception.ClientUnavailableException;
import com.mediscreen.patientreport.service.AssessmentService;
import com.mediscreen.patientreport.service.MedicalHistoryService;
import com.mediscreen.patientreport.service.ReportService;
import java.time.LocalDate;
import java.time.Period;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Service class implementation to generate diabetes assessment {@link ReportDto} on the patients.
 */
@Service
public class ReportServiceImpl implements ReportService {

  private static final Logger LOGGER = LoggerFactory.getLogger(ReportServiceImpl.class);

  @Autowired
  private PatientInfoClient patientInfoClient;
  @Autowired
  private MedicalHistoryService medicalHistoryService;
  @Autowired
  private AssessmentService assessmentService;

  @Autowired
  private Validator validator;

  /**
   * {@inheritDoc}
   */
  @Override
  public Mono<ReportDto> getReportById(int id) {
    return patientInfoClient.getPatientById(id)
        .zipWith(medicalHistoryService.countDiabetesTriggers(id))
        .map(tuple -> {
          DatasetDto dataset = generateDataset(tuple.getT1(), tuple.getT2());
          return generateReport(tuple.getT1(), dataset);
        });
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Flux<ReportDto> getReportByLastname(String lastname) {
    SearchDto criteria = new SearchDto(null, lastname, null, null);
    return patientInfoClient.searchPatients(criteria)
        .flatMap(patient -> medicalHistoryService.countDiabetesTriggers(patient.getId())
            .map(triggersCount -> generateDataset(patient, triggersCount))
            .map(dataset -> generateReport(patient, dataset))
            .transform(reportMono -> handleError(reportMono, patient.getId()))
        );
  }

  /**
   * Handle error occurring while processing a patient from a flux to return a report fallback.
   *
   * @param reportMono a Mono of ReportDto that could contain error
   * @param id         id of the patient
   * @return a Mono of the fallback report
   */
  private Mono<ReportDto> handleError(Mono<ReportDto> reportMono, int id) {
    return reportMono
        .onErrorResume(ConstraintViolationException.class, e -> {
          LOGGER.warn("Patient {} invalid: {}", id, e.getMessage());
          String msg = String.format("Error in patient's personal information: %s", e.getMessage());
          return Mono.just(new ReportDto(id, msg, Risk.ERROR));
        })
        .onErrorResume(ClientBadResponseException.class, e -> {
          LOGGER.error(e.getMessage());
          return Mono.just(new ReportDto(id, e.getMessage(), Risk.ERROR));
        })
        .onErrorResume(ClientUnavailableException.class, e -> {
          LOGGER.error(e.getMessage());
          return Mono.just(new ReportDto(id, e.getMessage(), Risk.ERROR));
        });
  }

  /**
   * Generate a dataset to perform assessment from the patient's personal information and the number
   * of triggers in medical history.
   *
   * @param patient       patient's personal information
   * @param triggersCount number of triggers
   * @return a DatasetDto
   */
  @Validated
  private DatasetDto generateDataset(PatientDto patient, int triggersCount) {
    validatePatient(patient);
    return new DatasetDto(
        patient.getGender(),
        calculateAge(patient),
        triggersCount
    );
  }

  /**
   * Validate a patient.
   * In case of constraints violation then an ConstraintViolationException is thrown.
   *
   * @param patient patient to validate
   * @throws ConstraintViolationException with constraints violation
   */
  private void validatePatient(PatientDto patient) {
    Set<ConstraintViolation<PatientDto>> errors = validator.validate(patient);
    if (!errors.isEmpty()) {
      throw new ConstraintViolationException(errors);
    }
  }

  /**
   * Calculate the age of the patient.
   * Patient date of birth must have been validated first.
   *
   * @param patient patient's personal information
   * @return patient's age
   */
  private int calculateAge(PatientDto patient) {
    LocalDate dateOfBirth = patient.getDateOfBirth();
    return Period.between(dateOfBirth, LocalDate.now()).getYears();
  }

  /**
   * Generate a report from the provided patient's personal information and the diabetes risk level
   * return by the assessment on the dataset.
   *
   * @param patient patient's personal information
   * @param dataset dataset to perform assessment
   * @return a ReportDto
   */
  private ReportDto generateReport(PatientDto patient, DatasetDto dataset) {
    Risk risk = assessmentService.assess(dataset);
    String msg = String.format("Patient: %s %s (age %d) diabetes assessment is: %s",
        patient.getFirstname(), patient.getLastname(), dataset.getAge(), risk.getValue());
    return new ReportDto(patient.getId(), msg, risk);
  }

}
