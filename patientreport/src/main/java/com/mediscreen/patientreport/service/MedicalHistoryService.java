package com.mediscreen.patientreport.service;

import reactor.core.publisher.Mono;

/**
 * Service interface to analyze the patient's medical history and return data to assess.
 */
public interface MedicalHistoryService {

  /**
   * Return the number of diabetes triggers in the patient's medical history by the provided id.
   *
   * @param id id of the patient
   * @return a Mono of diabetes triggers count
   */
  Mono<Integer> countDiabetesTriggers(int id);

}
