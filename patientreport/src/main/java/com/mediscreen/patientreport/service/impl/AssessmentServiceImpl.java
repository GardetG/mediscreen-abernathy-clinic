package com.mediscreen.patientreport.service.impl;

import com.mediscreen.patientreport.constant.Gender;
import com.mediscreen.patientreport.constant.Risk;
import com.mediscreen.patientreport.dto.DatasetDto;
import com.mediscreen.patientreport.service.AssessmentService;
import org.springframework.stereotype.Service;

/**
 * Service class implementation to perform assessment.
 */
@Service
public class AssessmentServiceImpl implements AssessmentService {

  /**
   * {@inheritDoc}
   */
  @Override
  public Risk assess(DatasetDto dataset) {
    if (dataset == null) {
      throw new  IllegalArgumentException("Dataset to assess can't be null.");
    }
    if (dataset.getAge() > 30) {
      return assessOlderThan30(dataset);
    }
    return assessYoungerThan30(dataset);
  }

  /**
   * Assess patient older than 30yo.
   *
   * @param dataset dataset to assess
   * @return Risk level
   */
  private Risk assessOlderThan30(DatasetDto dataset) {
    if (dataset.getTriggersCount() >= 8) {
      return Risk.EARLY;
    }
    if (dataset.getTriggersCount() >= 6) {
      return Risk.DANGER;
    }
    if (dataset.getTriggersCount() >= 2) {
      return Risk.BORDERLINE;
    }
    return Risk.NONE;
  }

  /**
   * Assess patient younger than 30yo.
   *
   * @param dataset dataset to assess
   * @return Risk level
   */
  private Risk assessYoungerThan30(DatasetDto dataset) {
    if (dataset.getGender().equals(Gender.MALE)) {
      return assessMaleYoungerThan30(dataset);
    }
    return assessFemaleYoungerThan30(dataset);
  }

  /**
   * Assess Male patient younger than 30yo.
   *
   * @param dataset dataset to assess
   * @return Risk level
   */
  private Risk assessMaleYoungerThan30(DatasetDto dataset) {
    if (dataset.getTriggersCount() >= 5) {
      return Risk.EARLY;
    }
    if (dataset.getTriggersCount() >= 3) {
      return Risk.DANGER;
    }
    return Risk.NONE;
  }

  /**
   * Assess Female patient younger than 30yo.
   *
   * @param dataset dataset to assess
   * @return Risk level
   */
  private Risk assessFemaleYoungerThan30(DatasetDto dataset) {
    if (dataset.getTriggersCount() >= 7) {
      return Risk.EARLY;
    }
    if (dataset.getTriggersCount() >= 4) {
      return Risk.DANGER;
    }
    return Risk.NONE;
  }

}
