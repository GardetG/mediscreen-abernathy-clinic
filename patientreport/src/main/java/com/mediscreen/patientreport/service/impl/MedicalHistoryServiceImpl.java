package com.mediscreen.patientreport.service.impl;

import com.mediscreen.patientreport.client.PatientHistoryClient;
import com.mediscreen.patientreport.config.TriggersConfig;
import com.mediscreen.patientreport.dto.RecordDto;
import com.mediscreen.patientreport.service.MedicalHistoryService;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

/**
 * Service implementation class to analyze the patient's medical history and return data to assess.
 */
@Service
public class MedicalHistoryServiceImpl implements MedicalHistoryService {

  @Autowired
  private TriggersConfig config;
  @Autowired
  private PatientHistoryClient patientHistoryClient;

  /**
   * {@inheritDoc}
   */
  @Override
  public Mono<Integer> countDiabetesTriggers(int id) {
    return patientHistoryClient.getMedicalHistory(id)
        .collectList()
        .map(this::countTriggers);
  }

  /**
   * Count diabetes triggers in the provided list of medical records.
   *
   * @param records medical records from the patient's medial history
   * @return triggers count
   */
  private int countTriggers(List<RecordDto> records) {
    return config.getDiabetesTriggers()
        .stream()
        .filter(trigger -> isPresent(records, trigger))
        .collect(Collectors.toList())
        .size();
  }

  /**
   * Check if the provided trigger is present in any medical record of the medical history.
   *
   * @param records records of the medical history
   * @param trigger trigger keyword to find
   * @return true if trigger present
   */
  private boolean isPresent(List<RecordDto> records, String trigger) {
    String regex = String.format("(?i).*%s.*", trigger);
    return records.stream()
        .anyMatch(medicalRecord -> medicalRecord.getNotes() != null
            && medicalRecord.getNotes().matches(regex));
  }

}
