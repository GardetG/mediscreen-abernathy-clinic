package com.mediscreen.patientreport.service;

import com.mediscreen.patientreport.constant.Risk;
import com.mediscreen.patientreport.dto.DatasetDto;

/**
 * Service interface to perform assessment.
 */
public interface AssessmentService {

  /**
   * Performs assessment on the provided {@link DatasetDto} to define the diabetes {@link Risk}
   * level.
   *
   * @param dataset {@link DatasetDto} data to assess
   * @return {@link Risk} level
   */
  Risk assess(DatasetDto dataset);

}
