package com.mediscreen.patientreport.route;

import static org.springframework.web.reactive.function.server.RequestPredicates.POST;

import com.mediscreen.patientreport.dto.ReportDto;
import com.mediscreen.patientreport.dto.RequestByIdDto;
import com.mediscreen.patientreport.dto.RequestByLastnameDto;
import com.mediscreen.patientreport.exception.ClientBadResponseException;
import com.mediscreen.patientreport.exception.ClientUnavailableException;
import com.mediscreen.patientreport.exception.PatientNotFoundException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import javax.validation.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springdoc.core.annotations.RouterOperation;
import org.springdoc.core.annotations.RouterOperations;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.reactive.function.server.HandlerFilterFunction;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

/**
 * Configuration class to configure the application functional endpoints route and the filter to
 * handle exceptions.
 */
@Configuration
public class RouteConfig {

  private static final Logger LOGGER = LoggerFactory.getLogger(RouteConfig.class);

  /**
   * RouterFunction Bean mapping request route to the handier methods.
   *
   * @param handler {@link ReportHandler} which handle report related request.
   * @return RouterFunction
   */
  @RouterOperations(
      {
          @RouterOperation(path = "/assess/id",
              produces = {MediaType.APPLICATION_JSON_VALUE},
              consumes = {MediaType.APPLICATION_JSON_VALUE},
              method = RequestMethod.POST, beanClass = ReportHandler.class, beanMethod = "getReportById",
              operation = @Operation(operationId = "getReportById",
                  description = "Get a report by patient's Id",
                  responses = {
                      @ApiResponse(responseCode = "200", description = "OK",
                          content = @Content(schema = @Schema(implementation = ReportDto.class))),
                      @ApiResponse(responseCode = "404", description = "NOT FOUND - Patient not found",
                          content = @Content),
                      @ApiResponse(responseCode = "502", description = "BAD GATEWAY - Unable to "
                          + "process patient since external service returns an unexpected response",
                          content = @Content),
                      @ApiResponse(responseCode = "503", description = "SERVICE UNAVAILABLE - "
                          + " Unable to process patient since external service is unavailable",
                          content = @Content)
                  },
                  requestBody = @RequestBody(
                      content = @Content(schema = @Schema(implementation = RequestByIdDto.class))),
                  tags = {"Report"}
              )
          ),
          @RouterOperation(path = "/assess/lastname",
              produces = {MediaType.APPLICATION_JSON_VALUE},
              consumes = {MediaType.APPLICATION_JSON_VALUE},
              method = RequestMethod.POST, beanClass = ReportHandler.class, beanMethod = "getReportByLastname",
              operation = @Operation(operationId = "getReportByLastname",
                  description = "Get a report by patient's lastname",
                  responses = {
                      @ApiResponse(responseCode = "200", description = "OK", content = @Content(
                          array = @ArraySchema(schema = @Schema(implementation = ReportDto.class)))),
                      @ApiResponse(responseCode = "502", description = "BAD GATEWAY - Unable to "
                          +
                          "process patients since external service returns an unexpected response",
                          content = @Content),
                      @ApiResponse(responseCode = "503", description = "SERVICE UNAVAILABLE - "
                          + " Unable to process patients since external services is unavailable",
                          content = @Content)
                  },
                  requestBody = @RequestBody(
                      content = @Content(schema = @Schema(implementation = RequestByLastnameDto.class))),
                  tags = {"Report"}
              )
          )
      }
  )
  @Bean
  public RouterFunction<ServerResponse> route(ReportHandler handler) {
    return RouterFunctions
        .route(POST("/assess/id"), handler::getReportById)
        .andRoute(POST("/assess/familyName"), handler::getReportByLastname)
        .filter(handlePatientNotFoundException())
        .filter(handleConstraintViolationException())
        .filter(handleClientBadResponseException())
        .filter(handleClientUnavailableException());
  }

  /**
   * Filter to handle {@link PatientNotFoundException} thrown when the patient couldn't be found.
   *
   * @return HandlerFilterFunction
   */
  private HandlerFilterFunction<ServerResponse, ServerResponse> handlePatientNotFoundException() {
    return (request, next) -> next.handle(request)
        .onErrorResume(PatientNotFoundException.class, e -> {
              String error = e.getMessage();
              LOGGER.warn(error);
              HttpStatus status = HttpStatus.NOT_FOUND;
              LOGGER.info("Response: {} {}", status.value(), status.getReasonPhrase());
              return ServerResponse.status(status).bodyValue(error);
            }
        );
  }

  /**
   * Filter to handle {@link ConstraintViolationException} thrown when the patient is invalid.
   *
   * @return HandlerFilterFunction
   */
  private HandlerFilterFunction<ServerResponse, ServerResponse> handleConstraintViolationException() {
    return (request, next) -> next.handle(request)
        .onErrorResume(ConstraintViolationException.class, e -> {
              String error = String.format("Unable to process patient: %s", e.getMessage());
              LOGGER.error(error);
              HttpStatus status = HttpStatus.BAD_GATEWAY;
              LOGGER.info("Response: {} {}", status.value(), status.getReasonPhrase());
              return ServerResponse.status(status).bodyValue(error);
            }
        );
  }

  /**
   * Filter to handle {@link ClientBadResponseException} thrown when requested service returned an
   * unexpected response.
   *
   * @return HandlerFilterFunction
   */
  private HandlerFilterFunction<ServerResponse, ServerResponse> handleClientBadResponseException() {
    return (request, next) -> next.handle(request)
        .onErrorResume(ClientBadResponseException.class, e -> {
          String error = e.getMessage();
          LOGGER.error("{} {}", error, e.getResponse());
          HttpStatus status = HttpStatus.BAD_GATEWAY;
          LOGGER.info("Response: {} {}", status.value(), status.getReasonPhrase());
          return ServerResponse.status(status).bodyValue(error);
        });
  }

  /**
   * Filter to handle {@link ClientUnavailableException} thrown when requested service is
   * unavailable.
   *
   * @return HandlerFilterFunction
   */
  private HandlerFilterFunction<ServerResponse, ServerResponse> handleClientUnavailableException() {
    return (request, next) -> next.handle(request)
        .onErrorResume(ClientUnavailableException.class, e -> {
          String error = e.getMessage();
          LOGGER.error(error);
          HttpStatus status = HttpStatus.SERVICE_UNAVAILABLE;
          LOGGER.info("Response: {} {}", status.value(), status.getReasonPhrase());
          return ServerResponse.status(status).bodyValue(error);
        });
  }

}
