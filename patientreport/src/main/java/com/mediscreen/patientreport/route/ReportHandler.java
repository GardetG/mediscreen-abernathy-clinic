package com.mediscreen.patientreport.route;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mediscreen.patientreport.dto.ReportDto;
import com.mediscreen.patientreport.dto.RequestByIdDto;
import com.mediscreen.patientreport.dto.RequestByLastnameDto;
import com.mediscreen.patientreport.service.ReportService;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

/**
 * Handler class for functional endpoints to handle report related requests.
 */
@Component
public class ReportHandler {

  private static final Logger LOGGER = LoggerFactory.getLogger(ReportHandler.class);

  @Autowired
  private ReportService reportService;
  @Autowired
  private ObjectMapper mapper;

  /**
   * Handle GET request on a patient's diabetes assessment report by the provided patient id in the
   * request body.
   *
   * @param request Request to handle
   * @return a Mono of ServerResponse with HTTP 200 and diabetes assessment {@link ReportDto}
   */
  @NonNull
  public Mono<ServerResponse> getReportById(ServerRequest request) {
    return extractBodyAsMono(request, RequestByIdDto.class)
        .map(RequestByIdDto::getPatientId)
        .flatMap(id -> {
          LOGGER.info("Request: Get patient {}'s report", id);
          return reportService.getReportById(id);
        })
        .flatMap(reportDto -> {
          LOGGER.info("Response: Patient {}'s report sent", reportDto.getPatientId());
          return ServerResponse.ok().bodyValue(reportDto);
        });
  }

  /**
   * Handle GET request on patients' diabetes assessment report by the provided patient lastname in
   * the request body.
   *
   * @param request Request to handle
   * @return a Mono of ServerResponse with HTTP 200 and diabetes assessment {@link ReportDto}
   */
  @NonNull
  public Mono<ServerResponse> getReportByLastname(ServerRequest request) {
    return extractBodyAsMono(request, RequestByLastnameDto.class)
        .map(RequestByLastnameDto::getLastname)
        .flatMapMany(lastname -> {
          LOGGER.info("Request: Get reports for patients with lastname {}", lastname);
          return reportService.getReportByLastname(lastname);
        })
        .collectList()
        .flatMap(reportList -> {
          LOGGER.info("Response: Report for patients sent");
          return ServerResponse.ok().bodyValue(reportList);
        });
  }

  /**
   * Extract and map the body of the request as a mono of the requested Dto.
   * Form URL encoded et application Json MediaTypes are handled.
   *
   * @param request Request from which the body is extracted
   * @param clazz   Class of the required Dto
   * @param <T>     Type of the required Dto
   * @return Mono of the Do
   */
  private <T> Mono<T> extractBodyAsMono(ServerRequest request, Class<T> clazz) {
    Optional<MediaType> mediaType = request.headers().contentType();
    if (mediaType.isPresent() && mediaType.get().equals(MediaType.APPLICATION_FORM_URLENCODED)) {
      return request.formData()
          .map(MultiValueMap::toSingleValueMap)
          .map(map -> mapper.convertValue(map, clazz));
    }
    return request.bodyToMono(clazz);
  }

}
