package com.mediscreen.patientreport.constant;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Enum to hold Risk level.
 */
public enum Risk {
  NONE("None"),
  BORDERLINE("Borderline"),
  DANGER("In danger"),
  EARLY("Early onset"),
  ERROR("Error");

  private final String value;

  Risk(String value) {
    this.value = value;
  }

  @JsonValue
  public String getValue() {
    return value;
  }

}
