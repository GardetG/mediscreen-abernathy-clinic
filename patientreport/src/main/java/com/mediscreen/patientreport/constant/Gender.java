package com.mediscreen.patientreport.constant;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Enum to hold patient's gender.
 */
public enum Gender {
  FEMALE("F"),
  MALE("M");

  private final String value;

  Gender(String value) {
    this.value = value;
  }

  @JsonValue
  public String getValue() {
    return value;
  }

}
