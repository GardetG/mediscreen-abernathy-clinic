package com.mediscreen.patientreport.client.impl;

import com.mediscreen.patientreport.client.PatientInfoClient;
import com.mediscreen.patientreport.dto.PatientDto;
import com.mediscreen.patientreport.dto.SearchDto;
import com.mediscreen.patientreport.exception.ClientBadResponseException;
import com.mediscreen.patientreport.exception.ClientUnavailableException;
import com.mediscreen.patientreport.exception.PatientNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientRequestException;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Client class implementationto request {@link PatientDto Patient} from the PatientInfo service.
 */
@Service
public class PatientInfoClientImpl implements PatientInfoClient {

  @Autowired
  @Qualifier("infoWebClient")
  private WebClient webClient;

  /**
   * {@inheritDoc}
   */
  @Override
  public Mono<PatientDto> getPatientById(int patientId) {
    String url = String.format("/patient/%d", patientId);
    return webClient.get()
        .uri(url)
        .retrieve()
        .bodyToMono(PatientDto.class)
        .onErrorMap(WebClientResponseException.class,
            e -> {
              if (e.getRawStatusCode() == 404) {
                return new PatientNotFoundException("Patient not Found.");
              }
              String response = String.format("Error: %s", e.getRawStatusCode());
              return new ClientBadResponseException("Unable to get patient.", response, e);
            })
        .onErrorMap(WebClientRequestException.class,
            e -> new ClientUnavailableException("Unable to get patient.", e));
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Flux<PatientDto> searchPatients(SearchDto criteria) {
    String url = "/patient/search";
    return webClient.post()
        .uri(url)
        .bodyValue(criteria)
        .retrieve()
        .bodyToFlux(PatientDto.class)
        .onErrorMap(WebClientResponseException.class,
            e -> {
              String response = String.format("Error: %s", e.getRawStatusCode());
              return new ClientBadResponseException("Unable to search patients.", response, e);
            })
        .onErrorMap(WebClientRequestException.class,
            e -> new ClientUnavailableException("Unable to search patients.", e));
  }

}
