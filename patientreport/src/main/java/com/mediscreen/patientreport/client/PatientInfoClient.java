package com.mediscreen.patientreport.client;

import com.mediscreen.patientreport.dto.PatientDto;
import com.mediscreen.patientreport.dto.SearchDto;
import com.mediscreen.patientreport.exception.ClientBadResponseException;
import com.mediscreen.patientreport.exception.ClientUnavailableException;
import com.mediscreen.patientreport.exception.PatientNotFoundException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Client interface to request {@link PatientDto Patient} from the PatientInfo service.
 */
public interface PatientInfoClient {

  /**
   * Fetch the patient's personal information by the provided id.
   * If no patient with such id could be found then an {@link PatientNotFoundException} is thrown.
   *
   * @param patientId id of the {@link PatientDto Patient}
   * @return a Mono of {@link PatientDto Patient}
   * @throws PatientNotFoundException   when patient is not found
   * @throws ClientBadResponseException when PatientInfo service returned an unexpected response
   * @throws ClientUnavailableException when PatientInfo service is unavailable
   */
  Mono<PatientDto> getPatientById(int patientId);

  /**
   * Search all patient's personal information matching with the provided search criteria.
   *
   * @param criteria Search criteria
   * @return a Flux of {@link PatientDto Patient}
   * @throws ClientBadResponseException when PatientInfo service returned an unexpected response
   * @throws ClientUnavailableException when PatientInfo service is unavailable
   */
  Flux<PatientDto> searchPatients(SearchDto criteria);

}
