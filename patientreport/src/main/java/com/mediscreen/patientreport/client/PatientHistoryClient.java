package com.mediscreen.patientreport.client;

import com.mediscreen.patientreport.dto.PatientDto;
import com.mediscreen.patientreport.dto.RecordDto;
import com.mediscreen.patientreport.exception.ClientBadResponseException;
import com.mediscreen.patientreport.exception.ClientUnavailableException;
import reactor.core.publisher.Flux;

/**
 * Client interface to request {@link RecordDto records} of medical history from the
 * PatientHistory service.
 */
public interface PatientHistoryClient {

  /**
   * Fetch the patient's medical history by the provided id.
   *
   * @param patientId id of the {@link PatientDto Patient}
   * @return a Flux of {@link RecordDto}
   * @throws ClientBadResponseException when PatientHistory service returned an unexpected response
   * @throws ClientUnavailableException when PatientHistory service is unavailable
   */
  Flux<RecordDto> getMedicalHistory(int patientId);

}
