package com.mediscreen.patientreport.client.impl;

import com.mediscreen.patientreport.client.PatientHistoryClient;
import com.mediscreen.patientreport.dto.RecordDto;
import com.mediscreen.patientreport.exception.ClientBadResponseException;
import com.mediscreen.patientreport.exception.ClientUnavailableException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientRequestException;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.core.publisher.Flux;

/**
 * Client class implementation to request {@link RecordDto records} of medical history from the
 * PatientHistory service.
 */
@Service
public class PatientHistoryClientImpl implements PatientHistoryClient {

  @Autowired
  @Qualifier("historyWebClient")
  private WebClient webClient;

  /**
   * {@inheritDoc}
   */
  @Override
  public Flux<RecordDto> getMedicalHistory(int patientId) {
    String url = String.format("patHistory/patient/%d/plain", patientId);
    return webClient.get()
        .uri(url)
        .retrieve()
        .bodyToFlux(RecordDto.class)
        .onErrorMap(WebClientResponseException.class,
            e -> {
              String response = String.format("Error: %s", e.getRawStatusCode());
              return new ClientBadResponseException("Unable to get medical history.", response, e);
            })
        .onErrorMap(WebClientRequestException.class,
            e -> new ClientUnavailableException("Unable to get medical history.", e));
  }

}
