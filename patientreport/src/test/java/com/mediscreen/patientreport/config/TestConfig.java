package com.mediscreen.patientreport.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.web.reactive.function.client.ExchangeFunction;
import org.springframework.web.reactive.function.client.WebClient;

/**
 * Configuration class for testing.
 */
@TestConfiguration
public class TestConfig {

  @Autowired
  private ExchangeFunction exchangeFunction;

  /**
   * Declare a WebClient Bean for testing purposes using a custom exchange function which could be
   * mocked to return the expected mocked response.
   *
   * @return WebClient
   */
  @Bean({"infoWebClient", "historyWebClient"})
  public WebClient TestWebClient() {
    return WebClient.builder()
        .exchangeFunction(exchangeFunction)
        .build();
  }

}
