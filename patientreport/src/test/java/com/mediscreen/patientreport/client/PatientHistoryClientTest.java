package com.mediscreen.patientreport.client;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mediscreen.patientreport.client.impl.PatientHistoryClientImpl;
import com.mediscreen.patientreport.config.TestConfig;
import com.mediscreen.patientreport.constant.Gender;
import com.mediscreen.patientreport.dto.PatientDto;
import com.mediscreen.patientreport.dto.RecordDto;
import com.mediscreen.patientreport.exception.ClientBadResponseException;
import com.mediscreen.patientreport.exception.ClientUnavailableException;
import com.mediscreen.patientreport.utils.TestUtils;
import java.io.IOException;
import java.net.URI;
import java.time.LocalDate;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.web.reactive.function.client.ClientRequest;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.ExchangeFunction;
import org.springframework.web.reactive.function.client.WebClientRequestException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

@SpringBootTest(classes = {PatientHistoryClientImpl.class})
@Import(TestConfig.class)
class PatientHistoryClientTest {

  @Autowired
  PatientHistoryClient patientHistoryClient;

  @MockBean
  private ExchangeFunction exchangeFunction;

  @Captor
  ArgumentCaptor<ClientRequest> clientRequestCaptor;

  @DisplayName("Get a patient's medical history should return a Flux with records Dto")
  @Test
  void getMedicalHistoryTest() throws JsonProcessingException {
    // GIVEN
    int patientId = 1;
    PatientDto patient = new PatientDto(1, "Patient", "Test", LocalDate.parse("2000-01-01"), Gender.FEMALE);
    RecordDto record1 = new RecordDto(1, "Record notes test 1");
    RecordDto record2 = new RecordDto(1, "Record notes test 2");
    Mono<ClientResponse> response = TestUtils.mockResponse(HttpStatus.OK, List.of(record1, record2));
    when(exchangeFunction.exchange(any(ClientRequest.class))).thenReturn(response);

    // WHEN
    Flux<RecordDto> recordsFlux = patientHistoryClient.getMedicalHistory(patientId);

    // THEN
    StepVerifier.create(recordsFlux)
        .assertNext(r -> assertThat(r).usingRecursiveComparison().isEqualTo(record1))
        .assertNext(r -> assertThat(r).usingRecursiveComparison().isEqualTo(record2))
        .verifyComplete();
    verify(exchangeFunction, times(1)).exchange(clientRequestCaptor.capture());
    assertThat(clientRequestCaptor.getValue().url())
        .isEqualTo(URI.create("patHistory/patient/" + patientId + "/plain"));
  }

  @DisplayName("Get a patient's medical history when an unexpected response is returned should return a Flux with exception")
  @Test
  void getMedicalHistoryWhenBadResponseTest() throws JsonProcessingException {
    // GIVEN
    int patientId = 1;
    HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
    Mono<ClientResponse> response = TestUtils.mockResponse(status, status.getReasonPhrase());
    when(exchangeFunction.exchange(any(ClientRequest.class))).thenReturn(response);

    // WHEN
    Flux<RecordDto> recordsFlux = patientHistoryClient.getMedicalHistory(patientId);

    // THEN
    StepVerifier.create(recordsFlux)
        .expectError(ClientBadResponseException.class)
        .verify();
    verify(exchangeFunction, times(1)).exchange(any(ClientRequest.class));
  }

  @DisplayName("Get a patient's medical history when the service is unavailable should return a Flux with exception")
  @Test
  void getMedicalHistoryWhenUnavailableTest() {
    // GIVEN
    int patientId = 9;
    Mono<ClientResponse> response =  Mono.error(new WebClientRequestException(
        new IOException("Connection refused"), HttpMethod.GET, URI.create("/patient/9"), new HttpHeaders()
    ));
    when(exchangeFunction.exchange(any(ClientRequest.class))).thenReturn(response);

    // WHEN
    Flux<RecordDto> recordsFlux = patientHistoryClient.getMedicalHistory(patientId);

    // THEN
    StepVerifier.create(recordsFlux)
        .expectError(ClientUnavailableException.class)
        .verify();
    verify(exchangeFunction, times(1)).exchange(any(ClientRequest.class));
  }

}