package com.mediscreen.patientreport.client;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mediscreen.patientreport.client.impl.PatientInfoClientImpl;
import com.mediscreen.patientreport.config.TestConfig;
import com.mediscreen.patientreport.constant.Gender;
import com.mediscreen.patientreport.dto.PatientDto;
import com.mediscreen.patientreport.dto.SearchDto;
import com.mediscreen.patientreport.exception.ClientBadResponseException;
import com.mediscreen.patientreport.exception.ClientUnavailableException;
import com.mediscreen.patientreport.exception.PatientNotFoundException;
import com.mediscreen.patientreport.utils.TestUtils;
import java.io.IOException;
import java.net.URI;
import java.time.LocalDate;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.web.reactive.function.client.ClientRequest;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.ExchangeFunction;
import org.springframework.web.reactive.function.client.WebClientRequestException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

@SpringBootTest(classes = {PatientInfoClientImpl.class})
@Import(TestConfig.class)
class PatientInfoClientTest {

  @Autowired
  private PatientInfoClient patientInfoClient;

  @MockBean
  private ExchangeFunction exchangeFunction;

  @Captor
  ArgumentCaptor<ClientRequest> clientRequestCaptor;

  @DisplayName("Get a patient by id should return a Mono with the patient Dto")
  @Test
  void getPatientByIdTest() throws JsonProcessingException {
    // GIVEN
    int patientId = 1;
    PatientDto patient = new PatientDto(1, "Patient", "Test", LocalDate.parse("2000-01-01"), Gender.FEMALE);
    Mono<ClientResponse> response = TestUtils.mockResponse(HttpStatus.OK, patient);
    when(exchangeFunction.exchange(any(ClientRequest.class))).thenReturn(response);

    // WHEN
    Mono<PatientDto> patientMono = patientInfoClient.getPatientById(patientId);

    // THEN
    StepVerifier.create(patientMono)
        .assertNext(p -> assertThat(p).usingRecursiveComparison().isEqualTo(patient))
        .verifyComplete();
    verify(exchangeFunction, times(1)).exchange(clientRequestCaptor.capture());
    assertThat(clientRequestCaptor.getValue().url()).isEqualTo(URI.create("/patient/" + patientId));
  }

  @DisplayName("Get a patient by id when response is 404 not found should return a Mono with exception")
  @Test
  void getPatientByIdWhenNotFoundTest() throws JsonProcessingException {
    // GIVEN
    int patientId = 9;
    Mono<ClientResponse> response = TestUtils.mockResponse(HttpStatus.NOT_FOUND, "Patient not found");
    when(exchangeFunction.exchange(any(ClientRequest.class))).thenReturn(response);

    // WHEN
    Mono<PatientDto> patientMono = patientInfoClient.getPatientById(patientId);

    // THEN
    StepVerifier.create(patientMono)
        .expectError(PatientNotFoundException.class)
        .verify();
    verify(exchangeFunction, times(1)).exchange(any(ClientRequest.class));
  }

  @DisplayName("Get a patient by id when an unexpected response is returned should return a Mono with exception")
  @Test
  void getPatientByIdWhenBadResponseTest() throws JsonProcessingException {
    // GIVEN
    int patientId = 1;
    HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
    Mono<ClientResponse> response = TestUtils.mockResponse(status, status.getReasonPhrase());
    when(exchangeFunction.exchange(any(ClientRequest.class))).thenReturn(response);

    // WHEN
    Mono<PatientDto> patientMono = patientInfoClient.getPatientById(patientId);

    // THEN
    StepVerifier.create(patientMono)
        .expectError(ClientBadResponseException.class)
        .verify();
    verify(exchangeFunction, times(1)).exchange(any(ClientRequest.class));
  }

  @DisplayName("Get a patient by id when the service is unavailable should return a Mono with exception")
  @Test
  void getPatientByIdWhenUnavailableTest() {
    // GIVEN
    int patientId = 9;
    Mono<ClientResponse> response =  Mono.error(new WebClientRequestException(
        new IOException("Connection refused"), HttpMethod.GET, URI.create("/patient/9"), new HttpHeaders()
        ));
    when(exchangeFunction.exchange(any(ClientRequest.class))).thenReturn(response);

    // WHEN
    Mono<PatientDto> patientMono = patientInfoClient.getPatientById(patientId);

    // THEN
    StepVerifier.create(patientMono)
        .expectError(ClientUnavailableException.class)
        .verify();
    verify(exchangeFunction, times(1)).exchange(any(ClientRequest.class));
  }

  @DisplayName("Search patients by lastname should return a Flux with the patients Dto")
  @Test
  void searchPatientsTest() throws JsonProcessingException {
    // GIVEN
    SearchDto criteria = new SearchDto(null, "Test", null, null);
    PatientDto patient1 = new PatientDto(1, "Patient1", "Test", LocalDate.parse("2000-01-01"), Gender.FEMALE);
    PatientDto patient2 = new PatientDto(2, "Patient2", "Test", LocalDate.parse("2000-01-01"), Gender.FEMALE);
    Mono<ClientResponse> response = TestUtils.mockResponse(HttpStatus.OK, List.of(patient1, patient2));
    when(exchangeFunction.exchange(any(ClientRequest.class))).thenReturn(response);

    // WHEN
    Flux<PatientDto> patientFlux = patientInfoClient.searchPatients(criteria);

    // THEN
    StepVerifier.create(patientFlux)
        .assertNext(p -> assertThat(p).usingRecursiveComparison().isEqualTo(patient1))
        .assertNext(p -> assertThat(p).usingRecursiveComparison().isEqualTo(patient2))
        .verifyComplete();
    verify(exchangeFunction, times(1)).exchange(clientRequestCaptor.capture());
    assertThat(clientRequestCaptor.getValue().url()).isEqualTo(URI.create("/patient/search"));
  }

  @DisplayName("Search patients when an unexpected response is returned should return a Flux with exception")
  @Test
  void searchPatientsWhenBadResponseTest() throws JsonProcessingException {
    // GIVEN
    SearchDto criteria = new SearchDto(null, "Test", null, null);
    HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
    Mono<ClientResponse> response = TestUtils.mockResponse(status, status.getReasonPhrase());
    when(exchangeFunction.exchange(any(ClientRequest.class))).thenReturn(response);

    // WHEN
    Flux<PatientDto> patientFlux = patientInfoClient.searchPatients(criteria);

    // THEN
    StepVerifier.create(patientFlux)
        .expectError(ClientBadResponseException.class)
        .verify();
    verify(exchangeFunction, times(1)).exchange(any(ClientRequest.class));
  }

  @DisplayName("Search patients when the service is unavailable should return a Flux with exception")
  @Test
  void searchPatientsWhenUnavailableTest() {
    // GIVEN
    SearchDto criteria = new SearchDto(null, "Test", null, null);
    Mono<ClientResponse> response =  Mono.error(new WebClientRequestException(
        new IOException("Connection refused"), HttpMethod.GET, URI.create("/patient/9"), new HttpHeaders()
    ));
    when(exchangeFunction.exchange(any(ClientRequest.class))).thenReturn(response);

    // WHEN
    Flux<PatientDto> patientFlux = patientInfoClient.searchPatients(criteria);

    // THEN
    StepVerifier.create(patientFlux)
        .expectError(ClientUnavailableException.class)
        .verify();
    verify(exchangeFunction, times(1)).exchange(any(ClientRequest.class));
  }

}