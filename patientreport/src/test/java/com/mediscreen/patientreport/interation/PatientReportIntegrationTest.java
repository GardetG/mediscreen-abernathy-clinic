package com.mediscreen.patientreport.interation;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;

import com.github.tomakehurst.wiremock.http.HttpHeader;
import com.github.tomakehurst.wiremock.http.HttpHeaders;
import com.mediscreen.patientreport.constant.Risk;
import com.mediscreen.patientreport.dto.RequestByIdDto;
import com.mediscreen.patientreport.dto.RequestByLastnameDto;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.test.web.reactive.server.WebTestClient;

@SpringBootTest(properties = {
    "patientInfoUrl=http://localhost:${wiremock.server.port}",
    "patientHistoryUrl=http://localhost:${wiremock.server.port}",
})
@AutoConfigureWebTestClient
@AutoConfigureWireMock(port = 0)
class PatientReportIntegrationTest {

  private static final HttpHeaders HEADERS = new HttpHeaders(
      new HttpHeader("Content-Type", "application/json")
  );

  @Autowired
  private WebTestClient webClient;

  @DisplayName("As a Doctor i want to get the diabetes assessment rapport of a patient by id to inform him")
  @Test
  void getReportByIdTest() {
    // GIVEN a patient's personal information and medical history
    stubFor(get(urlEqualTo("/patient/1")).willReturn(aResponse()
        .withStatus(200).withHeaders(HEADERS).withBodyFile("reportById/patientInfo.json")));
    stubFor(get(urlEqualTo("/patHistory/patient/1/plain")).willReturn(aResponse()
        .withStatus(200).withHeaders(HEADERS).withBodyFile("reportById/patientHistory.json")));
    RequestByIdDto requestBody = new RequestByIdDto(1);

    // WHEN a Doctor request the assessment report
    webClient
        .post().uri("/assess/id")
        .bodyValue(requestBody)
        .exchange()

        // THEN he receives the report
        .expectStatus().isOk()
        .expectBody()
        .jsonPath("$.patientId").isEqualTo(1)
        .jsonPath("$.risk").isEqualTo(Risk.EARLY.getValue())
        .jsonPath("$.report")
        .isEqualTo("Patient: PatientEarlyOnset Test (age 20) diabetes assessment is: Early onset");
  }

  @DisplayName("As a Doctor i want to get assessment rapport of relatives by lastname to inform him")
  @Test
  void getReportByLastnameTest() {
    // GIVEN patients sharing same lastname and their medical history
    stubFor(post(urlEqualTo("/patient/search")).willReturn(aResponse()
        .withStatus(200).withHeaders(HEADERS).withBodyFile("reportsByLastname/patientsInfo.json")));
    stubFor(get(urlEqualTo("/patHistory/patient/1/plain")).willReturn(aResponse()
        .withStatus(200).withHeaders(HEADERS)
        .withBodyFile("reportsByLastname/patient1History.json")));
    stubFor(get(urlEqualTo("/patHistory/patient/2/plain")).willReturn(aResponse()
        .withStatus(200).withHeaders(HEADERS)
        .withBodyFile("reportsByLastname/patient2History.json")));
    stubFor(get(urlEqualTo("/patHistory/patient/3/plain")).willReturn(aResponse()
        .withStatus(200).withHeaders(HEADERS)
        .withBodyFile("reportsByLastname/patient3History.json")));
    RequestByLastnameDto requestBody = new RequestByLastnameDto("Test");

    // WHEN a Doctor request the assessment reports
    webClient
        .post().uri("/assess/familyName")
        .bodyValue(requestBody)
        .exchange()

        // THEN he receives it
        .expectStatus().isOk()
        .expectBody()
        .jsonPath("$[0].patientId").isEqualTo(1)
        .jsonPath("$[0].risk").isEqualTo(Risk.NONE.getValue())
        .jsonPath("$[0].report")
        .isEqualTo("Patient: PatientNone Test (age 55) diabetes assessment is: None")
        .jsonPath("$[1].patientId").isEqualTo(2)
        .jsonPath("$[1].risk").isEqualTo(Risk.BORDERLINE.getValue())
        .jsonPath("$[1].report")
        .isEqualTo("Patient: PatientBorderline Test (age 77) diabetes assessment is: Borderline")
        .jsonPath("$[2].patientId").isEqualTo(3)
        .jsonPath("$[2].risk").isEqualTo(Risk.DANGER.getValue())
        .jsonPath("$[2].report")
        .isEqualTo("Patient: PatientInDanger Test (age 18) diabetes assessment is: In danger");
  }

}
