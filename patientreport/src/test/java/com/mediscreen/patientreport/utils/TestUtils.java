package com.mediscreen.patientreport.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.http.HttpStatus;
import org.springframework.web.reactive.function.client.ClientResponse;
import reactor.core.publisher.Mono;

/**
 * Utility class used for testing
 */
public class TestUtils {

  private static final ObjectMapper MAPPER = new ObjectMapper()
      .registerModule(new JavaTimeModule());

  /**
   * Build a ClientResponse to mock WebClient response according to the provided HttpStatus and body.
   *
   * @param status HttpStatus of the response
   * @param body Body of the response
   * @param <T> Type of the provided body
   * @return ClientResponse
   * @throws JsonProcessingException if a problem is encountered while deserializing the body
   */
  public static <T> Mono<ClientResponse> mockResponse(HttpStatus status, T body)
      throws JsonProcessingException {
    String bodyString = MAPPER.writeValueAsString(body);
    return Mono.just(ClientResponse
        .create(status)
        .header("content-type", "application/json")
        .body(bodyString)
        .build());
  }

}
