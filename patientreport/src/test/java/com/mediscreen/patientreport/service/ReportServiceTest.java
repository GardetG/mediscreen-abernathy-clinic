package com.mediscreen.patientreport.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.mediscreen.patientreport.client.PatientInfoClient;
import com.mediscreen.patientreport.constant.Gender;
import com.mediscreen.patientreport.constant.Risk;
import com.mediscreen.patientreport.dto.DatasetDto;
import com.mediscreen.patientreport.dto.PatientDto;
import com.mediscreen.patientreport.dto.ReportDto;
import com.mediscreen.patientreport.dto.SearchDto;
import com.mediscreen.patientreport.exception.ClientUnavailableException;
import com.mediscreen.patientreport.exception.PatientNotFoundException;
import java.time.LocalDate;
import java.util.List;
import javax.validation.ConstraintViolationException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

@SpringBootTest
class ReportServiceTest {

  @Autowired
  private ReportService reportService;

  @MockBean
  private PatientInfoClient infoClient;
  @MockBean
  private MedicalHistoryService medicalHistoryService;
  @MockBean
  private AssessmentService assessmentService;

  @Captor
  private ArgumentCaptor<DatasetDto> datasetCaptor;

  @DisplayName("Get report by id should return a Mono of patient's report")
  @Test
  void getReportByIdTest() {
    // GIVEN
    int id = 1;
    LocalDate birthdate = LocalDate.now().minusYears(20);
    PatientDto patient = new PatientDto(id, "Patient1", "Test", birthdate, Gender.FEMALE);
    when(infoClient.getPatientById(anyInt())).thenReturn(Mono.just(patient));
    when(medicalHistoryService.countDiabetesTriggers(anyInt())).thenReturn(Mono.just(1));
    when(assessmentService.assess(any())).thenReturn(Risk.NONE);

    // WHEN
    Mono<ReportDto> reportMono = reportService.getReportById(id);

    // THEN
    StepVerifier.create(reportMono)
        .assertNext(report -> assertThat(report).usingRecursiveComparison()
            .isEqualTo(
                new ReportDto(1, "Patient: Patient1 Test (age 20) diabetes assessment is: None",
                    Risk.NONE)))
        .verifyComplete();
    verify(infoClient, times(1)).getPatientById(1);
    verify(medicalHistoryService, times(1)).countDiabetesTriggers(1);
    verify(assessmentService, times(1)).assess(datasetCaptor.capture());
    assertThat(datasetCaptor.getValue()).usingRecursiveComparison()
        .isEqualTo(new DatasetDto(Gender.FEMALE, 20, 1));
  }

  @DisplayName("Get report by id when patient not found should return a Mono error")
  @Test
  void getReportByIdWhenNotFoundTest() {
    // GIVEN
    when(infoClient.getPatientById(anyInt()))
        .thenReturn(Mono.error(new PatientNotFoundException("Patient not Found.")));
    when(medicalHistoryService.countDiabetesTriggers(anyInt())).thenReturn(Mono.just(1));

    // WHEN
    Mono<ReportDto> reportMono = reportService.getReportById(9);

    // THEN
    StepVerifier.create(reportMono)
        .expectError(PatientNotFoundException.class)
        .verify();
    verify(assessmentService, times(0)).assess(any(DatasetDto.class));
  }

  @DisplayName("Get report by id when patient invalid should return a Mono error")
  @Test
  void getReportByIdWhenInvalidTest() {
    // GIVEN
    int id = 1;
    LocalDate birthdate = LocalDate.now().plusYears(20);
    PatientDto patient = new PatientDto(id, "Patient1", "Test", birthdate, null);
    when(infoClient.getPatientById(anyInt())).thenReturn(Mono.just(patient));
    when(medicalHistoryService.countDiabetesTriggers(anyInt())).thenReturn(Mono.just(1));

    // WHEN
    Mono<ReportDto> reportMono = reportService.getReportById(id);

    // THEN
    StepVerifier.create(reportMono)
        .expectErrorSatisfies(e -> assertThat(e)
            .isInstanceOf(ConstraintViolationException.class)
            .hasMessageContaining("must be a past date")
            .hasMessageContaining("must be a valid gender")
        )
        .verify();
    verify(assessmentService, times(0)).assess(any(DatasetDto.class));
  }

  @DisplayName("Get report by id when info client exception should return a Mono error")
  @Test
  void getReportByIdWhenInfoClientExceptionTest() {
    // GIVEN
    when(infoClient.getPatientById(anyInt()))
        .thenReturn(Mono.error(new ClientUnavailableException("Unable to get patient.")));
    when(medicalHistoryService.countDiabetesTriggers(anyInt())).thenReturn(Mono.just(1));

    // WHEN
    Mono<ReportDto> reportMono = reportService.getReportById(1);

    // THEN
    StepVerifier.create(reportMono)
        .expectError(ClientUnavailableException.class)
        .verify();
    verify(assessmentService, times(0)).assess(any(DatasetDto.class));
  }

  @DisplayName("Get report by id when medical history client exception should return a Mono error")
  @Test
  void getReportByIdWhenHistoryClientExceptionTest() {
    // GIVEN
    int id = 1;
    LocalDate birthdate = LocalDate.now().plusYears(20);
    PatientDto patient = new PatientDto(id, "Patient1", "Test", birthdate, null);
    when(infoClient.getPatientById(anyInt())).thenReturn(Mono.just(patient));
    when(medicalHistoryService.countDiabetesTriggers(anyInt()))
        .thenReturn(Mono.error(new ClientUnavailableException("Unable to get medical history.")));

    // WHEN
    Mono<ReportDto> reportMono = reportService.getReportById(1);

    // THEN
    StepVerifier.create(reportMono)
        .expectError(ClientUnavailableException.class)
        .verify();
    verify(assessmentService, times(0)).assess(any(DatasetDto.class));
  }

  @DisplayName("Get report by lastname should return a flux of report")
  @Test
  void getReportByLastnameTest() {
    // GIVEN
    String lastname = "Test";
    LocalDate birthdate1 = LocalDate.now().minusYears(20);
    PatientDto patient1 = new PatientDto(1, "Patient1", lastname, birthdate1, Gender.FEMALE);
    LocalDate birthdate2 = LocalDate.now().minusYears(40);
    PatientDto patient2 = new PatientDto(2, "Patient2", lastname, birthdate2, Gender.MALE);
    when(infoClient.searchPatients(any())).thenReturn(
        Flux.fromIterable(List.of(patient1, patient2)));
    when(medicalHistoryService.countDiabetesTriggers(anyInt()))
        .thenReturn(Mono.just(1)).thenReturn(Mono.just(8));
    when(assessmentService.assess(any())).thenReturn(Risk.NONE).thenReturn(Risk.EARLY);

    // WHEN
    Flux<ReportDto> reportFlux = reportService.getReportByLastname(lastname);

    // THEN
    StepVerifier.create(reportFlux)
        .assertNext(report -> assertThat(report).usingRecursiveComparison()
            .isEqualTo(new ReportDto(1, "Patient: Patient1 Test (age 20) diabetes assessment is: None", Risk.NONE)))
        .assertNext(report -> assertThat(report).usingRecursiveComparison()
            .isEqualTo(new ReportDto(2, "Patient: Patient2 Test (age 40) diabetes assessment is: Early onset", Risk.EARLY)))
        .verifyComplete();
    verify(infoClient, times(1)).searchPatients(any(SearchDto.class));
    verify(medicalHistoryService, times(2)).countDiabetesTriggers(anyInt());
    verify(assessmentService, times(2)).assess(any());
  }

  @DisplayName("Get report by lastname when empty should return an empty flux of report")
  @Test
  void getReportByLastnameWhenEmptyTest() {
    // GIVEN
    String lastname = "NotFound";
    when(infoClient.searchPatients(any())).thenReturn(Flux.empty());

    // WHEN
    Flux<ReportDto> reportFlux = reportService.getReportByLastname(lastname);

    // THEN
    StepVerifier.create(reportFlux)
        .verifyComplete();
    verify(medicalHistoryService, times(0)).countDiabetesTriggers(anyInt());
    verify(assessmentService, times(0)).assess(any());
  }

  @DisplayName("Get report by lastname when info client exception occurred should return flux error")
  @Test
  void getReportByLastnameWhenInfoClientExceptionTest() {
    // GIVEN
    String lastname = "NotFound";
    when(infoClient.searchPatients(any()))
        .thenReturn(Flux.error(new ClientUnavailableException("Unable to get patients.")));

    // WHEN
    Flux<ReportDto> reportFlux = reportService.getReportByLastname(lastname);

    // THEN
    StepVerifier.create(reportFlux)
        .expectError(ClientUnavailableException.class)
        .verify();
    verify(medicalHistoryService, times(0)).countDiabetesTriggers(anyInt());
    verify(assessmentService, times(0)).assess(any());
  }

  @DisplayName("Get report by lastname when one patient invalid should return a flux of report")
  @Test
  void getReportByLastnameWhenOneIsInvalidTest() {
    // GIVEN
    String lastname = "Test";
    LocalDate birthdate1 = LocalDate.now().plusYears(20);
    PatientDto patient1 = new PatientDto(1, "Patient1", lastname, birthdate1, null);
    when(infoClient.searchPatients(any())).thenReturn(Flux.fromIterable(List.of(patient1)));
    when(medicalHistoryService.countDiabetesTriggers(anyInt())).thenReturn(Mono.just(8));

    // WHEN
    Flux<ReportDto> reportFlux = reportService.getReportByLastname(lastname);

    // THEN
    StepVerifier.create(reportFlux)
        .assertNext(report -> {
          assertThat(report.getReport()).startsWith("Error in patient's personal information:");
          assertThat(report.getRisk()).isEqualTo(Risk.ERROR);
        })
        .verifyComplete();
    verify(assessmentService, times(0)).assess(any());
  }

  @DisplayName("Get report by lastname when history client exception for one patient should return a flux of report")
  @Test
  void getReportByLastnameWhenOneHistoryClientExceptionTest() {
    // GIVEN
    String lastname = "Test";
    LocalDate birthdate1 = LocalDate.now().plusYears(20);
    PatientDto patient1 = new PatientDto(1, "Patient1", lastname, birthdate1, null);
    when(infoClient.searchPatients(any())).thenReturn(Flux.fromIterable(List.of(patient1)));
    when(medicalHistoryService.countDiabetesTriggers(anyInt()))
        .thenReturn(Mono.error(new ClientUnavailableException("Unable to get medical history.")));

    // WHEN
    Flux<ReportDto> reportFlux = reportService.getReportByLastname(lastname);

    // THEN
    StepVerifier.create(reportFlux)
        .assertNext(report -> {
          assertThat(report.getReport()).isEqualTo("Unable to get medical history. Service Unavailable.");
          assertThat(report.getRisk()).isEqualTo(Risk.ERROR);
        })
        .verifyComplete();
    verify(assessmentService, times(0)).assess(any());
  }

}