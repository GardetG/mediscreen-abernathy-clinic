package com.mediscreen.patientreport.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.mediscreen.patientreport.constant.Gender;
import com.mediscreen.patientreport.constant.Risk;
import com.mediscreen.patientreport.dto.DatasetDto;
import java.util.stream.Stream;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class AssessmentServiceTest {

  @Autowired
  private AssessmentService assessmentService;

  @DisplayName("Assess a dataset should return the expected result")
  @ParameterizedTest(name = "Dataset {2} should have risk {1}")
  @MethodSource("provideRecord")
  void assessTest(DatasetDto dataset, Risk expected, String msg) {
    // WHEN
    Risk actualRisk = assessmentService.assess(dataset);

    // THEN
    assertThat(actualRisk).isEqualTo(expected);
  }

  @DisplayName("Assess a null dataset should throw an exception")
  @Test
  void assessWhenNullTest() {
    // WHEN
    assertThatThrownBy(() -> assessmentService.assess(null))

        // THEN
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessageContaining("Dataset to assess can't be null.");
  }

  private static Stream<Arguments> provideRecord() {
    return Stream.of(
        Arguments.of(new DatasetDto(Gender.MALE, 20, 0), Risk.NONE,"M 20yo 0triggers"),
        Arguments.of(new DatasetDto(Gender.MALE, 20, 3), Risk.DANGER,"M 20yo 3triggers"),
        Arguments.of(new DatasetDto(Gender.MALE, 20, 5), Risk.EARLY,"M 20yo 5triggers"),
        Arguments.of(new DatasetDto(Gender.MALE, 40, 0), Risk.NONE,"M 40yo 0triggers"),
        Arguments.of(new DatasetDto(Gender.MALE, 40, 2), Risk.BORDERLINE,"M 40yo 2triggers"),
        Arguments.of(new DatasetDto(Gender.MALE, 40, 6), Risk.DANGER, "M 40yo 6triggers"),
        Arguments.of(new DatasetDto(Gender.MALE, 40, 8), Risk.EARLY, "M 40yo 8triggers"),
        Arguments.of(new DatasetDto(Gender.FEMALE, 20, 0), Risk.NONE, "F 20yo 0triggers"),
        Arguments.of(new DatasetDto(Gender.FEMALE, 20, 4), Risk.DANGER, "F 20yo 4triggers"),
        Arguments.of(new DatasetDto(Gender.FEMALE, 20, 7), Risk.EARLY, "F 20yo 7triggers"),
        Arguments.of(new DatasetDto(Gender.FEMALE, 40, 0), Risk.NONE, "F 40yo 0triggers"),
        Arguments.of(new DatasetDto(Gender.FEMALE, 40, 2), Risk.BORDERLINE, "F 40yo 2triggers"),
        Arguments.of(new DatasetDto(Gender.FEMALE, 40, 6), Risk.DANGER, "F 40yo 6triggers"),
        Arguments.of(new DatasetDto(Gender.FEMALE, 20, 8), Risk.EARLY, "F 40yo 0triggers")
    );
  }

}