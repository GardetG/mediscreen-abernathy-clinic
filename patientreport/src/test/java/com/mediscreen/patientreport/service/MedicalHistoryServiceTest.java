package com.mediscreen.patientreport.service;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.mediscreen.patientreport.client.PatientHistoryClient;
import com.mediscreen.patientreport.dto.RecordDto;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

@SpringBootTest(properties = { "diabetesTriggers=Trigger, OtherTrigger, ThirdTrigger" })
class MedicalHistoryServiceTest {

  @Autowired
  private MedicalHistoryService medicalHistoryService;

  @MockBean
  private PatientHistoryClient patientHistoryClient;

  @DisplayName("Count triggers with one record should return the correct trigger count")
  @ParameterizedTest(name = "Record with {0} should contain {1} triggers")
  @MethodSource("provideRecord")
  void countDiabetesTriggersTest(String notes, int expected) {
    // GIVEN
    Flux<RecordDto> recordsFlux = Flux.fromIterable(List.of(new RecordDto(1, notes)));
    when(patientHistoryClient.getMedicalHistory(anyInt())).thenReturn(recordsFlux);

    // WHEN
    Mono<Integer> countMono = medicalHistoryService.countDiabetesTriggers(1);

    // THEN
    StepVerifier.create(countMono)
            .expectNext(expected)
                .verifyComplete();
    verify(patientHistoryClient,times(1)).getMedicalHistory(1);
  }

  private static Stream<Arguments> provideRecord() {
    return Stream.of(
        Arguments.of(null, 0),
        Arguments.of("", 0),
        Arguments.of("Nothing here", 0),
        Arguments.of("Exact Trigger", 1),
        Arguments.of("trigger lowercase", 1),
        Arguments.of("TRIGGER uppercase", 1),
        Arguments.of("Trigger and same Trigger", 1),
        Arguments.of("Trigger and otherTrigger", 2)
    );
  }

  @DisplayName("Count triggers across multiple record should return the correct trigger count")
  @Test
  void countDiabetesTriggersMultipleRecordTest() {
    // GIVEN
    Flux<RecordDto> recordsFlux = Flux.fromIterable(List.of(
        new RecordDto(1, "One Trigger here"),
        new RecordDto(1, "Nothing Here"),
        new RecordDto(1, "One Trigger and an otherTrigger here"),
        new RecordDto(1, "One Trigger and a ThirdTrigger")
    ));
    when(patientHistoryClient.getMedicalHistory(anyInt())).thenReturn(recordsFlux);

    // WHEN
    Mono<Integer> countMono = medicalHistoryService.countDiabetesTriggers(1);

    // THEN
    StepVerifier.create(countMono)
        .expectNext(3)
        .verifyComplete();
    verify(patientHistoryClient,times(1)).getMedicalHistory(1);
  }

  @DisplayName("Count triggers when no records present should return 0")
  @Test
  void countDiabetesTriggersWhenEmptyTest() {
    // GIVEN
    Flux<RecordDto> recordsFlux = Flux.fromIterable(Collections.emptyList());
    when(patientHistoryClient.getMedicalHistory(anyInt())).thenReturn(recordsFlux);

    // WHEN
    Mono<Integer> countMono = medicalHistoryService.countDiabetesTriggers(1);

    // THEN
    StepVerifier.create(countMono)
        .expectNext(0)
        .verifyComplete();
    verify(patientHistoryClient,times(1)).getMedicalHistory(1);
  }

}