package com.mediscreen.patientreport.route;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import com.mediscreen.patientreport.constant.Risk;
import com.mediscreen.patientreport.dto.ReportDto;
import com.mediscreen.patientreport.dto.RequestByIdDto;
import com.mediscreen.patientreport.dto.RequestByLastnameDto;
import com.mediscreen.patientreport.exception.ClientBadResponseException;
import com.mediscreen.patientreport.exception.ClientUnavailableException;
import com.mediscreen.patientreport.exception.PatientNotFoundException;
import com.mediscreen.patientreport.service.ReportService;
import java.util.List;
import javax.validation.ConstraintViolationException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ReportHandlerTest {

  @Autowired
  private WebTestClient webclient;

  @MockBean
  private ReportService reportService;

  @DisplayName("GET patient's report by id should return 200 with patient's report")
  @Test
  void getReportByIdTest() {
    // GIVEN
    ReportDto report = new ReportDto(1,"This is a test report", Risk.NONE);
    RequestByIdDto requestBody = new RequestByIdDto(1);
    when(reportService.getReportById(anyInt())).thenReturn(Mono.just(report));

    // WHEN
    webclient.post()
        .uri("/assess/id")
        .bodyValue(requestBody)
        .exchange()

        // THEN
        .expectStatus().isOk()
        .expectBody()
        .jsonPath("$.patientId").isEqualTo(1)
        .jsonPath("$.report").isEqualTo("This is a test report")
        .jsonPath("$.risk").isEqualTo(Risk.NONE.getValue());
  }

  @DisplayName("GET patient's report by id when patient invalid should return 502")
  @Test
  void getReportByIdWhenInvalidTest() {
    // GIVEN
    RequestByIdDto requestBody = new RequestByIdDto(1);
    when(reportService.getReportById(anyInt()))
        .thenReturn(Mono.error(new ConstraintViolationException("patient: must be valid.", null)));

    // WHEN
    webclient.post()
        .uri("/assess/id")
        .bodyValue(requestBody)
        .exchange()

        // THEN
        .expectStatus().isEqualTo(HttpStatus.BAD_GATEWAY)
        .expectBody()
        .jsonPath("$").isEqualTo("Unable to process patient: patient: must be valid.");
  }

  @DisplayName("GET patient's report by id when not found should return 404")
  @Test
  void getReportByIdWhenNotFoundTest() {
    // GIVEN
    RequestByIdDto requestBody = new RequestByIdDto(1);
    when(reportService.getReportById(anyInt()))
        .thenReturn(Mono.error(new PatientNotFoundException("Patient not found.")));

    // WHEN
    webclient.post()
        .uri("/assess/id")
        .bodyValue(requestBody)
        .exchange()

        // THEN
        .expectStatus().isNotFound()
        .expectBody()
        .jsonPath("$").isEqualTo("Patient not found.");
  }

  @DisplayName("GET patient's report by id when client bad response should return 502")
  @Test
  void getReportByIdWhenClientBadResponseTest() {
    // GIVEN
    RequestByIdDto requestBody = new RequestByIdDto(1);
    when(reportService.getReportById(anyInt()))
        .thenReturn(Mono.error(new ClientBadResponseException("Unable to request service.", null)));

    // WHEN
    webclient.post()
        .uri("/assess/id")
        .bodyValue(requestBody)
        .exchange()

        // THEN
        .expectStatus().isEqualTo(HttpStatus.BAD_GATEWAY)
        .expectBody()
        .jsonPath("$").isEqualTo("Unable to request service. Unexpected Response.");
  }

  @DisplayName("GET patient's report by id when client service unavailable should return 503")
  @Test
  void getReportByIdWhenClientServiceUnavailableTest() {
    // GIVEN
    RequestByIdDto requestBody = new RequestByIdDto(1);
    when(reportService.getReportById(anyInt()))
        .thenReturn(Mono.error(new ClientUnavailableException("Unable to request service.")));

    // WHEN
    webclient.post()
        .uri("/assess/id")
        .bodyValue(requestBody)
        .exchange()

        // THEN
        .expectStatus().isEqualTo(HttpStatus.SERVICE_UNAVAILABLE)
        .expectBody()
        .jsonPath("$").isEqualTo("Unable to request service. Service Unavailable.");
  }

  @DisplayName("GET patient's report by lastname should return 200 with patients' report")
  @Test
  void getReportByLastnameTest() {
    // GIVEN
    ReportDto report1 = new ReportDto(1,"This is a test report", Risk.NONE);
    ReportDto report2 = new ReportDto(2,"This is an other test report", Risk.BORDERLINE);
    RequestByLastnameDto requestBody = new RequestByLastnameDto("Test");
    when(reportService.getReportByLastname(anyString()))
        .thenReturn(Flux.fromIterable(List.of(report1, report2)));

    // WHEN
    webclient.post()
        .uri("/assess/familyName")
        .bodyValue(requestBody)
        .exchange()

        // THEN
        .expectStatus().isOk()
        .expectBody()
        .jsonPath("$").isArray()
        .jsonPath("$[0].patientId").isEqualTo(1)
        .jsonPath("$[0].risk").isEqualTo(Risk.NONE.getValue())
        .jsonPath("$[1].patientId").isEqualTo(2)
        .jsonPath("$[1].risk").isEqualTo(Risk.BORDERLINE.getValue());
  }

  @DisplayName("GET patient's report by lastname when client bad response should return 502")
  @Test
  void getReportByLastnameWhenClientBadResponseTest() {
    // GIVEN
    RequestByLastnameDto requestBody = new RequestByLastnameDto("Test");
    when(reportService.getReportByLastname(anyString()))
        .thenReturn(Flux.error(new ClientBadResponseException("Unable to request service.", null)));

    // WHEN
    webclient.post()
        .uri("/assess/familyName")
        .bodyValue(requestBody)
        .exchange()

        // THEN
        .expectStatus().isEqualTo(HttpStatus.BAD_GATEWAY)
        .expectBody()
        .jsonPath("$").isEqualTo("Unable to request service. Unexpected Response.");
  }

  @DisplayName("GET patient's report by lastname when client service unavailable should return 503")
  @Test
  void getReportByLastnameWhenClientServiceUnavailableTest() {
    // GIVEN
    RequestByLastnameDto requestBody = new RequestByLastnameDto("Test");
    when(reportService.getReportByLastname(anyString()))
        .thenReturn(Flux.error(new ClientUnavailableException("Unable to request service.")));

    // WHEN
    webclient.post()
        .uri("/assess/familyName")
        .bodyValue(requestBody)
        .exchange()

        // THEN
        .expectStatus().isEqualTo(HttpStatus.SERVICE_UNAVAILABLE)
        .expectBody()
        .jsonPath("$").isEqualTo("Unable to request service. Service Unavailable.");
  }

}