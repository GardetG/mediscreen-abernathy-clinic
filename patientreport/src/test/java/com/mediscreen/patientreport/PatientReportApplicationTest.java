package com.mediscreen.patientreport;

import static org.assertj.core.api.Assertions.assertThat;

import com.mediscreen.patientreport.client.PatientHistoryClient;
import com.mediscreen.patientreport.client.PatientInfoClient;
import com.mediscreen.patientreport.service.AssessmentService;
import com.mediscreen.patientreport.service.MedicalHistoryService;
import com.mediscreen.patientreport.service.ReportService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class PatientReportApplicationTest {

  @Autowired
  private PatientInfoClient patientInfoClient;
  @Autowired
  private PatientHistoryClient patientHistoryClient;
  @Autowired
  private ReportService reportService;
  @Autowired
  private MedicalHistoryService medicalHistoryService;
  @Autowired
  private AssessmentService assessmentService;

  @Test
  void contextLoads() {
    assertThat(patientInfoClient).isNotNull();
    assertThat(patientHistoryClient).isNotNull();
    assertThat(reportService).isNotNull();
    assertThat(medicalHistoryService).isNotNull();
    assertThat(assessmentService).isNotNull();
  }

}