package com.mediscreen.common.utils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.mediscreen.common.config.JwtConfig;
import com.mediscreen.common.domain.JwtPrincipal;
import com.mediscreen.common.domain.Role;
import com.mediscreen.common.exception.JwtValidationException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class JwtUtilsTest {

  private static JwtUtils jwtUtils;

  @BeforeAll
  static void setUp() {
    JwtConfig config = new JwtConfig();
    config.setSecret("myTestingSecret");
    config.setValidity(900000);
    config.setIssuer("Test");
    jwtUtils = new JwtUtils(config);
  }

  @DisplayName("Generate a token fo a user should return a valid jwt")
  @Test
  void generateTokenTest() {
    // Given
    JwtPrincipal user = new JwtPrincipal(1, Role.ADMIN);
    JWTVerifier verifier = JWT.require(Algorithm.HMAC256("myTestingSecret"))
        .withSubject(String.valueOf(user.getUserId()))
        .withClaim("auth", user.getRole().toString())
        .withIssuer("Test")
        .build();

    // When
    String actualToken = jwtUtils.generateToken(user);

    // Then
    assertThat(actualToken).isNotBlank();
    assertThatNoException().isThrownBy(() -> verifier.verify(actualToken));
  }

  @DisplayName("Get principal from a token should return the user")
  @Test
  void getPrincipalTest() throws JwtValidationException {
    // Given
    JwtPrincipal user = new JwtPrincipal(1, Role.ADMIN);
    String token = jwtUtils.generateToken(user);

    // When
    JwtPrincipal actualUser = jwtUtils.getPrincipal(token);

    // Then
    assertThat(actualUser).usingRecursiveComparison().isEqualTo(user);
  }

  @DisplayName("Get principal from an expired token should throw an exception")
  @Test
  void getPrincipalWhenExpiredTest() {
    // Given
    Instant issued15MinutesAgo = Instant.now().minus(15, ChronoUnit.MINUTES);
    Instant expired10MinutesAgo = Instant.now().minus(10, ChronoUnit.MINUTES);
    String token = JWT.create()
        .withSubject("subject")
        .withExpiresAt(Date.from(expired10MinutesAgo))
        .withIssuedAt(Date.from(issued15MinutesAgo))
        .withIssuer("Test")
        .sign(Algorithm.HMAC256("myTestingSecret"));

    // When
    assertThatThrownBy(() -> jwtUtils.getPrincipal(token))

        // Then
        .isInstanceOf(JwtValidationException.class)
        .hasMessage("Authentication token has expired");
  }

  @DisplayName("Get principal from an invalid token should throw an exception")
  @Test
  void getPrincipalWhenInvalidTest() {
    // Given
    String token = "Invalid Token";

    // When
    assertThatThrownBy(() -> jwtUtils.getPrincipal(token))

        // Then
        .isInstanceOf(JwtValidationException.class)
        .hasMessage("Authentication token can't be verified");
  }

}