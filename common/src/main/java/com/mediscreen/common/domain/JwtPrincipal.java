package com.mediscreen.common.domain;

/**
 * Principal class representing a user for an authentication with Jwt.
 */
public class JwtPrincipal {

  private final int userId;
  private final Role role;

  /**
   * Constructor method for a JwtPrincipal with user id and role.
   *
   * @param userId id of the user
   * @param role   {@link Role} of the user
   */
  public JwtPrincipal(int userId, Role role) {
    this.userId = userId;
    this.role = role;
  }

  public int getUserId() {
    return userId;
  }

  public Role getRole() {
    return role;
  }

}
