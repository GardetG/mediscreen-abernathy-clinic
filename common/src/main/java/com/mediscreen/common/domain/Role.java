package com.mediscreen.common.domain;

/**
 * Role Enum to hold the available users' role.
 */
public enum Role {
  ADMIN,
  DOCTOR,
  ORGANIZER,
  GUEST
}
