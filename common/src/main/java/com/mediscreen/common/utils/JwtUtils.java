package com.mediscreen.common.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.mediscreen.common.config.JwtConfig;
import com.mediscreen.common.domain.JwtPrincipal;
import com.mediscreen.common.domain.Role;
import com.mediscreen.common.exception.JwtValidationException;
import java.time.Instant;
import java.util.Date;

/**
 * Utils class to generate, validate and extract Jwt.
 */
public class JwtUtils {

  private final JwtConfig jwtConfig;

  public JwtUtils(JwtConfig jwtConfig) {
    this.jwtConfig = jwtConfig;
  }


  /**
   * Generate a token with the information of the provided principal.
   *
   * @param principal {@link JwtPrincipal} from which generate the token
   * @return token
   */
  public String generateToken(JwtPrincipal principal) {
    Instant now = Instant.now();
    return JWT.create()
        .withSubject(String.valueOf(principal.getUserId()))
        .withClaim("auth", principal.getRole().toString())
        .withExpiresAt(Date.from(now.plusMillis(jwtConfig.getValidity())))
        .withIssuedAt(Date.from(now))
        .withIssuer(jwtConfig.getIssuer())
        .sign(Algorithm.HMAC256(jwtConfig.getSecret()));
  }

  /**
   * Validate the provided token and extract information to return a principal to authenticate.
   * If the token is invalid or is malformed an exception is thrown.
   *
   * @param token to be extracted
   * @return {@link JwtPrincipal} extracted
   * @throws JwtValidationException when token validation fails.
   */
  public JwtPrincipal getPrincipal(String token) throws JwtValidationException {
    DecodedJWT decodedJwt = validateAndDecodeToken(token);
    return new JwtPrincipal(
        Integer.parseInt(decodedJwt.getSubject()),
        Role.valueOf(decodedJwt.getClaim("auth").asString())
    );
  }

  /**
   * Validate a token and return a decoded token for information extraction.
   * If the token is invalid or is malformed an exception is thrown.
   *
   * @param token to validate and decode
   * @return Decoded jwt
   * @throws JwtValidationException when the token is invalid or malformed
   */
  private DecodedJWT validateAndDecodeToken(String token) throws JwtValidationException {
    JWTVerifier verifier = JWT.require(Algorithm.HMAC256(jwtConfig.getSecret()))
        .withIssuer(jwtConfig.getIssuer())
        .build();
    try {
      return verifier.verify(token);
    } catch (TokenExpiredException ex) {
      throw new JwtValidationException("Authentication token has expired");
    } catch (SignatureVerificationException ex) {
      throw new JwtValidationException("Invalid signature for Authentication token");
    } catch (Exception ex) {
      throw new JwtValidationException("Authentication token can't be verified");
    }
  }

}
