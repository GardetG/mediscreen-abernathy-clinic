package com.mediscreen.common.exception;

/**
 * Exception thrown when JWT token validation failed.
 */
public class JwtValidationException extends Exception {

  public JwtValidationException(String s) {
    super(s);
  }

}