package com.mediscreen.common.config;

/**
 *  Configuration Class for jwt properties:
 *  - secret : secret key to signed jwt
 *  - validity : validity duration of token in ms.
 *  - issuer : issuer of the jwt
 */
public class JwtConfig {

  private String secret;
  private long validity;
  private String issuer;

  public String getSecret() {
    return secret;
  }

  public void setSecret(String secret) {
    this.secret = secret;
  }

  public long getValidity() {
    return validity;
  }

  public void setValidity(long validity) {
    this.validity = validity;
  }

  public String getIssuer() {
    return issuer;
  }

  public void setIssuer(String issuer) {
    this.issuer = issuer;
  }

}