package com.mediscreen.apigateway.route;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.mediscreen.common.domain.JwtPrincipal;
import com.mediscreen.common.domain.Role;
import com.mediscreen.common.utils.JwtUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.reactive.server.WebTestClient;

@SpringBootTest(properties = {
    "patientInfoUrl=http://localhost:${wiremock.server.port}",
    "patientHistoryUrl=http://localhost:${wiremock.server.port}",
    "patientReportUrl=http://localhost:${wiremock.server.port}",
    "authServerUrl=http://localhost:${wiremock.server.port}",
})
@AutoConfigureWebTestClient
@AutoConfigureWireMock(port = 0)
class RouteLocatorTest {

  @Autowired
  private WebTestClient webClient;
  @Autowired
  private WireMockServer server;
  @Autowired
  private JwtUtils jwtUtils;

  @DisplayName("As an anonymous user when I request a AuthServer endpoint the gateway should forward the request")
  @Test
  void routeToAuthServerTest() {
    // Given
    stubFor(get(urlEqualTo("/auth")).willReturn(aResponse().withStatus(200)));

    // When
    webClient
        .get().uri("/auth")
        .exchange()

        // Then
        .expectStatus().isOk();
  }

  @DisplayName("As an authenticate user when I request a PatientInfo endpoint the gateway should forward the request")
  @ParameterizedTest(name = "As a {0} when requesting PatientInfo I should receive {1}")
  @CsvSource({"ADMIN, 200", "DOCTOR, 200", "ORGANIZER, 200", "GUEST, 403"})
  void routeToPatientInfoTest(String role, int statusCode) {
    // Given
    JwtPrincipal user = new JwtPrincipal(1, Role.valueOf(role));
    String token = jwtUtils.generateToken(user);
    stubFor(get(urlEqualTo("/patient")).willReturn(aResponse().withStatus(200)));

    // When
    webClient
        .get().uri("/patient")
        .header("Authorization", "Bearer " + token)
        .exchange()

        // Then
        .expectStatus().isEqualTo(HttpStatus.valueOf(statusCode));
  }

  @DisplayName("As an anonymous user when I request a PatientInfo endpoint the gateway should return Unauthorized")
  @Test
  void routeToPatientInfoWhenNotAuthenticateTest() {
    // Given
    stubFor(get(urlEqualTo("/patient")).willReturn(aResponse().withStatus(200)));

    // When
    webClient
        .get().uri("/patient")
        .exchange()

        // Then
        .expectStatus().isUnauthorized();
  }

  @DisplayName("As an authenticate user when I request a PatientHistory endpoint the gateway should forward")
  @ParameterizedTest(name = "As a {0} when requesting PatientHistory I should receive {1}")
  @CsvSource({"ADMIN, 200", "DOCTOR, 200", "ORGANIZER, 403", "GUEST, 403"})
  void routeToPatientHistoryTest(String role, int statusCode) {
    // Given
    JwtPrincipal user = new JwtPrincipal(1, Role.valueOf(role));
    String token = jwtUtils.generateToken(user);
    stubFor(get(urlEqualTo("/patHistory")).willReturn(aResponse().withStatus(200)));

    // When
    webClient
        .get().uri("/patHistory")
        .header("Authorization", "Bearer " + token)
        .exchange()

        // Then
        .expectStatus().isEqualTo(HttpStatus.valueOf(statusCode));
  }

  @DisplayName("As an anonymous user when I request a PatientHistory endpoint the gateway should return Unauthorized")
  @Test
  void routeToPatientHistoryWhenNotAuthenticateTest() {
    // Given
    stubFor(get(urlEqualTo("/patHistory")).willReturn(aResponse().withStatus(200)));

    // When
    webClient
        .get().uri("/patHistory")
        .exchange()

        // Then
        .expectStatus().isUnauthorized();
  }

  @DisplayName("As an authenticate user when I request a PatientReport endpoint the gateway should forward")
  @ParameterizedTest(name = "As a {0} when requesting PatientReport I should receive {1}")
  @CsvSource({"ADMIN, 200", "DOCTOR, 200", "ORGANIZER, 403", "GUEST, 403"})
  void routeToPatientReportTest(String role, int statusCode) {
    // Given
    JwtPrincipal user = new JwtPrincipal(1, Role.valueOf(role));
    String token = jwtUtils.generateToken(user);
    stubFor(get(urlEqualTo("/assess")).willReturn(aResponse().withStatus(200)));

    // When
    webClient
        .get().uri("/assess")
        .header("Authorization", "Bearer " + token)
        .exchange()

        // Then
        .expectStatus().isEqualTo(HttpStatus.valueOf(statusCode));
  }

  @DisplayName("As an anonymous user when I request a PatientReport endpoint the gateway should return Unauthorized")
  @Test
  void routeToPatientReportWhenNotAuthenticateTest() {
    // Given
    stubFor(get(urlEqualTo("/assess")).willReturn(aResponse().withStatus(200)));

    // When
    webClient
        .get().uri("/assess")
        .exchange()

        // Then
        .expectStatus().isUnauthorized();
  }

  @DisplayName("As a user when I request a down service endpoint the gateway should return Unavailable Service")
  @Test
  void routeWhenServiceDownTest() {
    // Given
    server.stop();

    // When
    webClient
        .get().uri("/auth")
        .exchange()

        // Then
        .expectStatus().isEqualTo(HttpStatus.SERVICE_UNAVAILABLE);
    server.start();
  }

}
