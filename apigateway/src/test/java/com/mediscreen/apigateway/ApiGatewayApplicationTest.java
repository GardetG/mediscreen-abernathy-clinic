package com.mediscreen.apigateway;

import static org.assertj.core.api.Assertions.assertThat;

import com.mediscreen.common.utils.JwtUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.gateway.route.RouteLocator;

@SpringBootTest
class ApiGatewayApplicationTest {

  @Autowired
  private JwtUtils jwtUtils;
  @Autowired
  private RouteLocator routeLocator;

  @Test
  void contextLoads() {
    assertThat(jwtUtils).isNotNull();
    assertThat(routeLocator).isNotNull();
  }

}