package com.mediscreen.apigateway.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.security.web.server.context.ServerSecurityContextRepository;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * Server security context repository implementation when using Jwt.
 */
@Component
public class JwtSecurityContextRepository implements ServerSecurityContextRepository {

  @Autowired
  private JwtAuthenticationManager jwtAuthenticationManager;

  /**
   * Saves the SecurityContext. In th case of a stateless Jwt authentication, the security context
   * is held by the token.
   *
   * @param exchange the exchange to associate to the SecurityContext
   * @param context the SecurityContext to save
   * @return a completion notification
   */
  @Override
  public Mono<Void> save(ServerWebExchange exchange, SecurityContext context) {
    throw new UnsupportedOperationException("Stateless Authentication.");
  }

  /**
   * Loads the SecurityContext associated with the {@link ServerWebExchange} by extracting the Jwt
   * token include in the header.
   *
   * @param exchange the exchange to look up the {@link SecurityContext}
   * @return the {@link SecurityContext} to lookup or empty if not found. Never null
   */
  @Override
  public Mono<SecurityContext> load(ServerWebExchange exchange) {
    return Mono.justOrEmpty(exchange.getRequest().getHeaders().getFirst(HttpHeaders.AUTHORIZATION))
        .filter(authHeader -> authHeader.startsWith("Bearer "))
        .flatMap(authHeader -> {
          String authToken = authHeader.substring(7);
          Authentication auth = new UsernamePasswordAuthenticationToken(authToken, authToken);
          return this.jwtAuthenticationManager.authenticate(auth).map(SecurityContextImpl::new);
        });
  }

}