package com.mediscreen.apigateway.security;

import com.mediscreen.common.domain.Role;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.method.configuration.EnableReactiveMethodSecurity;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.web.server.SecurityWebFilterChain;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsConfigurationSource;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;

/**
 * Spring Security configuration class for WebFlux.
 */
@EnableWebFluxSecurity
@EnableReactiveMethodSecurity
public class SecurityConfig {

  private static final String ADMIN = Role.ADMIN.name();
  private static final String DOCTOR = Role.DOCTOR.name();
  private static final String ORGANIZER = Role.ORGANIZER.name();

  @Autowired
  private JwtAuthenticationManager jwtAuthenticationManager;
  @Autowired
  private JwtSecurityContextRepository jwtSecurityContextRepository;

  /**
   * Define the web filter chain with CORS Policy, Jwt authentication and endpoints authorization
   * policies.
   *
   * @param http ServerHttpSecurity
   * @return SecurityWebFilterChain configured
   */
  @Bean
  public SecurityWebFilterChain securityWebFilterChain(ServerHttpSecurity http) {
    return http
        // Enable CORS and disable CSRF
        .cors().configurationSource(corsConfigurationSource())
        .and()
        .csrf().disable()

        // Set AuthenticationManager for Jwt
        .httpBasic().disable()
        .formLogin().disable()
        .authenticationManager(jwtAuthenticationManager)
        .securityContextRepository(jwtSecurityContextRepository)

        // Set permissions on endpoints
        .authorizeExchange()
        .pathMatchers("/auth/**").permitAll()
        .pathMatchers("/patient/**").hasAnyRole(ADMIN, DOCTOR, ORGANIZER)
        .pathMatchers("/patHistory/**").hasAnyRole(ADMIN, DOCTOR)
        .pathMatchers("/assess/**").hasAnyRole(ADMIN, DOCTOR)
        .anyExchange().authenticated()
        .and().build();
  }

  /**
   * Define a Cors configuration Bean.
   *
   * @return CorsConfigurationSource
   */
  @Bean
  CorsConfigurationSource corsConfigurationSource() {
    CorsConfiguration configuration = new CorsConfiguration();
    configuration.setAllowedOrigins(List.of("http://localhost:3000", "http://localhost"));
    configuration.setAllowedMethods(List.of("GET", "POST", "PUT", "PATCH", "DELETE"));
    configuration.setAllowedHeaders(List.of("Authorization", "Cache-Control", "Content-Type"));
    configuration.setExposedHeaders(List.of("Authorization"));
    configuration.setMaxAge(3600L);
    configuration.applyPermitDefaultValues();
    UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
    source.registerCorsConfiguration("/**", configuration);
    return source;
  }

}