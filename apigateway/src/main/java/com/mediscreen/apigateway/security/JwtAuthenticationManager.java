package com.mediscreen.apigateway.security;

import com.mediscreen.common.domain.JwtPrincipal;
import com.mediscreen.common.exception.JwtValidationException;
import com.mediscreen.common.utils.JwtUtils;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

/**
 * Reactive Authentication Manager implementation to authenticate using Jwt.
 */
@Component
public class JwtAuthenticationManager implements ReactiveAuthenticationManager {

  private static final Logger LOGGER = LoggerFactory.getLogger(JwtAuthenticationManager.class);

  @Autowired
  private JwtUtils jwtUtils;

  /**
   * Attempts to authenticate the provided {@link Authentication} with Jwt token.
   *
   * @param authentication the {@link Authentication} to authenticate
   * @return if authentication is successful an {@link Authentication} is returned.
   *     If authentication cannot be determined, an empty Mono is returned.
   */
  @Override
  public Mono<Authentication> authenticate(Authentication authentication) {
    String authToken = authentication.getCredentials().toString();
    return Mono.just(getAuthenticationFromToken(authToken))
        .filter(Optional::isPresent)
        .switchIfEmpty(Mono.empty())
        .map(Optional::get);
  }

  /**
   * Extract authentication from the provided token and return optional of it.
   * If the provided Jwt is invalid and authentication can't be extract from it then an empty
   * optional is returned.
   *
   * @param token Jwt Token
   * @return Optional of Authentication
   */
  private Optional<Authentication> getAuthenticationFromToken(String token) {
    try {
      JwtPrincipal principal = jwtUtils.getPrincipal(token);
      return Optional.of(
          new UsernamePasswordAuthenticationToken(principal, null,
              List.of(new SimpleGrantedAuthority("ROLE_" + principal.getRole())))
      );
    } catch (JwtValidationException ex) {
      LOGGER.warn("Provided JWT invalid: {}", ex.getMessage());
      return Optional.empty();
    }
  }

}