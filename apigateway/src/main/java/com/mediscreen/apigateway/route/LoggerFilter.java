package com.mediscreen.apigateway.route;

import static org.springframework.cloud.gateway.support.ServerWebExchangeUtils.GATEWAY_REQUEST_URL_ATTR;

import com.mediscreen.common.domain.JwtPrincipal;
import java.net.URI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.core.Ordered;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * GatewayFilter class to log user's requests and responses.
 */
public class LoggerFilter implements GatewayFilter, Ordered {

  private static final Logger LOGGER = LoggerFactory.getLogger(LoggerFilter.class);

  /**
   * Log the request with the target route URI, the id of the currently authenticate user and the
   * response status code.
   * If the user is not authenticate then he will be designated as anonymous.
   *
   * @param exchange The current server exchange
   * @param chain    Provides a way to delegate to the next filter
   * @return {@code Mono<Void>} to indicate when request processing is complete
   */
  @Override
  public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
    return exchange.getPrincipal()
        .filter(UsernamePasswordAuthenticationToken.class::isInstance)
        .map(principal ->
            (JwtPrincipal) ((UsernamePasswordAuthenticationToken) principal).getPrincipal())
        .map(principal -> String.format("%s Id %d", principal.getRole(), principal.getUserId()))
        .defaultIfEmpty("ANONYMOUS")
        .map(userName -> {
          URI routeUri = exchange.getAttribute(GATEWAY_REQUEST_URL_ATTR);
          Integer status = exchange.getResponse().getRawStatusCode();
          LOGGER.info("{}: request to {}, response {}", userName, routeUri, status);
          return exchange;
        })
        .flatMap(chain::filter);
  }

  /**
   * Get the order value of this object.
   *
   * @return the order value
   */
  @Override
  public int getOrder() {
    return Ordered.LOWEST_PRECEDENCE;
  }
}
