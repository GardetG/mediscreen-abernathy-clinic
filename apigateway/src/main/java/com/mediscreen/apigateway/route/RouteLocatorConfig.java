package com.mediscreen.apigateway.route;

import com.mediscreen.apigateway.config.UrlConfig;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Configuration class to define the routing of the gateway.
 */
@Configuration
public class RouteLocatorConfig {

  /**
   * RouteLocator Bean.
   *
   * @param builder RouteLocator builder
   * @param config  Url configuration
   * @return RouteLocator
   */
  @Bean
  public RouteLocator customRouteLocator(RouteLocatorBuilder builder, UrlConfig config) {
    return builder.routes()
        .route("authserver", r -> r.path("/auth/**")
            .filters(f -> f.filter(new LoggerFilter()))
            .uri(config.getAuthServerUrl()))
        .route("patientinfo", r -> r.path("/patient/**")
            .filters(f -> f.filter(new LoggerFilter()))
            .uri(config.getPatientInfoUrl()))
        .route("patienthistory", r -> r.path("/patHistory/**")
            .filters(f -> f.filter(new LoggerFilter()))
            .uri(config.getPatientHistoryUrl()))
        .route("patientreport", r -> r.path("/assess/**")
            .filters(f -> f.filter(new LoggerFilter()))
            .uri(config.getPatientReportUrl()))
        .build();
  }

}
