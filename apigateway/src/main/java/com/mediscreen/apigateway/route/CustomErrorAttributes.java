package com.mediscreen.apigateway.route;

import static org.springframework.http.HttpStatus.SERVICE_UNAVAILABLE;

import java.io.IOException;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.reactive.error.DefaultErrorAttributes;
import org.springframework.web.reactive.function.server.ServerRequest;

/**
 * Custom ErrorAttributes class extending the default one to handle IOException and provides access
 * to error attributes which can be logged or presented to the user.
 */
public class CustomErrorAttributes extends DefaultErrorAttributes {

  private static final Logger LOGGER = LoggerFactory.getLogger(CustomErrorAttributes.class);

  /**
   * Get error attributes and handle IOException to return corresponding 503-Service Unavailable
   * response.
   *
   * @param request the source request
   * @param options options for error attribute contents
   * @return a map of error attributes
   */
  @Override
  public Map<String, Object> getErrorAttributes(ServerRequest request,
                                                ErrorAttributeOptions options) {
    Map<String, Object> attributes = super.getErrorAttributes(request, options);
    Throwable error = getError(request);
    if (error instanceof IOException) {
      attributes.put("status", SERVICE_UNAVAILABLE.value());
      attributes.put("error", SERVICE_UNAVAILABLE.getReasonPhrase());
      LOGGER.error("Error: request to {}, response 503 : {}", request.uri(), error.getMessage());
    }
    return attributes;
  }

}