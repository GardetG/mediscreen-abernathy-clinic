package com.mediscreen.apigateway.config;

import com.mediscreen.apigateway.route.CustomErrorAttributes;
import com.mediscreen.common.config.JwtConfig;
import com.mediscreen.common.utils.JwtUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.reactive.error.ErrorAttributes;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Configuration class to create and configure beans.
 */
@Configuration
public class AppConfig {

  @Bean
  @ConfigurationProperties(prefix = "jwt")
  public JwtConfig getJwtConfig() {
    return new JwtConfig();
  }

  @Bean
  public JwtUtils getJwtUtils(JwtConfig config) {
    return new JwtUtils(config);
  }

  @Bean
  public ErrorAttributes errorAttributes() {
    return new CustomErrorAttributes();
  }

}
