package com.mediscreen.apigateway.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Configuration class for services Url:
 * - authServerUrl: Url of the Authentication service.
 * - patientInfoUrl: Url of the PatientInfo service.
 * - patientInfoUrl: Url of the PatientHistory service.
 * - patientReportUrl: Url of the PatientReport service.
 */
@Component
@ConfigurationProperties
public class UrlConfig {

  private String authServerUrl;
  private String patientInfoUrl;
  private String patientHistoryUrl;
  private String patientReportUrl;

  public String getAuthServerUrl() {
    return authServerUrl;
  }

  public void setAuthServerUrl(String authServerUrl) {
    this.authServerUrl = authServerUrl;
  }

  public String getPatientInfoUrl() {
    return patientInfoUrl;
  }

  public void setPatientInfoUrl(String patientInfoUrl) {
    this.patientInfoUrl = patientInfoUrl;
  }

  public String getPatientHistoryUrl() {
    return patientHistoryUrl;
  }

  public void setPatientHistoryUrl(String patientHistoryUrl) {
    this.patientHistoryUrl = patientHistoryUrl;
  }

  public String getPatientReportUrl() {
    return patientReportUrl;
  }

  public void setPatientReportUrl(String patientReportUrl) {
    this.patientReportUrl = patientReportUrl;
  }

}
