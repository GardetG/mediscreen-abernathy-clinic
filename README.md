# Mediscreen - Abernathy Clinic

Mediscreen specializes in detecting risk factors for disease by using predictive analysis of patient populations.
This project developed for Abernathy Clinic aims to provide a tool to help doctors identify patients at risk for diabetes.

## About The Project!

![diagram](docs/schema/microservices.png)

This project is composed of an ensembles of microservices deployed with Docker providing users with a secured interface by authentication token (JWT) to manage their patients:
* Front-End: 
  * React web application providing user interface and communicating with the back-end services through the Api Gateway.
* Back-End:
  * Api Gateway: Handle the incoming traffic of the application and routes it to the other services applying authentication policy.
  * Auth Server: Service managing user account and handling the generation of authentication token.
  * Patient Info: Service managing the patients' personal information.
  * Patient History: Service managing the patient's medical history.
  * Patient Report: Service generating diabetes assessment report.
* Persistence:
  * MySql: Mysql Database service storing patients and users data.
  * MongoDB: Mongo Database service storing patients' medical records.

### Build with

* IntellJ
* JAVA 11
* Gradle 7.4.1
* Spring Boot 2.6.7
* MySql 8
* MongoDB 5.0.9
* Node Js 16.15.0
* React 18.1.0
* Bootstrap 5.1.3
* Docker 20.10.12

## Getting Started

### Prerequisites

* IDE like Eclipse or IntelliJ
* Docker and Docker compose

### Installation

1. Clone the repository
   ```sh
   git@gitlab.com:GardetG/mediscreen-abernathy-clinic.git
   
2. Build services through gradle:
    ```sh
    .\gradlew assemble  or .\gradlew <service>:assemble

3. Test services through gradle:
    ```sh
    .\gradlew check  or .\gradlew <service>:check
   
4. Build docker image and start the stack of containers with Docker-compose: 
    ```sh
   docker-compose up
   
## Usage

* The web application is available when deploy on:  
    ```sh
    http://localhost/ 
* The Api can be tested with Postman using the gateway exposed on:  
    ```sh
    http://localhost:8080/ 

* The application is initialized with a default admin account to log-in:  
    - Email: admin@abernathy.com   
    - Password: adminA=0

* The project offers a bash script to execute the curl command provided in the client specification to initialize testing data, 
It can be found under the scripts folder.
The curl have been modified to take into account the gateway on port 8080 and the authentication process.
    ```sh
    sh initial_curl.sh -u "admin@abernathy.com" -p "adminA=0"

## Documentations

The project JavaDoc and tests reports and coverage can be found [here](https://gardetg.gitlab.io/mediscreen-abernathy-clinic/).

## Roadmap

The Kanban is available [here](https://www.notion.so/75cf8cc5ac0341dd8f6cfdc378be98d7?v=9722cb9613ec4dd69bb5cee83e5e7316).

- [x] Sprint I [Release v0.1.0]
  - [x] View patient info
  - [x] Update patient info
  - [x] Add patient info
  - [x] Remove patient info
  - [x] User Login
  - [x] User Register
- [x] Sprint II [Release v0.2.0]
  - [x] View patient's history
  - [x] Update patient's history
  - [x] Add note to patient's history
  - [x] Remove notes to patient's history
  - [X] Users role management for Admin
- [X] Sprint III
  - [X] Generate diabetes report
  - [X] Implement refresh Token